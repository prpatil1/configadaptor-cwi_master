﻿namespace PRISMDataAdaptor
{
    using CaseParserMaster;
    using ExtractionMaster;
    using HtmlAgilityPack;
    using log4net;
    using Newtonsoft.Json.Linq;
    using OpenQA.Selenium;
    using OpenQA.Selenium.Remote;
    using OpenQA.Selenium.Support.UI;
    using PRISM.Common.DataHelper.AzureSqlManager;
    using PRISM.Common.HttpHelper;
    using PRISM.Common.Models.Constants;
    using PRISM.Common.Models.DBModels;
    using PRISM.Common.Models.RuleEngine;
    using PRISM.Common.Models.Screening;
    using PRISM.Common.NameHelper;
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using System.Data;
    using System.Globalization;
    using System.IO;
    using System.Linq;
    using System.Net;
    using System.Net.Http;
    using System.Net.Http.Headers;
    using System.Reflection;
    using System.Text;
    using System.Text.RegularExpressions;
    using System.Threading;
    using System.Xml.Linq;
    public class Fulfillment
    {
        public IntegrationResponseModel ResponseModel { get; set; }

        public HtmlLog HTMLlogs { get; set; }

        public HttpManager HttpManager { get; set; }

        public List<ConfigDetails> ConfigDetail { get; set; }

        public List<EnumValuesModel> EnumValues { get; set; }

        public DataTable ConfigurableQCSteps { get; set; }

        /// <summary>
        /// Stores Miscellaneous data objects
        /// </summary>
        public Dictionary<string, object> DataDictionary { get; set; }

        /// <summary>
        /// Gets or sets the error string to store responses from functions.
        /// </summary>
        /// <value>
        /// The error string.
        /// </value>
        private string ErrorString { get; set; }

        /// <summary>
        /// Gets or sets the result table column names.
        /// </summary>
        /// <value>
        /// The result table column names.
        /// </value>
        private List<ResultTableColumns> ResultTableColumnNames { get; set; }

        private ConfigCWIModel ConfigCWIModel;

        private static readonly ILog logger = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);
        private string firstname = string.Empty;
        private string lastname = string.Empty;
        private string mongoID = string.Empty;
        private long transactionId = 0;
        private string screeningID = string.Empty;
        private string FirstRowText = string.Empty;
        private bool continuePosting = false;
        private bool performPaging = false;
        private bool hasMultipleResultPage = false;
        private bool hasPagingOrMultilpleLink = false;
        private bool checkResultTable = true;
        private Dictionary<string, object> Session;
        private string dateOfBirth = string.Empty;
        private string curentDate = DateTime.Now.ToString("MM/dd/yyyy");
        private List<string> clickableLinks = new List<string>();
        private int counter = 0;
        private string excelPath = string.Empty;
        private string remoteWebDriverURL = string.Empty;
        private string formatColumnText = string.Empty;
        private int additionalSearchCounter = 0;
        private string rowData = string.Empty;
        private string updatedParameters = string.Empty;
        private bool isElementPresent = false;
        private string minimumFirstNameLength = string.Empty;
        private List<bool> saveResults = new List<bool>();
        private bool continueResultAnalysis = true;
        private bool isValidPage = true;
        private bool clearStringFound = false;
        private bool isReviewCase = false;
        private int stepCount;
        private List<string> firstNameList = new List<string>();
        private List<string> lastNameList = new List<string>();
        private bool performFalseClearCheck = false;
        private RemoteWebDriver driver;
        public string websiteResponse = string.Empty;
        private string postString = string.Empty;
        private Dictionary<string, string> hashValues = new Dictionary<string, string>();
        private string functionresult = string.Empty;
        private List<string> innerHtmlValueList = new List<string>();
        private string addOnDllPath = string.Empty;
        private string formattedSSN = string.Empty;
        private int counterMaxLimit = 50;
        private List<string> globalStringList = new List<string>();
        private int globalStringListCounter = -1;
        private List<string> globalCaseDetailLinkList = new List<string>();
        private int globalCaseDetailLinkCounter = -1;
        private bool isRetryOnMaxCounterLimit = false;
        private int pagingCounter = 0;
        private string counterFormat = string.Empty;
        private bool DoNotUseHashTableForPosting = false;
        private string parentVendor = string.Empty;
        private List<ResponsePageInfo> responsePageList = new List<ResponsePageInfo>();
        private List<ConfigurableCWIStepsModel> configurableCWISteps;
        bool IsCaptchaFail = false;
        public void SearchData()
        {
            log4net.Config.XmlConfigurator.Configure();
            ////Initializing the Dictionary object to save all the objects required while iterating through all the steps
            Session = new Dictionary<string, object>();
            ////Saving all the details of Result Table
            this.ResultTableColumnNames = new List<ResultTableColumns>();
            this.ErrorString = string.Empty;
            try
            {
                ////Reading Config values
                ReadParameters();
                if (this.ErrorString.Trim().Length > 0)
                {
                    ResponseModel.ErrorDescription = this.ErrorString;
                    ResponseModel.ErrorCode = ErrorCode.RunTimeError;
                    ResponseModel.Status = OrderStatus.Retry;
                    return;
                }
                PerformAdditionalSearch();
                if (this.ErrorString.Trim().Length > 0) return;

                ////For False Clear Check and Selenium Check
                bool usesSelenium = false;
                if (configurableCWISteps.Any(row => row.Step.ToString().Trim().ToUpper() == "FALSECLEAR"))
                    performFalseClearCheck = true;
                if (configurableCWISteps.Any(row => row.Step.ToString().Trim().ToUpper() == "USESSELENIUM"))
                    usesSelenium = true;

                if (usesSelenium)
                {
                    ////Initializing the driver
                    driver = new RemoteWebDriver(new Uri(this.remoteWebDriverURL), DesiredCapabilities.Firefox());
                }

                //// Perform Search Operation for each name
                PerformNameSearch();

                //// Check for False Clear
                //if (performFalseClearCheck && isValidPage && !isReviewCase && string.IsNullOrWhiteSpace(this.ErrorString) && isClearCase)
                //{
                //    FalseClearCheck(driver);
                //}

                //// Close the Selenium Driver if it is not null
                if (driver != null)
                {
                    driver.Close();
                }

                ////Set Status depending on the result of iterations
                if (!isValidPage)
                {
                    ResponseModel.ErrorDescription = "ParentVendor: " + parentVendor + " ScreeningID: " + screeningID + " " + this.ErrorString;
                    ResponseModel.ErrorCode = ErrorCode.HttpConnectionFailure;
                    ResponseModel.Status = OrderStatus.Retry;
                    return;
                }
                else if (isReviewCase)
                {
                    ResponseModel.ErrorDescription = string.Empty;
                    ResponseModel.Screening[0].ResultStatus = ResultStatus.Review;
                    ResponseModel.Status = OrderStatus.Fulfilled;
                    return;
                }
                else if (this.ErrorString.Trim().Length > 0)
                {
                    ResponseModel.ErrorDescription = "ParentVendor: " + parentVendor + " ScreeningID: " + screeningID + " " + this.ErrorString;
                    if (ResponseModel.ErrorCode == ErrorCode.Success)
                    {
                        ResponseModel.ErrorCode = ErrorCode.RunTimeError;
                    }
                    if (ResponseModel.Status == OrderStatus.InProgress)
                    {
                        ResponseModel.Status = OrderStatus.Retry;
                    }
                    return;
                }

                //// For Cases
                if (ResponseModel.Screening.First().ScreeningResponse.Cases.Count == 0)
                {
                    ResponseModel.Screening.First().ResultStatus = ResultStatus.Clear;
                    ResponseModel.Status = OrderStatus.Fulfilled;
                    return;
                }
                // Retry order if CaseServedStatus is not FULFUILLED for every case
                else if (ResponseModel.Screening.First().ScreeningResponse.Cases.Any(x => x.CaseServedStatus.Trim().ToUpper() != OrderStatus.Fulfilled.Trim().ToUpper()))
                {
                    ResponseModel.Screening.First().CaseInfoExtracted = false;
                    ResponseModel.Screening.First().ScreeningResponse = null;
                    ResponseModel.ErrorCode = ErrorCode.RunTimeError;
                    ResponseModel.ErrorDescription = "Case served Status not set to 'Fulfilled' for all cases and Error String was not found";
                    return;
                }
                // Set order to HIT if all cases served successfully
                else
                {
                    ResponseModel.Screening.First().ResultStatus = ResultStatus.Hit;
                    ResponseModel.Status = OrderStatus.Fulfilled;
                    return;
                }
            }
            catch (Exception ex)
            {
                ResponseModel.ErrorDescription = "ParentVendor: " + parentVendor + " ScreeningID: " + screeningID + " Error in Configurable QC: " + ex;
                ResponseModel.ErrorCode = ErrorCode.RunTimeError;
                ResponseModel.Status = OrderStatus.Retry;
                return;
            }
            finally
            {
                configurableCWISteps = null;
            }
        }

        private void PerformAdditionalSearch()
        {
            try
            {
                ////Assigning values to Class level variables
                HTMLlogs = new HtmlLog();
                HTMLlogs.HtmlLogFile = new Dictionary<string, string>();
                HTMLlogs.ScreeningId = ResponseModel.Screening[0].ScreeningId;

                ////Applying Name Utility for Name Cleaning and Additional Search
                NameHelper nameHelper = new NameHelper();
                nameHelper.RequestFirstName = this.firstname;
                nameHelper.RequestLastName = this.lastname;
                nameHelper.DefaultAdditionalSearch = true;
                nameHelper.GetAdditionalSearchNames();
                if (!nameHelper.ErrorDescription.Equals(string.Empty))
                {
                    ResponseModel.ErrorCode = nameHelper.ErrorCode;
                    this.ErrorString = ResponseModel.ErrorDescription = nameHelper.ErrorDescription;
                    if (ResponseModel.ErrorCode == ErrorCode.UnhandledSpecialCharacter)
                    {
                        ResponseModel.Status = OrderStatus.Cancelled;
                    }
                    else
                    {
                        ResponseModel.Status = OrderStatus.Retry;
                    }
                    return;
                }

                firstNameList = nameHelper.FirstNameList;
                lastNameList = nameHelper.LastNameList;
                this.firstname = nameHelper.RequestFirstName;
                this.lastname = nameHelper.RequestLastName;
            }
            catch (Exception ex)
            {
                ResponseModel.ErrorCode = ErrorCode.RunTimeError;
                ResponseModel.ErrorDescription = "Error in PerformAddtionalSearch: " + ex.Message;
                ResponseModel.Status = OrderStatus.Retry;
                this.ErrorString = "Error in PerformAddtionalSearch: " + ex.Message;
            }

        }

        private void PerformNameSearch()
        {
            try
            {
                ////Looping for Additonal Search
                for (additionalSearchCounter = 0; additionalSearchCounter < firstNameList.Count; additionalSearchCounter++)
                {
                    this.firstname = firstNameList[additionalSearchCounter];
                    this.lastname = lastNameList[additionalSearchCounter];

                    ////Iterate all the steps present in Excel
                    IterateSteps();

                    ////Check for CLEAR. If CLEAR then perform Additional Search
                    ////Check Error String
                    if (this.ErrorString.Trim().ToUpper() == "SKIP_NAME")
                    {
                        ////Name cannot be searched on Website hence cancel the order
                        this.ErrorString = "484~Defendent Name Limit Exceeded";
                        ResponseModel.ErrorCode = ErrorCode.DefendentNameLimitExceeded;
                        ResponseModel.Status = OrderStatus.Cancelled;
                        break;
                    }
                    else if (this.ErrorString.Trim().Length > 0 || isReviewCase)
                    {
                        break;
                    }
                    //else if (this.ErrorString.Trim().ToUpper() == "CLEAR" || isClearCase)
                    //{
                    //    isClearCase = true;
                    //}
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private void IterateSteps()
        {
            try
            {
                bool exitIteration = false;
                ////Reset all dependant variables
                this.ResultTableColumnNames = new List<ResultTableColumns>();
                FirstRowText = string.Empty;
                clickableLinks = new List<string>();
                innerHtmlValueList = new List<string>();
                this.saveResults = new List<bool>();
                this.postString = string.Empty;
                hashValues = new Dictionary<string, string>();
                continueResultAnalysis = true;

                //if (additionalSearchCounter > 0 ^ isClearCase)
                //{
                //    functionresult = string.Empty;
                //    this.ErrorString = "ParentVendor: " + parentVendor + " ScreeningID: " + screeningID + " Clear case flag was not set before resetting. Please check your steps";
                //}
                //else
                //{
                //    this.isClearCase = false;
                //}

                //Iterate through all steps
                for (stepCount = 0; stepCount < configurableCWISteps.Count; stepCount++)
                {
                    if (pagingCounter > counterMaxLimit)
                    {
                        isReviewCase = true;
                    }
                    else if (counter > counterMaxLimit)
                    {
                        if (isRetryOnMaxCounterLimit)
                        {
                            this.ErrorString = "ParentVendor: " + parentVendor + " ScreeningID: " + screeningID + " Maximum limit for counter reached";
                        }
                        else
                        {
                            isReviewCase = true;
                        }
                    }
                    ////Check Error String
                    if (this.ErrorString.Trim().Length > 0 || isReviewCase || exitIteration)
                    {
                        break;
                    }
                    ////Process the row as per the details given in the columns of the row
                    ProcessRow(configurableCWISteps[stepCount], driver, ref exitIteration);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        //private void FalseClearCheck(RemoteWebDriver driver)
        //{
        //    bool falseClearFlag = true;
        //    try
        //    {
        //        ////Get all the false clear values in class level variables
        //        IEnumerable<ConfigurableCWIStepsModel> results = from myRow in configurableCWISteps
        //                                                         where myRow.Step.ToUpper() == "FALSECLEAR"
        //                                                         select myRow;

        //        List<string> reviewFirstNameList = new List<string>();
        //        List<string> reviewLastNameList = new List<string>();
        //        List<string> reviewDOBList = new List<string>();

        //        if (results.Any(x => x.StepType == "MULTILPLEREVIEWNAMES"))
        //        {
        //            foreach (string name in results.Where(x => x.StepType.ToUpper() == "MULTILPLEREVIEWNAMES")
        //           .Select(Y => Y.ConfigCWIParameter))
        //            {
        //                reviewFirstNameList.Add(name.Split('|')[0]);
        //                reviewLastNameList.Add(name.Split('|')[1]);
        //                reviewDOBList.Add(name.Split('|')[2]);
        //            }
        //        }
        //        else
        //        {
        //            reviewFirstNameList.Add(results.Where(x => x.StepType.ToUpper() == "REVIEWFN")
        //           .Select(Y => Y.ConfigCWIParameter).First<string>());

        //            reviewLastNameList.Add(results.Where(x => x.StepType.ToUpper() == "REVIEWLN")
        //                .Select(Y => Y.ConfigCWIParameter).First<string>());

        //            reviewDOBList.Add(results.Where(x => x.StepType.ToUpper() == "REVIEWDOB")
        //                .Select(Y => Y.ConfigCWIParameter).First<string>());
        //        }
        //        firstNameList = null;
        //        firstNameList = new List<string>(reviewFirstNameList);
        //        lastNameList = null;
        //        lastNameList = new List<string>(reviewLastNameList);

        //        for (int i = 0; i < reviewFirstNameList.Count; i++)
        //        {
        //            this.firstname = reviewFirstNameList[i];
        //            this.lastname = reviewLastNameList[i];
        //            this.dateOfBirth = reviewDOBList[i];

        //            ////Iterate all the steps present in Excel
        //            bool exitIteration = false;
        //            ////Reset all dependant variables
        //            this.ResultTableColumnNames = new List<ResultTableColumns>();
        //            FirstRowText = string.Empty;
        //            clickableLinks = new List<string>();
        //            innerHtmlValueList = new List<string>();
        //            this.saveResults = new List<bool>();
        //            this.postString = string.Empty;
        //            hashValues = new Dictionary<string, string>();
        //            continueResultAnalysis = true;
        //            if (isClearCase)
        //            {
        //                this.isClearCase = false;
        //            }
        //            else
        //            {
        //                this.ErrorString = "ParentVendor: " + parentVendor + " ScreeningID: " + screeningID + " Clear case flag was not set before resetting. Please check your steps";
        //            }
        //            for (stepCount = 0; stepCount < configurableCWISteps.Count; stepCount++)
        //            {
        //                ////Check Error String
        //                if (this.ErrorString.Trim().Length > 0 || isReviewCase || exitIteration)
        //                {
        //                    break;
        //                }
        //                if (configurableCWISteps[stepCount].Step.ToString().ToUpper().Equals("SETFALSECLEARFLAG"))
        //                {
        //                    if (configurableCWISteps[stepCount].ConfigCWIParameter.ToString().ToUpper().Equals("TRUE")
        //                        || configurableCWISteps[stepCount].ConfigCWIParameter.ToString().ToUpper().Equals("YES")
        //                        || configurableCWISteps[stepCount].ConfigCWIParameter.ToString().ToUpper().Equals("Y"))
        //                    {
        //                        falseClearFlag = true;
        //                    }
        //                    else
        //                    {
        //                        falseClearFlag = false;
        //                    }
        //                }
        //                if (falseClearFlag)
        //                {
        //                    ////Process the row as per the details given in the columns of the row
        //                    ProcessRow(configurableCWISteps[stepCount], driver, ref exitIteration);
        //                }
        //            }
        //            ////Check if the result is REVIEW.
        //            if (isReviewCase || this.ErrorString.Trim().ToUpper() == "REVIEW")
        //            {
        //                ////Review encountered. Hence set the original status to CLEAR
        //                isReviewCase = false;
        //                isClearCase = true;
        //            }
        //            else
        //            {
        //                ////Error in known Review case. Set Error message
        //                isReviewCase = false;
        //                isClearCase = false;
        //                this.ErrorString = "ParentVendor: " + parentVendor + " ScreeningID: " + screeningID + " Known Review is not working as expected. Kindly check the Website:-:" + this.ErrorString;
        //                return;
        //            }
        //        }

        //    }
        //    catch (Exception ex)
        //    {
        //        this.ErrorString = "Error in FalseClearCheck: " + ex;
        //    }
        //}

        private void ProcessRow(ConfigurableCWIStepsModel dataRow, RemoteWebDriver driver, ref bool exitIteration)
        {
            try
            {
                ////Replace the Place Holders
                #region Replace Place Holders

                rowData = ReplacePlaceHolders(dataRow.Data.ToString());
                updatedParameters = ReplacePlaceHolders(dataRow.ConfigCWIParameter.ToString());

                if (globalStringListCounter > -1 && globalStringListCounter < globalStringList.Count)
                    rowData = rowData.Replace("PH_GLOBALSTRINGLIST", globalStringList[globalStringListCounter]);

                if (globalCaseDetailLinkCounter > -1 && globalCaseDetailLinkCounter < globalCaseDetailLinkList.Count)
                    rowData = rowData.Replace("PH_GLOBALCASEDETAILLINKLIST", globalCaseDetailLinkList[globalCaseDetailLinkCounter]);

                if (ResponseModel.Screening.First().ScreeningResponse != null && ResponseModel.Screening.First().ScreeningResponse.Cases != null &&
                    ResponseModel.Screening.First().ScreeningResponse.Cases.Count > counter)
                    rowData = rowData.Replace("PH_CASENUMBER", ResponseModel.Screening.First().ScreeningResponse.Cases[counter].CaseNumber);
                #endregion

                switch (dataRow.Step.ToString().ToUpper())
                {
                    case "ISCASEINFOEXTRACTED":
                        if (ResponseModel.Screening[0].CaseInfoExtracted)
                        {
                            functionresult = "TRUE";
                            //// Set additional search counter to max of name list as all the names are searched
                            additionalSearchCounter = firstNameList.Count;
                        }
                        else if (additionalSearchCounter == 0)
                            ResponseModel.Screening[0].ScreeningResponse = new ScreeningResponse() { Cases = new List<Case>() };
                        break;
                    case "SETGLOBALVARIABLE":
                        ////Set/Reset the Global Variables
                        SetGlobalVariable(dataRow, rowData);
                        break;
                    case "ASSIGNDICTIONARYVALUE":
                        functionresult = string.Empty;
                        functionresult = Session[dataRow.StepType.ToString()].ToString();
                        break;
                    case "ASSIGNTOWEBSITERESPONSE":
                        HttpManager.Response = websiteResponse = rowData;
                        break;
                    case "ASSIGNTOFUNCTIONRESULT":
                        functionresult = rowData;
                        break;
                    case "ASSIGNSESSIONVALUETOHTTPMANAGER":
                        HttpManager.SessionValue = functionresult;
                        break;
                    case "GETSESSIONVALUEFROMHTTPMANAGER":
                        functionresult = string.Empty;
                        functionresult = HttpManager.SessionValue;
                        break;
                    case "GOTOURL":
                        ////Performs the posting using driver
                        PerformPosting(driver, dataRow.ConfigCWIParameter.ToString());
                        break;
                    case "VALIDATEPAGE":
                        ////Validates the website response and checks whether the given string is present in the response or not
                        ValidatePage(dataRow, rowData);
                        break;
                    case "TYPETEXT":
                    case "CLICK":
                    case "SELECTDROPDOWN":
                    case "DESELECTDROPDOWN":
                    case "SUBMIT":
                    case "SETCURSORPOSITIONTOSTART":
                    case "CHECKELEMENT":
                    case "CHECKVALUE":
                        ////Perform Element related operations                        
                        ElementOperations(driver, dataRow, ref exitIteration);
                        break;
                    case "MATCHCLEARSTRING":
                        ////Search for Clear Case String on Website
                        clearStringFound = MatchStringsOnWebsite(dataRow, rowData);
                        break;
                    case "MATCHERRORSTRING":
                        ////Search for Error String on Website
                        bool isErrorCase = MatchStringsOnWebsite(dataRow, rowData);
                        if (string.IsNullOrWhiteSpace(this.ErrorString) && isErrorCase)
                            this.ErrorString = "Error string found in website : " + rowData;
                        break;
                    case "MATCHREVIEWSTRING":
                        ////Search for Review Case String on Website
                        isReviewCase = MatchStringsOnWebsite(dataRow, rowData);
                        break;
                    case "MATCHCANCELERRORSTRING":
                        ////Search for Review Case String on Website
                        MatchCancelErrorString(dataRow, rowData);
                        break;
                    case "RESULTTABLECOLUMNNAMES":
                        ////Gets all the result table column details
                        if (this.checkResultTable)
                            GetColumnDetails(dataRow);
                        break;
                    case "EXTRACTHTMLTABLEINTODATATABLE":
                    case "EXTRACTDATATABLE_ROWHEADER":
                    case "EXTRACTDATATABLEFROMXML":
                    case "EXTRACTDATATABLEFROMJSON":
                        ////Extracts the result table from website response
                        if (this.checkResultTable)
                            Session[dataRow.StepType.ToString()] = ExtractResultDataTable(websiteResponse, dataRow);
                        break;
                    case "CHECKRESULTTABLE":
                        ////Performs all the checks specified for a particular column in result table and gives a result
                        CheckResultTable(dataRow, rowData, ref exitIteration);
                        break;
                    case "CLICKABLELINKS":
                        ClickableLinks(dataRow, ref exitIteration);
                        break;
                    case "INCREMENTCOUNTER":
                        counter = counter + Convert.ToInt32(dataRow.ConfigCWIParameter.ToString());
                        break;
                    case "INCREMENTPAGINGCOUNTER":
                        pagingCounter = pagingCounter + Convert.ToInt32(dataRow.ConfigCWIParameter.ToString());
                        break;
                    //case "CHECKCLEARCASE":
                    //    if (isClearCase)
                    //        ////Exiting the iteration
                    //        exitIteration = true;
                    //    break;
                    case "CHECKREVIEWCASE":
                        if (isReviewCase)////Exiting the iteration
                            exitIteration = true;
                        break;
                    case "SWITCHTOFRAMES":
                        SwitchToFrames(driver, dataRow);
                        break;
                    case "WAIT":
                        int waitTime; if (int.TryParse(dataRow.ConfigCWIParameter.ToString(), out waitTime) && waitTime > 0) Thread.Sleep(waitTime);
                        break;
                    case "ADDITIONALSEARCH":
                        AdditionalSearch(ref exitIteration);
                        break;
                    case "CHECKREQEUSTNAMELENGTH":
                        CheckReqeustNameLength(dataRow, this.firstname, this.lastname);
                        break;
                    case "CHECKNUMBEROFROWS":
                        CheckNumberOfRows(dataRow, (DataTable)Session[dataRow.StepType.ToString()]);
                        break;
                    case "NAVIGATE":
                        Navigate(driver, dataRow);
                        break;
                    case "CHECKCELLCONTENTS":
                        CheckCellContents(dataRow, (DataTable)Session[dataRow.StepType.ToString()]);
                        break;
                    case "FALSECLEAR":
                    case "USESSELENIUM":
                    case "BLANK-CHECKPOINT":
                    case "SETFALSECLEARFLAG":
                        //// Do nothing
                        break;
                    case "PERFORMHTTPPOSTING":
                        PerformHTTPPosting(dataRow, rowData);
                        return;
                    case "POSTSTRING":
                        CreatePostString(dataRow, rowData);
                        break;
                    case "CHECKLINK":
                    case "CHECKSPAN":
                    case "CHECKINPUT":
                    case "CHECKBUTTON":
                    case "CHECKDROPDOWN":
                    case "CHECKDROPDOWNELEMENT":
                    case "CHECKDROPDOWNFORNEWVALUE":
                    case "CHECKFORCHECKBOX":
                    case "CHECKFORRADIOBUTTON":
                    case "CHECKCUSTOMISEDNODE":
                    case "CHECKFORCHECKBOXEXACT":
                    case "CHECKFORRADIOBUTTONEXACT":
                        CheckResponse(dataRow, ref exitIteration);
                        break;
                    case "ADDONFUNCTION":
                        AddOnFunction(dataRow);
                        break;
                    case "EXTRACTFROMLINK":
                    case "EXTRACTFROMBUTTON":
                    case "EXTRACTFROMINPUT":
                    case "EXTRACTFROMDROPDOWN":
                    case "EXTRACTFROMFORM":
                    case "EXTRACTFROMIMAGE":
                    case "EXTRACTFROMSPAN":
                    case "EXTRACTFROMSELECT":
                    case "EXTRACTFROMDIV":
                    case "EXTRACTFROMOPTION":
                        ExtractDesiredValueFromResponse(dataRow, rowData);
                        break;
                    case "PERFORMDATEOPERATIONS":
                        PerformDateOperations(dataRow, rowData);
                        break;
                    case "EXTRACTATTRIBUTEFROMNODE":
                        ExtractAttributeFromNode(dataRow, rowData, ref exitIteration);
                        break;
                    case "EXTRACTENUMVALUES":
                        ExtractEnumValues(dataRow, rowData);
                        break;
                    case "CHECKREQUESTPARAMETERS":
                        CheckRequestParameters(dataRow, rowData);
                        break;
                    case "GOTOSPECIFIEDSTEP":
                        GoToSpecifiedStep(dataRow, rowData);
                        break;
                    case "EVALUATEVALUES":
                        EvaluateValues(dataRow, rowData);
                        break;
                    case "URLDECODE":
                        URLDecode(rowData);
                        break;
                    case "REPLACEINSTRING":
                        ReplaceInString(dataRow, rowData);
                        break;
                    case "NUMBERTOWORDS":
                        NumberToWords(rowData);
                        break;
                    case "GLOBALSTRINGLIST":
                        GlobalStringList(dataRow, rowData);
                        break;
                    case "GLOBALCASEDETAILLINKLIST":
                        GlobalCaseDetailLinkList(dataRow, rowData);
                        break;
                    case "INCREMENTGLOBALLISTCOUNTER":
                        IncrementGlobalListCounter();
                        break;
                    case "INCREMENTCASEDETAILLINKCOUNTER":
                        IncrementCaseDetailLinkCounter();
                        break;
                    case "UPDATEHTTPMANAGER":
                        UpdateHTTPManager(dataRow, rowData);
                        break;
                    case "SETERRORMESSAGE":
                        SetErrorMessage(dataRow, rowData);
                        break;
                    case "COMPARETWOVALUES":
                        CompareTwoValues(dataRow, rowData);
                        break;
                    case "GETOCTOPUSPROJECTID":
                        GetOctopusProjectID();
                        break;
                    case "CHECKUNHANDLEDSPECIALCHAR":
                        CheckUnhandledSpecialChar(dataRow);
                        break;
                    case "PERFORMSTRINGOPERATIONS":
                        PerformStringOperations(dataRow, rowData);
                        break;
                    case "GOOGLERECAPTCHAV2":
                        GoogleReCaptchaV2(dataRow);
                        break;
                    case "GOOGLERECAPTCHAV1":
                        GoogleReCaptchaV1(dataRow);
                        break;
                    case "SPLITINTOINNERHTMLLIST":
                        SplitIntoInnerHTMLList(dataRow, rowData);
                        break;
                    case "ASSIGNTORESPONSEPAGELIST":
                        AssignToResponsePageList();
                        break;                    
                    case "DECOUPLINGRULEENGINE":
                        DeCouplingRuleEngine(dataRow, rowData);
                        break;
                    case "DELETECASE":
                        DeleteCase();
                        break;
                    default:
                        this.ErrorString = "Incorrect Step provided for Process Rows : " + dataRow.Step.ToString().ToUpper();
                        break;
                }
            }
            catch (Exception ex)
            {
                this.ErrorString = "Error in ProcessRow: " + ex;
            }
        }

        private void AssignToResponsePageList()
        {
            try
            {
                responsePageList.Add(new ResponsePageInfo()
                {
                    ResponsePage = websiteResponse,
                    ResponsePageName = rowData
                });
            }
            catch (Exception ex)
            {
                this.ErrorString = "Error in AssignToResponsePageList: " + ex;
            }
        }

        private void AdditionalSearch(ref bool exitIteration)
        {
            try
            {
                additionalSearchCounter++;
                if (additionalSearchCounter < firstNameList.Count)
                {
                    firstname = firstNameList[additionalSearchCounter];
                    lastname = lastNameList[additionalSearchCounter];
                    ////Reset all dependant variables
                    exitIteration = false;
                    this.ResultTableColumnNames = new List<ResultTableColumns>();
                    FirstRowText = string.Empty;
                    clickableLinks = new List<string>();
                    innerHtmlValueList = new List<string>();
                    postString = string.Empty;
                    saveResults = new List<bool>();
                    //if (isClearCase || ResponseModel.Screening[0].ScreeningResponse.Cases.Count > 0)
                    //{
                    //    this.isClearCase = false;
                    //}
                    //else
                    //{
                    //    this.ErrorString = "Clear case flag was not set before resetting. Please check your steps";
                    //}
                    functionresult = string.Empty;
                }
                else
                {
                    ////Set CLEAR as all the additional search are over                            
                    if (hasPagingOrMultilpleLink)
                    {
                        functionresult = "CONTINUE_AND_EXITLOOP";
                    }
                    else
                    {
                        exitIteration = true;
                        return;
                    }
                }
            }
            catch (Exception ex)
            {
                this.ErrorString = "Error in AdditionalSearch: " + ex;
            }
        }

        private void ClickableLinks(ConfigurableCWIStepsModel dataRow, ref bool exitIteration)
        {
            try
            {
                if (dataRow.Verify.ToString().ToUpper() == "NO")
                {
                    string parameter = updatedParameters;
                    ElementOperations(driver, "CLICK", dataRow.StepType.ToString(), parameter, rowData);
                    ////Reset the data of first row of result table
                    this.FirstRowText = string.Empty;
                }
                else if (counter < clickableLinks.Count)
                {
                    ////Get the Parameter and format the same as per the current counter
                    string parameter = dataRow.ConfigCWIParameter.ToString();
                    parameter = parameter.Replace("COUNTER", counter.ToString());
                    parameter = clickableLinks[counter];
                    ElementOperations(driver, "CLICK", dataRow.StepType.ToString(), parameter, rowData);
                    ////Reset the data of first row of result table
                    this.FirstRowText = string.Empty;
                }
                else
                {
                    ////As all the clickable links are over, Return Clear Status
                    ////Exiting the iteration
                    //// isClearCase = true;
                    exitIteration = true;
                }
            }
            catch (Exception ex)
            {
                this.ErrorString = "Error in ClickableLinks: " + ex;
            }
            throw new NotImplementedException();
        }

        private string ReplacePlaceHolders(string stringToReplace)
        {
            try
            {
                return stringToReplace.Replace("PH_FIRSTNAME", this.firstname)
                           .Replace("PH_LASTNAME", this.lastname)
                           .Replace("PH_DOB", dateOfBirth)
                           .Replace("PH_CURRENTDATE", curentDate)
                           .Replace("PH_COUNTER", counter.ToString(counterFormat))
                           .Replace("PH_FUNCTIONRESULT", functionresult)
                           .Replace("PH_COUNTY", ResponseModel.Screening.First().ScreeningRequest.County)
                           .Replace("PH_REGION", ResponseModel.Screening.First().ScreeningRequest.Region)
                           .Replace("PH_COURTNAME", ResponseModel.Screening.First().ScreeningRequest.CourtName)
                           .Replace("PH_SSN", formattedSSN)
                           .Replace("PH_PAGINGCOUNTER", pagingCounter.ToString(counterFormat))
                           .Replace("PH_POSTSTRING", postString)
                           .Replace("PH_WEBSITERESPONSE", websiteResponse);
            }
            catch (Exception ex)
            {
                this.ErrorString = "Error in ReplacePlaceHolders: " + ex.Message;
                throw new Exception(this.ErrorString);
            }
        }

        private void DeleteCase()
        {
            try
            {
                ResponseModel.Screening[0].ScreeningResponse.Cases.RemoveAll(x => x.CaseNumber.ToUpper() == "DELETE");
            }
            catch (Exception ex)
            {
                this.ErrorString = "Error in DeCouplingRuleEngine: " + ex;
                ResponseModel.Screening[0].ScreeningResponse.Cases[counter].CaseServedStatus = OrderStatus.Retry;
            }
        }

        private void DeCouplingRuleEngine(ConfigurableCWIStepsModel dataRow, string rowData)
        {
            try
            {
                ResponseData responseData = new ResponseData()
                {
                    CaseIdentifier = rowData,
                    ResponsePageList = responsePageList
                };

                PersonName person = new PersonName()
                {
                    FirstName = ResponseModel.Screening[0].ScreeningResponse.Cases[counter].ExtraInfo.EvaluateValues("<FN>", "</FN>"),
                    LastName = ResponseModel.Screening[0].ScreeningResponse.Cases[counter].ExtraInfo.EvaluateValues("<LN>", "</LN>"),
                    DateOfBirth = dateOfBirth
                };


                //// Extraction
                ExtractionLibrary extractionLibrary = new ExtractionLibrary()
                {
                    ExtractionStepList = ConfigCWIModel.ExtractionSteps,
                    FieldConfigList = ConfigCWIModel.FieldConfigurations,
                    ResponseData = responseData,
                    ResponseModel = ResponseModel
                };

                // Extract response data
                extractionLibrary.ExtractResponseData();

                // Save Extracted info
                HTMLlogs.HtmlLogFile.Add("ExtractedData_CaseNumber_"+ ResponseModel.Screening[0].ScreeningResponse.Cases[counter].CaseNumber,
                                         Newtonsoft.Json.JsonConvert.SerializeObject(extractionLibrary.SectionDataList));

                //Check for error
                if (!string.IsNullOrWhiteSpace(ResponseModel.ErrorDescription))
                {
                    this.ErrorString = ResponseModel.ErrorDescription;
                    ResponseModel.Screening[0].ScreeningResponse.Cases[counter].CaseServedStatus = OrderStatus.Retry;
                    return;
                }

#if (DEBUG)
              //  File.WriteAllText(@"C:\Responses\ExtractedData\ExtractedInfo.json", Newtonsoft.Json.JsonConvert.SerializeObject(extractionLibrary.SectionDataList));
#endif 

                //// Case Parsing
                CaseParserLibrary caseParserLibrary = new CaseParserLibrary()
                {
                    ResponseModel = ResponseModel,
                    SectionDataList = extractionLibrary.SectionDataList,
                    RuleEngineConfigurations = ConfigCWIModel.RuleEngineSteps,
                    FilterList = ConfigCWIModel.DataFilters,
                    MappingList = ConfigCWIModel.SourceFieldMappings,
                    ScreeningResponseCase = ResponseModel.Screening[0].ScreeningResponse.Cases[counter],
                    EnumValues = EnumValues,
                    ConfigDetail = ConfigDetail,
                    CurrentRequestName = person
                };

                // Parse Extracted data to CaseModel
                caseParserLibrary.ParseToCaseModel();

                // Save parsed info
                HTMLlogs.HtmlLogFile.Add("ParsedCaseModel_CaseNumber_" + ResponseModel.Screening[0].ScreeningResponse.Cases[counter].CaseNumber,
                                         Newtonsoft.Json.JsonConvert.SerializeObject(caseParserLibrary.CaseModel));

                //Check for error
                if (!string.IsNullOrWhiteSpace(ResponseModel.ErrorDescription))
                {
                    this.ErrorString = ResponseModel.ErrorDescription;
                    ResponseModel.Screening[0].ScreeningResponse.Cases[counter].CaseServedStatus = OrderStatus.Retry;
                    return;
                }
#if (DEBUG)
                //File.WriteAllText(@"C:\Responses\FinalData\CaseModel.json", Newtonsoft.Json.JsonConvert.SerializeObject(caseParserLibrary.CaseModel));
#endif 
                //// Rule Engine
                RuleEngineMaster.RuleEngineLibrary ruleEngineLibrary = new RuleEngineMaster.RuleEngineLibrary
                {
                    CaseModel = caseParserLibrary.CaseModel,
                    ConfigDetail = ConfigDetail,
                    EnumValues = EnumValues,
                    ResponseModel = ResponseModel,
                    RuleEngineStepsModels = ConfigCWIModel.RuleEngineSteps,
                    CurrentRequestInfo = person,
                    ScreeningResponseCase = ResponseModel.Screening[0].ScreeningResponse.Cases[counter]
                };

                // Apply Rules on parsed data
                ruleEngineLibrary.ApplyRulesToCaseModel();

                // Save final parsed info
                HTMLlogs.HtmlLogFile.Add("FinalParsedCaseModel_CaseNumber_" + ResponseModel.Screening[0].ScreeningResponse.Cases[counter].CaseNumber,
                                         Newtonsoft.Json.JsonConvert.SerializeObject(ruleEngineLibrary.CaseModel));

                //Check for error
                if (!string.IsNullOrWhiteSpace(ResponseModel.ErrorDescription))
                {
                    this.ErrorString = ResponseModel.ErrorDescription;
                    ResponseModel.Screening[0].ScreeningResponse.Cases[counter].CaseServedStatus = OrderStatus.Retry;
                    return;
                }

                //// Assigning Rule Engine case to original Case and setting case fulfilled status                
                ResponseModel.Screening[0].ScreeningResponse.Cases[counter].CaseServedStatus = OrderStatus.Fulfilled;
            }
            catch (Exception ex)
            {
                this.ErrorString = "Error in DeCouplingRuleEngine: " + ex;
                ResponseModel.Screening[0].ScreeningResponse.Cases[counter].CaseServedStatus = OrderStatus.Retry;
            }
        }

        private void GoogleReCaptchaV1(ConfigurableCWIStepsModel dataRow)
        {
            string prismApiUser = string.Empty;
            string prismAPIPassword = string.Empty;
            string captchaURL = string.Empty;
            try
            {
                functionresult = string.Empty;
                if (ConfigDetail.Any(x => x.FieldName == "PrismApiUser") && ConfigDetail.Any(x => x.FieldName == "PrismAPIPassword") && ConfigDetail.Any(x => x.FieldName == "CaptchaURL"))
                {
                    prismApiUser = ConfigDetail.Where(x => x.FieldName == "PrismApiUser").First().FieldValue;
                    prismAPIPassword = ConfigDetail.Where(x => x.FieldName == "PrismAPIPassword").First().FieldValue;
                    captchaURL = ConfigDetail.Where(x => x.FieldName == "CaptchaURL").First().FieldValue;
                }
                else
                {
                    this.ErrorString = "Prism API Username or Password or CaptchaURL not found in ConfigDetails";
                    return;
                }
                using (var client = new HttpClient())
                {
                    client.DefaultRequestHeaders.Accept.Clear();
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                    HttpRequestMessage httpRequest = new HttpRequestMessage(HttpMethod.Get, captchaURL);
                    client.DefaultRequestHeaders.Authorization =
                                            new AuthenticationHeaderValue("Basic", Convert.ToBase64String(System.Text.Encoding.UTF8.GetBytes(prismApiUser + ":" + prismAPIPassword)));
                    var result = client.SendAsync(httpRequest).Result;
                    string resultContent = result.Content.ReadAsStringAsync().Result;
                    if (!(result.StatusCode == HttpStatusCode.OK) || resultContent == string.Empty || resultContent.ToUpper().Trim() == "TIMEOUT" || resultContent.ToUpper().Trim() == "ERROR-API")
                    {
                        this.ErrorString = "Error in Captcha Web API : " + result.StatusCode.ToString();
                        return;
                    }

                    string[] separator = new string[1];
                    separator[0] = ",";
                    string[] tempArr =
                        resultContent.Replace("[", string.Empty)
                            .Replace("]", string.Empty)
                            .Replace("\"", String.Empty)
                            .Split(separator, StringSplitOptions.None);
                    functionresult = "<CaptchaKey>" + tempArr[0] + "</CaptchaKey><CaptchaImage>" + tempArr[1] + "</CaptchaImage>";
                    return;
                }
            }
            catch (Exception ex)
            {
                this.ErrorString = "Error in GoogleReCaptchaV2: " + ex;
            }

        }

        private void GoogleReCaptchaV2(ConfigurableCWIStepsModel dataRow)
        {
            string captchaResponse = string.Empty;
            string prismApiUser = string.Empty;
            string prismAPIPassword = string.Empty;
            string CapchaURL = string.Empty;
            functionresult = string.Empty;
            try
            {
                if (ConfigDetail.Any(x => x.FieldName == "PrismApiUser") && ConfigDetail.Any(x => x.FieldName == "PrismAPIPassword"))
                {
                    prismApiUser = ConfigDetail.Where(x => x.FieldName == "PrismApiUser").First().FieldValue;
                    prismAPIPassword = ConfigDetail.Where(x => x.FieldName == "PrismAPIPassword").First().FieldValue;
                    CapchaURL = ConfigDetail.Where(x => x.FieldName == "CapchaURL").First().FieldValue;
                }
                else
                {
                    this.ErrorString = "Prism API Username or Password not found in ConfigDetails";
                    return;
                }
                logger.Info(Environment.MachineName + " - " + ResponseModel.Screening[0].ScreeningRequest.Vendor + "_Screening:" + this.screeningID + "Call made to GetCapchaText Function");
                WebClient webClient = new WebClient();
                webClient.Headers[HttpRequestHeader.ContentType] = "application/json";
                string credentials = Convert.ToBase64String(System.Text.Encoding.UTF8.GetBytes(prismApiUser + ":" + prismAPIPassword));
                webClient.Headers[HttpRequestHeader.Authorization] = string.Format("Basic {0}", credentials);

                captchaResponse = webClient.DownloadString(CapchaURL);
                captchaResponse = captchaResponse.Substring(1, captchaResponse.Length - 2);

                if (string.IsNullOrWhiteSpace(captchaResponse) || captchaResponse.Trim().ToUpper() == "TIMEOUT" || captchaResponse.Trim().ToUpper() == "ERROR-API")
                {
                    this.ErrorString = "Error in GetCapchaText";
                    ResponseModel.ErrorCode = ErrorCode.CaptchaExpired;
                    ResponseModel.ErrorDescription = "Error in GetCapchaText";
                    ResponseModel.Status = OrderStatus.Retry;
                    logger.Error(Environment.MachineName + " - " + ResponseModel.Screening[0].ScreeningRequest.Vendor + "_Screening:" + this.screeningID + "-Result is-" + ResponseModel.ErrorCode.ToString() + "~" + ResponseModel.ErrorDescription.ToString());
                    return;
                }
                functionresult = captchaResponse;

            }
            catch (Exception ex)
            {
                this.ErrorString = "Error in GoogleReCaptchaV2: " + ex;
            }
        }

        private void PerformStringOperations(ConfigurableCWIStepsModel dataRow, string rowData)
        {
            string inputString = string.Empty;
            try
            {
                switch (dataRow.StepType.ToString().ToUpper())
                {
                    case "FUNCTIONRESULT":
                        inputString = functionresult;
                        break;
                    case "WEBSITERESPONSE":
                        inputString = websiteResponse;
                        break;
                    case "POSTSTRING":
                        inputString = postString;
                        break;
                    case "FIRSTNAME":
                        inputString = firstname;
                        break;
                    case "LASTNAME":
                        inputString = lastname;
                        break;
                    default:
                        this.ErrorString = "Incorrect StepType provided for PerformStringOperations : " + dataRow.StepType.ToString().ToUpper();
                        return;
                }
                functionresult = string.Empty;
                switch (dataRow.ConfigCWIParameter.ToString().ToUpper().Trim())
                {
                    case "TOUPPER":
                        functionresult = inputString.ToUpper();
                        break;
                    case "TRIM":
                        functionresult = inputString.Trim();
                        break;
                    default:
                        this.ErrorString = "Incorrect Step provided for PerformStringOperations : " + dataRow.Step.ToString().ToUpper();
                        break;
                }
            }
            catch (Exception ex)
            {
                this.ErrorString = "Error in PerformStringOperations: " + ex;
            }
        }

        private void GetOctopusProjectID()
        {
            try
            {
                var data = JObject.Parse(websiteResponse);
                functionresult = data.SelectToken("Id").ToString();
            }
            catch (Exception ex)
            {
                this.ErrorString = "Error in GetOctopusProjectID: " + ex;
            }
        }

        private void CompareTwoValues(ConfigurableCWIStepsModel dataRow, string rowData)
        {
            try
            {
                switch (dataRow.StepType.ToString().ToUpper())
                {
                    case "COMPARESTRING":
                        functionresult = string.Equals(updatedParameters, rowData, StringComparison.OrdinalIgnoreCase).ToString().ToUpper();
                        break;
                    case "COMPAREINTEGER":
                        CompareInteger(dataRow, rowData);
                        break;
                    //case "CompareDate":
                    //    CompareDate(dataRow, rowData);
                    //    break;
                    default:
                        this.ErrorString = "Incorrect Step provided for CompareTwoValues : " + dataRow.Step.ToString().ToUpper();
                        break;
                }
            }
            catch (Exception ex)
            {
                this.ErrorString = "Error in CompareTwoValues: " + ex;
            }
        }

        private void CompareInteger(ConfigurableCWIStepsModel dataRow, string rowData)
        {
            int parameter;
            int data;
            try
            {
                if (!int.TryParse(updatedParameters, out parameter))
                {
                    this.ErrorString = "Parameter passed for CompareInteger is not in Integer format";
                }
                if (!int.TryParse(rowData, out data))
                {
                    this.ErrorString = "Data passed for CompareInteger is not in Integer format";
                }
                if (parameter == data)
                {
                    functionresult = "TRUE";
                }
                else if (parameter < data)
                {
                    functionresult = "PARAMETER_LESS_THAN_DATA";
                }
                else
                {
                    functionresult = "PARAMETER_GREATER_THAN_DATA";
                }
            }
            catch (Exception ex)
            {
                this.ErrorString = "Error in CompareInteger: " + ex;
            }
        }

        private void SetErrorMessage(ConfigurableCWIStepsModel dataRow, string rowData)
        {
            try
            {
                switch (dataRow.StepType.ToString().ToUpper())
                {
                    case "WEBSITEERROR":
                        ResponseModel.ErrorCode = ErrorCode.HttpConnectionFailure;
                        break;
                    default:
                        this.ErrorString = "Incorrect StepType provided for SetErrorMessage : " + dataRow.Step.ToString().ToUpper();
                        break;
                }
                switch (dataRow.ConfigCWIParameter.ToString().ToUpper())
                {
                    case "RETRY":
                        ResponseModel.Status = OrderStatus.Retry;
                        break;
                    case "CANCEL":
                        ResponseModel.Status = OrderStatus.Cancelled;
                        break;
                    default:
                        this.ErrorString = "Incorrect Parameters provided for SetErrorMessage : " + dataRow.Step.ToString().ToUpper();
                        break;
                }
                if (!string.IsNullOrEmpty(rowData))
                {
                    this.ErrorString = rowData;
                }
                else
                {
                    this.ErrorString = "SetErrorMessage called but RowData is Empty";
                }
            }
            catch (Exception ex)
            {
                this.ErrorString = "Error in SetErrorMessage: " + ex;
            }
        }

        private void UpdateHTTPManager(ConfigurableCWIStepsModel dataRow, string rowData)
        {
            try
            {
                switch (dataRow.StepType.ToString().ToUpper())
                {
                    case "ADDHEADER":
                        if (HttpManager.Headers == null)
                        {
                            HttpManager.Headers = new List<string>();
                        }
                        HttpManager.Headers.Add(rowData);
                        break;
                    case "CONTENT-TYPE":
                        HttpManager.ContentType = rowData;
                        break;
                    case "REFERRER":
                        HttpManager.Referer = rowData;
                        break;
                    case "ENABLEHTTPSSECURITYPROTOCOL":
                        //HttpManager.EnableHttpsSecurityProtocol();
                        ServicePointManager.SecurityProtocol = SecurityProtocolType.Ssl3 | SecurityProtocolType.Tls | SecurityProtocolType.Tls11 | SecurityProtocolType.Tls12;
                        break;
                    case "ENABLESERVERCERTIFICATE":
                        ServicePointManager.ServerCertificateValidationCallback = (sender, cert, chain, sslPolicyErrors) => true;
                        break;
                    case "WAITTIMEBEFOREEVERYPOSTING":
                        HttpManager.WaitTimeBeforeEveryPosting = Convert.ToInt32(rowData);
                        break;
                    case "PROXYIPADDRESS":
                        //// TODO: To be done when an integration uses this functionality
                        //// HttpManager.ProxyIPAddress = rowData;                        
                        break;
                    case "REMOVEEXCEPT100CONTINUE":
                        HttpManager.isExcept100ContinueOn = false;
                        break;
                    case "ACCEPTHEADERVALUE":
                        HttpManager.AcceptHeaderValue = rowData;
                        break;
                    case "USERAGENT":
                        HttpManager.UserAgent = rowData;
                        break;
                    case "PROTOCOLVERSION10":
                        HttpManager.isProtocolVersion10 = true;
                        break;
                    case "GETRESPONSEURL":
                        functionresult = HttpManager.ResponseURL;
                        break;
                    case "ASSIGNRESPONSEURL":
                        HttpManager.URL = HttpManager.ResponseURL;
                        break;
                    case "SETURL":
                        HttpManager.URL = functionresult;
                        break;
                    default:
                        this.ErrorString = "Incorrect StepType provided for UpdateHTTPManager : " + dataRow.StepType.ToString().ToUpper();
                        break;
                }
            }
            catch (Exception ex)
            {
                this.ErrorString = "Error in UpdateHTTPManager: " + ex;
            }
        }

        private void CheckResultTable(ConfigurableCWIStepsModel dataRow, string rowData, ref bool exitIteration)
        {
            try
            {
                if (this.checkResultTable && this.continueResultAnalysis)
                {
                    CheckTable((DataTable)Session[dataRow.StepType.ToString()], isReviewCase, clearStringFound, dataRow);
                }
                if (this.ErrorString.Trim().ToUpper() == "CLEAR")
                {
                    ////Resetting the error string as paging will happen
                    this.ErrorString = string.Empty;
                    ////Setting the isClearCase and performPaging variable as true
                    //// isClearCase = true;
                    performPaging = true;
                }
                else if (this.ErrorString.Trim().ToUpper() == "REVIEW")
                {
                    if (this.continuePosting == true)
                    {
                        ////Resetting the error string as further postings are pending
                        this.ErrorString = string.Empty;
                    }
                    else
                    {
                        isReviewCase = true;
                        this.ErrorString = string.Empty;
                    }
                }
                else if (this.ErrorString.Trim().ToUpper() == "SAME PAGE" && performPaging == true)
                {
                    ////Check the error string for same result page to stop paging posting
                    ////Resetting the error string as paging is completed
                    this.ErrorString = string.Empty;
                    ////Exiting the iteration
                    //// isClearCase = true;
                    exitIteration = true;
                }
            }
            catch (Exception ex)
            {
                this.ErrorString = "Error in CheckResultTable: " + ex;
            }
        }

        public bool CheckISRegexMatch(string columnData, string patternToBeMatched, string format)
        {
            try
            {
                List<string> formats = new List<string>();
                formats.AddRange(format.ToUpper().Split('|').ToList());
                if (formats.Contains("DO_NOT_IGNORE_CASE"))
                {
                    if (Regex.IsMatch(columnData.Trim(), patternToBeMatched.Trim()))
                        return true;
                }
                else if (formats.Contains("CANCEL_CONDITION"))
                {
                    if (Regex.IsMatch(columnData.Trim(), patternToBeMatched.Trim(), RegexOptions.IgnoreCase))
                    {
                        this.ErrorString = "CANCEL: Row Data provided is not in specified format";
                        ResponseModel.ErrorDescription = this.ErrorString;
                        ResponseModel.ErrorCode = ErrorCode.HttpConnectionFailure;
                        ResponseModel.Status = OrderStatus.Cancelled;
                        return false;
                    }
                }
                else
                {
                    if (Regex.IsMatch(columnData.Trim(), patternToBeMatched.Trim(), RegexOptions.IgnoreCase))
                        return true;
                }

                return false;
            }
            catch (Exception ex)
            {
                this.ErrorString = "Error in checkISRegexMatch: " + ex;
                return false;
            }
        }

        private bool MatchStringsOnWebsite(ConfigurableCWIStepsModel dataRow, string rowData)
        {
            bool isFirstString = true;
            bool isStringsPresent = false;
            string inputString = websiteResponse;
            bool isExactMatch = false;
            try
            {
                if (string.IsNullOrWhiteSpace(rowData))
                {
                    this.ErrorString = "Row Data not provided for Checking string on Website";
                    return false;
                }

                if (dataRow.StepType.ToString().ToUpper().Trim() == "FUNCTIONRESULT")
                {
                    inputString = functionresult;
                }
                if (dataRow.ConfigCWIParameter.ToString().ToUpper().Trim() == "EXACT_MATCH")
                {
                    isExactMatch = true;
                }
                List<string> stringsToBeChecked = rowData.Split('|').ToList();

                foreach (string checkString in stringsToBeChecked)
                {
                    if (isFirstString)
                    {
                        if (isExactMatch)
                        {
                            isStringsPresent = inputString.ToUpper().Equals(checkString.ToUpper());
                        }
                        else
                        {
                            isStringsPresent = inputString.ToUpper().Contains(checkString.ToUpper());
                        }
                        isFirstString = false;
                    }
                    else if (dataRow.Logic != null && dataRow.Logic.ToString().ToUpper() == "AND")
                    {
                        if (isExactMatch)
                        {
                            isStringsPresent = isStringsPresent && inputString.ToUpper().Equals(checkString.ToUpper());
                        }
                        else
                        {
                            isStringsPresent = isStringsPresent && inputString.ToUpper().Contains(checkString.ToUpper());
                        }
                    }
                    else if (dataRow.Logic != null && dataRow.Logic.ToString().ToUpper() == "OR")
                    {
                        if (isExactMatch)
                        {
                            isStringsPresent = isStringsPresent || inputString.ToUpper().Equals(checkString.ToUpper());
                        }
                        else
                        {
                            isStringsPresent = isStringsPresent || inputString.ToUpper().Contains(checkString.ToUpper());
                        }
                    }
                    else
                    {
                        this.ErrorString = "Logic not provided for Multiple Strings Verification on Website";
                        return false;
                    }
                }
                return isStringsPresent;
            }
            catch (Exception ex)
            {
                this.ErrorString = "Error in MatchStringsOnWebsite: " + ex;
                return false;
            }
        }

        private void MatchCancelErrorString(ConfigurableCWIStepsModel dataRow, string rowData)
        {
            try
            {
                if (MatchStringsOnWebsite(dataRow, rowData))
                {
                    ResponseModel.Status = OrderStatus.Cancelled;
                    switch (dataRow.ConfigCWIParameter.ToString().ToUpper())
                    {
                        case "464":
                            this.ErrorString = "CANCEL With Error Code " + dataRow.ConfigCWIParameter.ToString().ToUpper() + ". Error string found in website : " + rowData;
                            ResponseModel.ErrorCode = ErrorCode.DataUnavailableAtSource;
                            break;
                        case "477":
                            this.ErrorString = "CANCEL with Error Code " + dataRow.ConfigCWIParameter.ToString().ToUpper() + ". Error string found in website : " + rowData;
                            ResponseModel.ErrorCode = ErrorCode.DateFormatError;
                            break;
                        case "484":
                            this.ErrorString = "CANCEL with Error Code " + dataRow.ConfigCWIParameter.ToString().ToUpper() + ". Error string found in website : " + rowData;
                            ResponseModel.ErrorCode = ErrorCode.DefendentNameLimitExceeded;
                            break;
                        case "602":
                            this.ErrorString = "CANCEL with Error Code " + dataRow.ConfigCWIParameter.ToString().ToUpper() + ". Error string found in website : " + rowData;
                            ResponseModel.ErrorCode = ErrorCode.RunTimeError;
                            break;
                        case "462":
                            this.ErrorString = "CANCEL with Error Code " + dataRow.ConfigCWIParameter.ToString().ToUpper() + ". Error string found in website : " + rowData;
                            ResponseModel.ErrorCode = ErrorCode.LoginFailure;
                            break;
                        case "485":
                            this.ErrorString = "CANCEL with Error Code " + dataRow.ConfigCWIParameter.ToString().ToUpper() + ". Error string found in website : " + rowData;
                            ResponseModel.ErrorCode = ErrorCode.SSNFormatError;
                            break;
                        case "476":
                            this.ErrorString = "CANCEL with Error Code " + dataRow.ConfigCWIParameter.ToString().ToUpper() + ". Error string found in website : " + rowData;
                            ResponseModel.ErrorCode = ErrorCode.UnhandledSpecialCharacter;
                            break;
                        default:
                            this.ErrorString = "CANCEL with Error Code " + dataRow.ConfigCWIParameter.ToString().ToUpper() + ". Error string found in website : " + rowData;
                            ResponseModel.ErrorCode = ErrorCode.HttpConnectionFailure;
                            break;
                    }
                }
            }
            catch (Exception ex)
            {
                this.ErrorString = "Error in MatchCancelErrorString: " + ex;
            }
        }

        private void CheckUnhandledSpecialChar(ConfigurableCWIStepsModel dataRow)
        {
            string specialCharToBeChecked = string.Empty;
            try
            {
                switch (dataRow.ConfigCWIParameter.ToString().ToUpper())
                {
                    case "DASH":
                        specialCharToBeChecked = "-";
                        break;
                    case "APOSTROPHE":
                        specialCharToBeChecked = "'";
                        break;
                    case "PERIOD":
                        specialCharToBeChecked = ".";
                        break;
                    case "SPACE":
                        specialCharToBeChecked = " ";
                        break;
                    case "ANY":
                        if (firstNameList.Count > 1)
                        {
                            this.ErrorString = "CANCEL with Error Code 476 : Website does not accept Special Characters";
                            ResponseModel.ErrorCode = ErrorCode.UnhandledSpecialCharacter;
                            ResponseModel.Status = OrderStatus.Cancelled;
                        }
                        return;
                    default:
                        this.ErrorString = "Error in CheckUnhandledSpecialChar: Incorrect parameter provided";
                        break;
                }

                switch (dataRow.StepType.ToString().ToUpper())
                {
                    case "FIRSTNAME":
                        if (firstNameList.Any(name => name.Contains(specialCharToBeChecked)))
                        {
                            this.ErrorString = "CANCEL with Error Code 476 : Unhandled Spc. character '" + dataRow.ConfigCWIParameter.ToString() + "' present in FirstName";
                            ResponseModel.ErrorCode = ErrorCode.UnhandledSpecialCharacter;
                            ResponseModel.Status = OrderStatus.Cancelled;
                        }
                        break;
                    case "LASTNAME":
                        if (lastNameList.Any(name => name.Contains(specialCharToBeChecked)))
                        {
                            this.ErrorString = "CANCEL with Error Code 476 : Unhandled Spc. character '" + dataRow.ConfigCWIParameter.ToString() + "' present in LastName";
                            ResponseModel.ErrorCode = ErrorCode.UnhandledSpecialCharacter;
                            ResponseModel.Status = OrderStatus.Cancelled;
                        }
                        break;
                    case "FULLNAME":
                        if (firstNameList.Any(name => name.Contains(specialCharToBeChecked)) || lastNameList.Any(name => name.Contains(specialCharToBeChecked)))
                        {
                            this.ErrorString = "CANCEL with Error Code 476 : Unhandled Spc. character '" + dataRow.ConfigCWIParameter.ToString() + "' present in the request name";
                            ResponseModel.ErrorCode = ErrorCode.UnhandledSpecialCharacter;
                            ResponseModel.Status = OrderStatus.Cancelled;
                        }
                        break;
                    default:
                        this.ErrorString = "Error in CheckUnhandledSpecialChar: Incorrect StepType provided for CheckUnhandledSpecialChar : " + dataRow.StepType.ToString().ToUpper();
                        break;
                }

            }
            catch (Exception ex)
            {
                this.ErrorString = "Error in CheckUnhandledSpecialChar: " + ex;
            }
        }
        /// <summary>
        /// Validates the string is present in the website response or not
        /// </summary>
        /// <param name="dataRow">Single row of Config QC Step Containing details of ValidatePage</param>
        /// <param name="rowData">The string to be checked in website response</param>
        private void ValidatePage(ConfigurableCWIStepsModel dataRow, string rowData)
        {
            try
            {
                if (string.IsNullOrWhiteSpace(rowData))
                {
                    this.ErrorString = "Row Data not provided for Proper Page Check";
                    return;
                }
                if (dataRow.StepType.ToString().ToUpper() == "FUNCTIONRESULT")
                {
                    functionresult = string.Empty;
                    functionresult = websiteResponse.ToUpper().Contains(rowData.ToUpper()).ToString();
                }
                else
                {
                    isValidPage = websiteResponse.ToUpper().Contains(rowData.ToUpper());
                    if (!isValidPage)
                    {
                        ////Check if the website has multiple page as response
                        if (!this.hasMultipleResultPage)
                        {
                            this.ErrorString = "Proper Page not Found for string: " + rowData;
                            ResponseModel.ErrorDescription = this.ErrorString;
                            ResponseModel.ErrorCode = ErrorCode.HttpConnectionFailure;
                            ResponseModel.Status = OrderStatus.Retry;
                            return;
                        }
                        else
                        {
                            this.checkResultTable = false;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                this.ErrorString = "Error in ValidatePage: " + ex;
            }
        }

        private void IncrementGlobalListCounter()
        {
            try
            {
                //// increment Global List Counter
                globalStringListCounter = globalStringListCounter + 1;

                if (globalStringList.Count > globalStringListCounter)
                {
                    functionresult = globalStringList[globalStringListCounter];
                }
                else
                {
                    functionresult = "END OF LIST";
                }
            }
            catch (Exception ex)
            {
                this.ErrorString = "Error in IncrementGlobalListCounter: " + ex;
            }
        }

        private void GlobalStringList(ConfigurableCWIStepsModel dataRow, string rowData)
        {
            globalStringList = new List<string>();
            globalStringListCounter = -1;
            try
            {
                switch (dataRow.StepType.ToString().ToUpper())
                {
                    case "COMMA":
                        globalStringList = rowData.Split(',').ToList();
                        break;
                    case "PIPE":
                        globalStringList = rowData.Split('|').ToList();
                        break;
                    case "TILDE":
                        globalStringList = rowData.Split('~').ToList();
                        break;
                    default:
                        this.ErrorString = "Incorrect StepType provided for GlobalStringList : " + dataRow.StepType.ToString().ToUpper();
                        break;
                }
            }
            catch (Exception ex)
            {
                this.ErrorString = "Error in GlobalStringList: " + ex;
            }
        }

        private void IncrementCaseDetailLinkCounter()
        {
            try
            {
                //// increment Global List Counter
                globalCaseDetailLinkCounter = globalCaseDetailLinkCounter + 1;

                if (globalCaseDetailLinkList.Count > globalCaseDetailLinkCounter)
                {
                    functionresult = globalCaseDetailLinkList[globalCaseDetailLinkCounter];
                }
                else
                {
                    functionresult = "END OF LIST";
                }
            }
            catch (Exception ex)
            {
                this.ErrorString = "Error in globalCaseDetailLinkCounter: " + ex;
            }
        }

        private void GlobalCaseDetailLinkList(ConfigurableCWIStepsModel dataRow, string rowData)
        {
            globalCaseDetailLinkList = new List<string>();
            globalCaseDetailLinkCounter = -1;
            try
            {
                switch (dataRow.StepType.ToString().ToUpper())
                {
                    case "COMMA":
                        globalCaseDetailLinkList = rowData.Split(',').ToList();
                        break;
                    case "PIPE":
                        globalCaseDetailLinkList = rowData.Split('|').ToList();
                        break;
                    case "TILDE":
                        globalCaseDetailLinkList = rowData.Split('~').ToList();
                        break;
                    default:
                        this.ErrorString = "Incorrect StepType provided for GlobalCaseDetailLinkList : " + dataRow.StepType.ToString().ToUpper();
                        break;
                }
            }
            catch (Exception ex)
            {
                this.ErrorString = "Error in GlobalCaseDetailLinkList: " + ex;
            }
        }

        private void AddOnFunction(ConfigurableCWIStepsModel dataRow)
        {
            bool isMethodPresent = false;
            string methodName = string.Empty;
            ArrayList additionalParameters = new ArrayList();
            try
            {
                ////Call the Add-on function of AddOnFulfillment class                
                Assembly loadedDLL = Assembly.LoadFrom(addOnDllPath);
                Type rtsObj = loadedDLL.GetType("PRISMConfigAdaptor.AddOnFulfillment");
                object obj = Activator.CreateInstance(rtsObj);

                ////Check the method name is present in the dll or not
                foreach (var method in rtsObj.GetMethods())
                {
                    if (method.Name.Trim().ToUpper() == dataRow.ConfigCWIParameter.ToString().Trim().ToUpper())
                    {
                        methodName = method.Name;
                        isMethodPresent = true;
                        break;
                    }
                }

                if (isMethodPresent)
                {
                    obj.GetType().GetProperty("ResponseModel").SetValue(obj, ResponseModel, null);
                    obj.GetType().GetProperty("HTMLlogs").SetValue(obj, HTMLlogs, null);
                    obj.GetType().GetProperty("HttpManager").SetValue(obj, HttpManager, null);
                    obj.GetType().GetProperty("ConfigDetail").SetValue(obj, ConfigDetail, null);
                    obj.GetType().GetProperty("EnumValues").SetValue(obj, EnumValues, null);
                    ////Reset the functionresult
                    functionresult = string.Empty;
                    switch (dataRow.StepType.ToString().ToUpper())
                    {
                        case "ADDITIONALPARAMETERS":
                            additionalParameters.Add(this.firstname);
                            additionalParameters.Add(this.lastname);
                            additionalParameters.Add(functionresult);

                            functionresult = rtsObj.InvokeMember(methodName, BindingFlags.InvokeMethod | BindingFlags.Public | BindingFlags.Instance,
                                null, obj, new object[] { websiteResponse, dataRow, firstNameList, lastNameList, additionalParameters }).ToString();
                            break;
                        default:
                            functionresult = rtsObj.InvokeMember(methodName, BindingFlags.InvokeMethod | BindingFlags.Public | BindingFlags.Instance,
                        null, obj, new object[] { websiteResponse, dataRow, firstNameList, lastNameList }).ToString();
                            break;
                    }
                }
                else
                {
                    this.ErrorString = "Method ("+dataRow.ConfigCWIParameter.ToString().Trim().ToUpper()+") not found in Reflecting DLL";
                    return;
                }

                if (functionresult.ToUpper().Trim().StartsWith("ERROR"))
                {
                    this.ErrorString = functionresult;
                    functionresult = string.Empty;
                }
                else if (functionresult.ToUpper().Trim().StartsWith("CANCEL"))
                {
                    this.ErrorString = functionresult;
                    ResponseModel.Status = OrderStatus.Cancelled;
                    if (functionresult.IndexOf(ErrorCode.DateFormatError.ToString()) > -1)
                    {
                        ResponseModel.ErrorCode = ErrorCode.DateFormatError;
                    }
                    else if (functionresult.IndexOf(ErrorCode.HttpConnectionFailure.ToString()) > -1)
                    {
                        ResponseModel.ErrorCode = ErrorCode.HttpConnectionFailure;
                    }
                    else if (functionresult.IndexOf(ErrorCode.UnhandledSpecialCharacter.ToString()) > -1)
                    {
                        ResponseModel.ErrorCode = ErrorCode.UnhandledSpecialCharacter;
                    }
                    else if (functionresult.IndexOf(ErrorCode.DataUnavailableAtSource.ToString()) > -1)
                    {
                        ResponseModel.ErrorCode = ErrorCode.DataUnavailableAtSource;
                    }
                    else if (functionresult.IndexOf(ErrorCode.FulfillmentTimeLimitExceeded.ToString()) > -1)
                    {
                        ResponseModel.ErrorCode = ErrorCode.FulfillmentTimeLimitExceeded;
                    }
                    else if (functionresult.IndexOf(ErrorCode.DefendentNameLimitExceeded.ToString()) > -1)
                    {
                        ResponseModel.ErrorCode = ErrorCode.DefendentNameLimitExceeded;
                    }
                    else if (functionresult.IndexOf(ErrorCode.OutOfRangeDateOfBirth.ToString()) > -1)
                    {
                        ResponseModel.ErrorCode = ErrorCode.OutOfRangeDateOfBirth;
                    }
                    else
                    {
                        ResponseModel.ErrorCode = ErrorCode.RunTimeError;
                    }
                    functionresult = string.Empty;
                }
                else if (functionresult.ToUpper().Trim().StartsWith("REVIEW~RESULT"))
                {
                    isReviewCase = true;
                }
                else if (functionresult.ToUpper().Trim().StartsWith("CLEAR~RESULT"))
                {
                    clearStringFound = true;
                }
            }
            catch (Exception ex)
            {
                this.ErrorString = "Error in AddOnFunction: " + ex;
            }
            finally
            { additionalParameters = null; }
        }

        private void CheckResponse(ConfigurableCWIStepsModel dataRow, ref bool exitIteration)
        {
            HtmlDocument document = new HtmlDocument();
            HtmlNodeCollection elements = null;
            List<string> parameters = new List<string>();
            try
            {
                isElementPresent = false;
                //// Remove Scripts from Response
                if (!string.IsNullOrEmpty(HttpManager.Response) && dataRow.ConfigCWIFormat != null && dataRow.ConfigCWIFormat.ToString().ToUpper().IndexOf("NO_SCRIPT_REMOVAL") == -1)
                {
                    websiteResponse = CommonExtensions.RemoveScriptFromResponse(HttpManager.Response);
                    if (!string.IsNullOrWhiteSpace(CommonExtensions.ErrorDescription))
                    {
                        ////Check the Error string
                        this.ErrorString = CommonExtensions.ErrorDescription;
                        return;
                    }
                }
                ////Load the response in HTML Document
                document.LoadHtml(websiteResponse);
                switch (dataRow.Step.ToString().ToUpper())
                {
                    case "CHECKLINK":
                        elements = document.DocumentNode.SelectNodes("//a");
                        break;                    
                    case "CHECKSPAN":
                        elements = document.DocumentNode.SelectNodes("//span");
                        break;
                    case "CHECKINPUT":
                        elements = document.DocumentNode.SelectNodes("//input");
                        break;
                    case "CHECKFORCHECKBOX":
                    case "CHECKFORRADIOBUTTON":
                    case "CHECKFORCHECKBOXEXACT":
                    case "CHECKFORRADIOBUTTONEXACT":
                        elements = document.DocumentNode.SelectNodes("//input[@class='" + rowData + "']");
                        if (elements == null)
                        {
                            elements = document.DocumentNode.SelectNodes("//input[@type='" + rowData + "']");
                        }
                        if (elements == null)
                        {
                            elements = document.DocumentNode.SelectNodes("//input[@name='" + rowData + "']");
                        }
                        break;
                    case "CHECKBUTTON":
                        elements = document.DocumentNode.SelectNodes("//button");
                        break;
                    case "CHECKDROPDOWN":
                    case "CHECKDROPDOWNELEMENT":
                    case "CHECKDROPDOWNFORNEWVALUE":
                        elements = document.DocumentNode.SelectNodes("//select[@id='" + rowData + "']/option");
                        if (elements == null)
                        {
                            elements = document.DocumentNode.SelectNodes("//select[@name='" + rowData + "']/option");
                        }
                        break;
                    case "CHECKCUSTOMISEDNODE":
                        elements = document.DocumentNode.SelectNodes("//" + rowData);
                        break;                    
                    default:
                        this.ErrorString = "Incorrect Step provided for Check Response";
                        break;
                }
                if (elements == null || elements.Count == 0)
                {
                    this.ErrorString = "Element not Found";
                    isElementPresent = false;
                }

                ////Get all the values in parameters List
                parameters = updatedParameters.ToUpper().Trim().Split('|').ToList();

                if (globalStringListCounter > -1 && globalStringList.Count <= globalStringListCounter && rowData.ToUpper().IndexOf("PH_GLOBALSTRINGLIST") > -1)
                {
                    rowData = rowData.Replace("PH_GLOBALSTRINGLIST", globalStringList[globalStringListCounter]);
                }

                if (globalCaseDetailLinkCounter > -1 && globalCaseDetailLinkList.Count <= globalCaseDetailLinkCounter && rowData.ToUpper().IndexOf("PH_GLOBALCASEDETAILLINKLIST") > -1)
                {
                    rowData = rowData.Replace("PH_GLOBALCASEDETAILLINKLIST", globalCaseDetailLinkList[globalCaseDetailLinkCounter]);
                }

                ////Check the length of Drop Down Elements
                if (dataRow.Step.ToString().ToUpper() == "CHECKDROPDOWN" && elements.Count != parameters.Count)
                {
                    this.ErrorString = "Length of Drop down elements do no match";
                    return;
                }


                //// For CheckDropDownForNewValue
                if (dataRow.Step.ToString().ToUpper() == "CHECKDROPDOWNFORNEWVALUE")
                {
                    switch (dataRow.StepType.ToString().ToUpper())
                    {
                        case "CHECKBYVALUE":
                            foreach (HtmlNode element in elements)
                            {
                                if (!parameters.Any(x => x.Equals(element.GetAttributeValue("value", string.Empty).ToUpper().Trim(), StringComparison.OrdinalIgnoreCase)))
                                {
                                    isElementPresent = false;
                                    this.ErrorString = "New Value of Drop Down Found on Website";
                                    return;
                                }
                            }
                            break;
                        case "CHECKBYNEXTSIBLINGINNERTEXT":
                            foreach (HtmlNode element in elements)
                            {
                                if (!parameters.Any(x => x.Equals(element.NextSibling.InnerText.ToUpper().Trim(), StringComparison.OrdinalIgnoreCase)))
                                {
                                    isElementPresent = false;
                                    this.ErrorString = "New Value of Drop Down Found on Website";
                                    return;
                                }
                            }
                            break;
                        default:
                            this.ErrorString = "Incorrect StepType provided for Check Response : " + dataRow.StepType.ToString().ToUpper();
                            break;
                    }
                    isElementPresent = true;
                }
                // Check for new values in Checkbox/radio button
                else if (dataRow.Step.ToString().ToUpper() == "CHECKFORCHECKBOX" || dataRow.Step.ToString().ToUpper() == "CHECKFORRADIOBUTTON")
                {
                    switch (dataRow.StepType.ToString().ToUpper())
                    {
                        case "CHECKBYID":
                            //foreach (HtmlNode element in elements)
                            //{
                            //    if (!parameters.Any(x => x.Equals(element.GetAttributeValue("id", string.Empty).ToUpper(), StringComparison.OrdinalIgnoreCase)))
                            //    {
                            //        isElementPresent = false;
                            //        this.ErrorString = "New value found in Website for element: " + rowData;
                            //        return;
                            //    }
                            //}
                            if (elements.AsQueryable().Select(x => x.GetAttributeValue("id", string.Empty).ToUpper()).Intersect(parameters, StringComparer.OrdinalIgnoreCase).Count() != parameters.Count)
                            {
                                elements.AsQueryable().Select(x => x.GetAttributeValue("id", string.Empty).ToUpper()).Intersect(parameters, StringComparer.OrdinalIgnoreCase).ToList().ForEach(x => { parameters.Remove(x); });
                                this.ErrorString = "ID(s) missing in Database but found on Website' - '" + string.Join("|", parameters) + "' ";
                                return;
                            }
                            if (elements.AsQueryable().Select(x => x.GetAttributeValue("id", string.Empty).ToUpper()).Intersect(parameters, StringComparer.OrdinalIgnoreCase).Count() != elements.Count)
                            {
                                var DBConfigDetail = elements.Select(x => x.GetAttributeValue("id", string.Empty).ToUpper()).ToList();
                                DBConfigDetail.Intersect(parameters, StringComparer.OrdinalIgnoreCase).ToList().ForEach(x => { DBConfigDetail.Remove(x); });
                                this.ErrorString = "ID(s) not found on Website' - '" + string.Join("|", DBConfigDetail) + "' ";
                                return;
                            }
                            break;
                        case "CHECKBYNAME":
                            //foreach (HtmlNode element in elements)
                            //{
                            //    if (!parameters.Any(x => x.Equals(element.GetAttributeValue("name", string.Empty).ToUpper(), StringComparison.OrdinalIgnoreCase)))
                            //    {
                            //        isElementPresent = false;
                            //        this.ErrorString = "New value found in Website for element: " + rowData;
                            //        return;
                            //    }
                            //}
                            if (elements.AsQueryable().Select(x => x.GetAttributeValue("name", string.Empty).ToUpper()).Intersect(parameters, StringComparer.OrdinalIgnoreCase).Count() != parameters.Count)
                            {
                                elements.AsQueryable().Select(x => x.GetAttributeValue("name", string.Empty).ToUpper()).Intersect(parameters, StringComparer.OrdinalIgnoreCase).ToList().ForEach(x => { parameters.Remove(x); });
                                this.ErrorString = "Name Attribute(s) missing in Database but found on Website' - '" + string.Join("|", parameters) + "' ";
                                return;
                            }
                            if (elements.AsQueryable().Select(x => x.GetAttributeValue("name", string.Empty).ToUpper()).Intersect(parameters, StringComparer.OrdinalIgnoreCase).Count() != elements.Count)
                            {
                                var DBConfigDetail = elements.Select(x => x.GetAttributeValue("name", string.Empty).ToUpper()).ToList();
                                DBConfigDetail.Intersect(parameters, StringComparer.OrdinalIgnoreCase).ToList().ForEach(x => { DBConfigDetail.Remove(x); });
                                this.ErrorString = "Name attribute(s) not found on Website' - '" + string.Join("|", DBConfigDetail) + "' ";
                                return;
                            }
                            break;
                        case "CHECKBYVALUE":
                            //foreach (HtmlNode element in elements)
                            //{
                            //    if (!parameters.Any(x => x.Equals(element.GetAttributeValue("value", string.Empty).ToUpper(), StringComparison.OrdinalIgnoreCase)))
                            //    {
                            //        isElementPresent = false;
                            //        this.ErrorString = "New value found in Website for element: " + rowData;
                            //        return;
                            //    }
                            //}
                            if (elements.AsQueryable().Select(x => x.GetAttributeValue("value", string.Empty).ToUpper()).Intersect(parameters, StringComparer.OrdinalIgnoreCase).Count() != parameters.Count)
                            {
                                elements.AsQueryable().Select(x => x.GetAttributeValue("value", string.Empty).ToUpper()).Intersect(parameters, StringComparer.OrdinalIgnoreCase).ToList().ForEach(x => { parameters.Remove(x); });
                                this.ErrorString = "Value(s) missing in Database but found on Website' - '" + string.Join("|", parameters) + "' ";
                                return;
                            }
                            if (elements.AsQueryable().Select(x => x.GetAttributeValue("value", string.Empty).ToUpper()).Intersect(parameters, StringComparer.OrdinalIgnoreCase).Count() != elements.Count)
                            {
                                var DBConfigDetail = elements.Select(x => x.GetAttributeValue("value", string.Empty).ToUpper()).ToList();
                                DBConfigDetail.Intersect(parameters, StringComparer.OrdinalIgnoreCase).ToList().ForEach(x => { DBConfigDetail.Remove(x); });
                                this.ErrorString = "Value(s) not found on Website' - '" + string.Join("|", DBConfigDetail) + "' ";
                                return;
                            }
                            break;
                        case "CHECKBYSEARCHOPTION":
                            //foreach (HtmlNode element in elements)
                            //{
                            //    if (!parameters.Any(x => x.Equals(element.GetAttributeValue("searchoption", string.Empty).ToUpper(), StringComparison.OrdinalIgnoreCase)))
                            //    {
                            //        isElementPresent = false;
                            //        this.ErrorString = "New value found in Website for element: " + rowData;
                            //        return;
                            //    }
                            //}
                            if (elements.AsQueryable().Select(x => x.GetAttributeValue("searchoption", string.Empty).ToUpper()).Intersect(parameters, StringComparer.OrdinalIgnoreCase).Count() != parameters.Count)
                            {
                                elements.AsQueryable().Select(x => x.GetAttributeValue("searchoption", string.Empty).ToUpper()).Intersect(parameters, StringComparer.OrdinalIgnoreCase).ToList().ForEach(x => { parameters.Remove(x); });
                                this.ErrorString = "Searchoption Attribute(s) missing in Database but found on Website' - '" + string.Join("|", parameters) + "' ";
                                return;
                            }
                            if (elements.AsQueryable().Select(x => x.GetAttributeValue("searchoption", string.Empty).ToUpper()).Intersect(parameters, StringComparer.OrdinalIgnoreCase).Count() != elements.Count)
                            {
                                var DBConfigDetail = elements.Select(x => x.GetAttributeValue("searchoption", string.Empty).ToUpper()).ToList();
                                DBConfigDetail.Intersect(parameters, StringComparer.OrdinalIgnoreCase).ToList().ForEach(x => { DBConfigDetail.Remove(x); });
                                this.ErrorString = "Searchoption Attribute(s) not found on Website' - '" + string.Join("|", DBConfigDetail) + "' ";
                                return;
                            }
                            break;
                        default:
                            this.ErrorString = "Incorrect StepType provided for Check Response : " + dataRow.StepType.ToString().ToUpper();
                            break;
                    }
                    isElementPresent = true;
                }
                else if (elements != null && elements.Count > 0)
                {
                    //// For all other checks
                    foreach (string parameter in parameters)
                    {
                        isElementPresent = false;

                        switch (dataRow.StepType.ToString().ToUpper())
                        {
                            case "CHECKBYID":
                                if (elements.Any(x => x.Id.ToUpper().Trim() == parameter) ||
                                    elements.Any(x => x.GetAttributeValue("id", string.Empty).ToUpper().Trim() == parameter))
                                {
                                    isElementPresent = true;
                                }
                                break;
                            case "CHECKBYLINKTEXT":
                                if (elements.Any(x => x.InnerText.ToUpper().Trim() == parameter) ||
                                        elements.Any(x => x.GetAttributeValue("Innertext", string.Empty).ToUpper().Trim() == parameter))
                                {
                                    isElementPresent = true;
                                }
                                break;
                            case "CHECKBYNAME":
                                if (elements.Any(x => x.Name.ToUpper().Trim() == parameter) ||
                                    elements.Any(x => x.GetAttributeValue("name", string.Empty).ToUpper().Trim() == parameter))
                                {
                                    isElementPresent = true;
                                }
                                break;
                            case "CHECKBYVALUE":
                                if (elements.Any(x => x.GetAttributeValue("value", string.Empty).ToUpper().Trim() == parameter))
                                {
                                    isElementPresent = true;
                                    break;
                                }
                                break;
                            case "CHECKBYTITLE":
                                if (elements.Any(x => x.GetAttributeValue("title", string.Empty).ToUpper().Trim() == parameter))
                                {
                                    isElementPresent = true;
                                    break;
                                }
                                break;
                            case "CHECKBYONCLICK":
                                if (elements.Any(x => x.GetAttributeValue("onclick", string.Empty).ToUpper().Trim() == parameter))
                                {
                                    isElementPresent = true;
                                    break;
                                }
                                break;
                            case "CHECKBYNEXTSIBLINGINNERTEXT":
                                if (elements.Any(x => x.NextSibling.InnerText.ToUpper().Trim() == parameter))
                                {
                                    isElementPresent = true;
                                    break;
                                }
                                break;
                            case "CHECKBYALT":
                                if (elements.Any(x => x.Id.ToUpper().Trim() == parameter) ||
                                    elements.Any(x => x.GetAttributeValue("alt", string.Empty).ToUpper().Trim() == parameter))
                                {
                                    isElementPresent = true;
                                }
                                break;
                            default:
                                this.ErrorString = "Incorrect StepType provided for Check Response : " + dataRow.StepType.ToString().ToUpper();
                                return;
                        }
                        //// Check if the element is present or not
                        if (!isElementPresent)
                        {
                            this.ErrorString = "Error in CheckResponse : Parameter not found - " + parameter;
                            break;
                        }
                    }
                }

                if (!isElementPresent)
                {
                    if (dataRow.Verify.ToString().Trim().ToUpper() == "NO")
                    {
                        ////Exit iteration if Verify is  "NO" (Handled for pagination in resultPage)
                        isElementPresent = false;
                        exitIteration = true;
                        this.ErrorString = string.Empty;
                    }
                    else if (dataRow.Verify.ToString().Trim().ToUpper() == "CONTINUE")
                    {
                        functionresult = "CONTINUE_AND_EXITLOOP";
                        this.ErrorString = string.Empty;
                    }
                    else if (string.IsNullOrWhiteSpace(this.ErrorString))
                    {
                        this.ErrorString = "Element not found : " + dataRow.ConfigCWIParameter.ToString();
                    }
                }
            }
            catch (Exception ex)
            {
                this.ErrorString = "Error in CheckResponse: " + ex;
            }
        }

        private void ExtractDesiredValueFromResponse(ConfigurableCWIStepsModel dataRow, string rowData)
        {
            HtmlDocument document = new HtmlDocument();
            HtmlNodeCollection elements = null;
            functionresult = string.Empty;
            try
            {
                //// Remove Scripts from Response

                if (!string.IsNullOrEmpty(HttpManager.Response) && dataRow.ConfigCWIFormat != null && dataRow.ConfigCWIFormat.ToString().ToUpper().IndexOf("NO_SCRIPT_REMOVAL") == -1)
                {
                    websiteResponse = CommonExtensions.RemoveScriptFromResponse(websiteResponse);
                    if (!string.IsNullOrWhiteSpace(CommonExtensions.ErrorDescription))
                    {
                        ////Check the Error string
                        this.ErrorString = CommonExtensions.ErrorDescription;
                        return;
                    }
                }
                ////Load the response in HTML Document                
                document.LoadHtml(websiteResponse);
                switch (dataRow.Step.ToString().ToUpper())
                {
                    case "EXTRACTFROMLINK":
                        elements = document.DocumentNode.SelectNodes("//a");
                        break;
                    case "EXTRACTFROMBUTTON":
                        elements = document.DocumentNode.SelectNodes("//button");
                        break;
                    case "EXTRACTFROMINPUT":
                        elements = document.DocumentNode.SelectNodes("//input");
                        break;
                    case "EXTRACTFROMDROPDOWN":
                        elements = document.DocumentNode.SelectNodes("//select[@id='" + rowData + "']/option");
                        if (elements == null)
                        {
                            elements = document.DocumentNode.SelectNodes("//select[@name='" + rowData + "']/option");
                        }
                        break;
                    case "EXTRACTFROMFORM":
                        elements = document.DocumentNode.SelectNodes("//form");
                        break;
                    case "EXTRACTFROMIMAGE":
                        elements = document.DocumentNode.SelectNodes("//img");
                        break;
                    case "EXTRACTFROMSPAN":
                        elements = document.DocumentNode.SelectNodes("//span");
                        break;
                    case "EXTRACTFROMSELECT":
                        elements = document.DocumentNode.SelectNodes("//select");
                        break;
                    case "EXTRACTFROMDIV":
                        elements = document.DocumentNode.SelectNodes("//div");
                        break;
                    case "EXTRACTFROMOPTION":
                        elements = document.DocumentNode.SelectNodes("//option");
                        break;
                    default:
                        this.ErrorString = "Incorrect Step provided for ExtractDesiredValueFromResponse";
                        break;
                }
                if (elements == null || elements.Count == 0)
                {
                    this.ErrorString = "Error in ExtractDesiredValueFromResponse Element List not Found";
                    isElementPresent = false;
                    return;
                }

                //// Format Row Data
                rowData = rowData.Trim().ToUpper();
                //// Check if the desired element is present or not
                HtmlNode requiredElement = null;
                switch (dataRow.StepType.ToString().ToUpper())
                {
                    case "EXTRACTBYID":
                        if (elements.Any(x => x.Id.ToUpper().Trim() == rowData))
                        {
                            requiredElement = elements.Where(x => x.Id.ToUpper().Trim() == rowData).First();
                        }
                        else if (elements.Any(x => x.GetAttributeValue("id", string.Empty).ToUpper().Trim() == rowData))
                        {
                            requiredElement = elements.Where(x => x.GetAttributeValue("id", string.Empty).ToUpper().Trim() == rowData).First();
                        }
                        break;
                    case "EXTRACTBYNAME":
                        if (elements.Any(x => x.Name.ToUpper().Trim() == rowData))
                        {
                            requiredElement = elements.Where(x => x.Name.ToUpper().Trim() == rowData).First();
                        }
                        else if (elements.Any(x => x.GetAttributeValue("Name", string.Empty).ToUpper().Trim() == rowData))
                        {
                            requiredElement = elements.Where(x => x.GetAttributeValue("Name", string.Empty).ToUpper().Trim() == rowData).First();
                        }
                        break;
                    case "EXTRACTBYNEXTSIBLINGINNERTEXT":
                        if (elements.Any(x => x.NextSibling.InnerText.ToUpper().Trim() == rowData))
                        {
                            requiredElement = elements.Where(x => x.NextSibling.InnerText.ToUpper().Trim() == rowData).First();
                        }
                        break;
                    case "EXTRACTBYLINKTEXT":
                        if (elements.Any(x => x.InnerText.ToUpper().Trim() == rowData))
                        {
                            requiredElement = elements.Where(x => x.InnerText.ToUpper().Trim() == rowData).First();
                        }
                        else if (elements.Any(x => x.GetAttributeValue("Innertext", string.Empty).ToUpper().Trim() == rowData))
                        {
                            requiredElement = elements.Where(x => x.GetAttributeValue("InnerText", string.Empty).ToUpper().Trim() == rowData).First();
                        }
                        break;
                    case "EXTRACTBYACTION":
                        if (elements.Any(x => x.GetAttributeValue("Action", string.Empty).ToUpper().Trim() == rowData))
                        {
                            requiredElement = elements.Where(x => x.GetAttributeValue("Action", string.Empty).ToUpper().Trim() == rowData).First();
                        }
                        break;
                    case "EXTRACTBYTITLE":
                        if (elements.Any(x => x.GetAttributeValue("Title", string.Empty).ToUpper().Trim() == rowData))
                        {
                            requiredElement = elements.Where(x => x.GetAttributeValue("Title", string.Empty).ToUpper().Trim() == rowData).First();
                        }
                        break;
                    case "EXTRACTBYCLASS":
                        if (elements.Any(x => x.GetAttributeValue("Class", string.Empty).ToUpper().Trim() == rowData))
                        {
                            requiredElement = elements.Where(x => x.GetAttributeValue("Class", string.Empty).ToUpper().Trim() == rowData).First();
                        }
                        break;
                    case "EXTRACTBYMETHOD":
                        if (elements.Any(x => x.GetAttributeValue("Method", string.Empty).ToUpper().Trim() == rowData))
                        {
                            requiredElement = elements.Where(x => x.GetAttributeValue("Method", string.Empty).ToUpper().Trim() == rowData).First();
                        }
                        break;
                    case "EXTRACTBYVALUE":
                        if (elements.Any(x => x.GetAttributeValue("Value", string.Empty).ToUpper().Trim() == rowData))
                        {
                            requiredElement = elements.Where(x => x.GetAttributeValue("Value", string.Empty).ToUpper().Trim() == rowData).First();
                        }
                        break;
                    default:
                        this.ErrorString = "Incorrect StepType provided for ExtractDesiredValueFromResponse";
                        break;
                }

                //// Check if we got the desired element or not
                if (requiredElement == null)
                {
                    this.ErrorString = "Error in ExtractDesiredValueFromResponse Required Element not Found";
                    isElementPresent = false;
                    return;
                }

                //// Get the desired value
                switch (dataRow.ConfigCWIParameter.ToString().ToUpper())
                {
                    case "EXTRACTID":
                        functionresult = requiredElement.Id;
                        if (string.IsNullOrWhiteSpace(functionresult))
                        {
                            functionresult = requiredElement.GetAttributeValue("Id", string.Empty);
                        }
                        break;
                    case "EXTRACTNAME":
                        functionresult = requiredElement.Name;
                        if (string.IsNullOrWhiteSpace(functionresult))
                        {
                            functionresult = requiredElement.GetAttributeValue("Name", string.Empty);
                        }
                        break;
                    case "EXTRACTVALUE":
                        functionresult = requiredElement.GetAttributeValue("Value", string.Empty);
                        break;
                    case "EXTRACTLINKTEXT":
                        functionresult = requiredElement.InnerText;
                        if (string.IsNullOrWhiteSpace(functionresult))
                        {
                            functionresult = requiredElement.GetAttributeValue("InnerText", string.Empty);
                        }
                        break;
                    case "EXTRACTACTION":
                        functionresult = requiredElement.GetAttributeValue("Action", string.Empty);
                        break;
                    case "EXTRACTHREF":
                        functionresult = requiredElement.GetAttributeValue("Href", string.Empty);
                        break;
                    case "EXTRACTONCLICK":
                        functionresult = requiredElement.GetAttributeValue("OnClick", string.Empty);
                        break;
                    case "EXTRACTSOURCE":
                        functionresult = requiredElement.GetAttributeValue("src", string.Empty);
                        break;
                    case "EXTRACTDISABLED":
                        functionresult = requiredElement.GetAttributeValue("disabled", string.Empty);
                        break;
                    case "EXTRACTNAMEATTRIBUTE":
                        functionresult = requiredElement.GetAttributeValue("Name", string.Empty);
                        break;
                    case "EXTRACTONCHANGE":
                        functionresult = requiredElement.GetAttributeValue("OnChange", string.Empty);
                        break;
                    case "EXTRACTSTYLE":
                        functionresult = requiredElement.GetAttributeValue("Style", string.Empty);
                        break;
                    case "EXTRACTSITEKEY":
                        functionresult = requiredElement.GetAttributeValue("data-sitekey", string.Empty);
                        break;
                    default:
                        this.ErrorString = "Incorrect Parameters provided for ExtractDesiredValueFromResponse";
                        break;
                }
            }
            catch (Exception ex)
            {
                this.ErrorString = "Error in ExtractDesiredValueFromResponse : " + ex;
            }
        }

        private void CreatePostString(ConfigurableCWIStepsModel dataRow, string rowData)
        {
            string postingKey = string.Empty;
            try
            {
                postingKey = updatedParameters;
                while (hashValues.ContainsKey(postingKey))
                {
                    postingKey = postingKey + "^";
                }
                switch (dataRow.StepType.ToString().ToUpper())
                {
                    case "GETVALUEBYCONTROLNAME":
                        string websiteResponse = string.Empty;
                        if (!string.IsNullOrEmpty(HttpManager.Response) && dataRow.ConfigCWIFormat != null && dataRow.ConfigCWIFormat.ToString().ToUpper().IndexOf("NO_SCRIPT_REMOVAL") == -1)
                        {
                            websiteResponse = CommonExtensions.RemoveScriptFromResponse(HttpManager.Response);
                            if (!string.IsNullOrWhiteSpace(CommonExtensions.ErrorDescription))
                            {
                                ////Check the Error string
                                this.ErrorString = CommonExtensions.ErrorDescription;
                                return;
                            }
                        }

                        if (DoNotUseHashTableForPosting)
                            this.postString = this.postString + "&" + updatedParameters + "=" + websiteResponse.GetValueByControlName(updatedParameters);
                        else
                            hashValues.Add(postingKey, websiteResponse.GetValueByControlName(dataRow.ConfigCWIParameter.ToString()));
                        break;
                    case "ADDPOSTSTRINGVALUE":
                        if (DoNotUseHashTableForPosting)
                            this.postString = this.postString + "&" + updatedParameters + "=" + rowData;
                        else
                            hashValues.Add(postingKey, rowData);
                        break;
                    case "CREATEPOSTING":
                        if (DoNotUseHashTableForPosting)
                        {
                            while (this.postString.StartsWith("&"))
                                this.postString = this.postString.Substring(1);
                            this.postString = HttpManager.UrlEncode(this.postString).Replace("^", "").Replace("%5E", "").Replace("%5e", "").Replace("%3d", "=").Replace("%3D", "=").Replace("%26", "&");

                        }
                        else
                        {
                            this.postString = string.Empty;
                            this.postString = CommonExtensions.GeneratePostString(new Hashtable(hashValues)).Replace("^", "").Replace("%5E", "").Replace("%5e", "");
                            this.ErrorString = CommonExtensions.ErrorDescription + string.Empty;
                        }
                        break;
                    //// Case to assign the postring AS IS
                    case "ASSIGNPOSTSTRINGVALUE":
                        this.postString = rowData;
                        break;
                    default:
                        this.ErrorString = "Incorrect StepType provided for Create Post String : " + dataRow.StepType.ToString().ToUpper();
                        break;
                }
            }
            catch (Exception ex)
            {
                this.ErrorString = "Error in ProcessRow: " + ex;
            }
        }

        private void PerformHTTPPosting(ConfigurableCWIStepsModel dataRow, string rowData)
        {
            try
            {
                if (dataRow.StepType.ToString().Trim().ToUpper() != "GETWITHPREVIOUSURL" &&
                    dataRow.StepType.ToString().Trim().ToUpper() != "POSTWITHPREVIOUSURL")
                {
                    if (!string.IsNullOrWhiteSpace(updatedParameters)) HttpManager.URL = updatedParameters;
                    if (!string.IsNullOrWhiteSpace(rowData))
                    {
                        HttpManager.URL = HttpManager.URL.Trim() + rowData;
                    }
                }

                switch (dataRow.StepType.ToString().Trim().ToUpper())
                {
                    case "GET":
                    case "GETWITHPREVIOUSURL":
                        HttpManager.ExecuteGet();
                        logger.Info("ParentVendor : " + parentVendor + "ScreeningID: " + screeningID + " : ConfigMasterAdaptor Get URL posted : " + HttpManager.URL);
                        break;
                    case "POST":
                    case "POSTWITHPREVIOUSURL":
                        HttpManager.PostData = this.postString;
                        logger.Info("ParentVendor : " + parentVendor + "ScreeningID: " + screeningID + " : ConfigMasterAdaptor Post URL : " + HttpManager.URL);
                        logger.Info("ParentVendor : " + parentVendor + "ScreeningID: " + screeningID + " : ConfigMasterAdaptor post string : " + postString);
                        HttpManager.ExecutePost();
                        break;
                    case "GETWITHQUERYSTRING":
                        HttpManager.URL = HttpManager.URL + this.postString;
                        HttpManager.ExecuteGet();
                        logger.Info("ParentVendor : " + parentVendor + "ScreeningID: " + screeningID + " : ConfigMasterAdaptor Query URL posted : " + HttpManager.URL + this.postString);
                        break;
                    case "GETCAPTCHAIMAGE":
                        GetCaptchaImage(dataRow, rowData);
                        break;
                    case "PUT":
                        HttpManager.PostData = this.postString;
                        logger.Info("ParentVendor : " + parentVendor + "ScreeningID: " + screeningID + " : ConfigMasterAdaptor PUT URL : " + HttpManager.URL);
                        logger.Info("ParentVendor : " + parentVendor + "ScreeningID: " + screeningID + " : ConfigMasterAdaptor PUT string : " + postString);
                        HttpManager.ExecutePut();
                        break;
                    case "GETMANUALCAPTCHAIMAGE":
                        GetManualCaptchaImage(dataRow);
                        break;
                    case "GETTWOCAPTCHAIMAGE":
                        GetTwoCaptchaImage(dataRow);
                        break;
                    case "GETANTICAPTCHAIMAGE":
                        GetAntiCaptchaImage(dataRow, rowData);
                        break;
                    default:
                        this.ErrorString = "Incorrect StepType provided for Perform HTTP Posting : " + dataRow.StepType.ToString().ToUpper();
                        break;
                }

                //// Save the Html Page
                if (!string.IsNullOrWhiteSpace(HttpManager.Response))
                {
                    websiteResponse = HttpManager.Response;
                    SaveLogs();
                }
                //// Check if we need to ignore the error or not
                if (!string.IsNullOrWhiteSpace(dataRow.Verify.ToString()) && dataRow.Verify.ToString() == "NO")
                {
                    this.ErrorString = string.Empty;
                }
            }
            catch (Exception ex)
            {
                this.ErrorString = "Error in PerformHTTPPosting: " + ex;
            }
        }

        private void GetManualCaptchaImage(ConfigurableCWIStepsModel dataRow)
        {
            int captchaCounter = 0;
            try
            {
                string captchaPollingTime = ConfigDetail.Where(x => x.FieldName == "CaptchaPollingTime").First().FieldValue;
                string captchaExpiryTime = ConfigDetail.Where(x => x.FieldName == "CaptchaExpiryTime").First().FieldValue;
                int captchaMaxRetryCount;
                if (!int.TryParse(ConfigDetail.Where(x => x.FieldName == "CaptchaMaxRetryCount").First().FieldValue, out captchaMaxRetryCount))
                {
                    captchaMaxRetryCount = 3;
                }

                if (string.IsNullOrWhiteSpace(captchaPollingTime) || string.IsNullOrWhiteSpace(captchaExpiryTime))
                {
                    this.ErrorString = "Error in GetCaptchaImage: Mandatory Captcha fields not found in ConfigDetails table";
                    return;
                }

                HttpManager.URL = dataRow.ConfigCWIParameter.ToString() + rowData;
                HttpWebResponse webResponse = HttpManager.GetCaptchaImage();

                logger.Info("captchaImageUrl = " + HttpManager.URL);
                //Call CheckFailSafe
                if (!HttpManager.Response.CheckFailSafe())
                {
                    ResponseModel.ErrorCode = CommonExtensions.ErrorCode;
                    ResponseModel.ErrorDescription = "[PRISM.ConfigQC-Master]-[PerformSearch]-Captcha Image Posting CheckFailSafe failed :" + CommonExtensions.ErrorDescription;
                    logger.Error("Screening:" + this.screeningID + "-Result is-" + ResponseModel.ErrorCode.ToString() + "~" + ResponseModel.ErrorDescription.ToString());
                    ResponseModel.Status = OrderStatus.Retry;
                    return;
                }
                //Retrieve captcha image
                string contentType = string.Empty;
                contentType = webResponse.ContentType;
                string imageExtension = GetImageContentType(contentType);
                byte[] byteObject = new byte[9999];
                Stream stream = default(Stream);
                int count = 0;
                MemoryStream memoryStream = new MemoryStream();
                stream = webResponse.GetResponseStream();
                do
                {
                    count = stream.Read(byteObject, 0, byteObject.Length);
                    if (count != 0)
                        memoryStream.Write(byteObject, 0, count);
                    else
                        break;

                }
                while (true);
                byte[] image = memoryStream.ToArray();

                // Insert Captcha Image in DB
                AzureSqlHelper azureSqlHelper = new AzureSqlHelper();
                CaptchaOrderDetailsModel captchaOrderDetailsModel = new CaptchaOrderDetailsModel();
                captchaOrderDetailsModel.ParentVendor = ResponseModel.Screening[0].ScreeningRequest.Vendor;
                captchaOrderDetailsModel.CaptchaImage = image;
                captchaOrderDetailsModel.Status = OrderStatus.New;
                captchaOrderDetailsModel.ScreeningId = ResponseModel.Screening[0].ScreeningId;
                captchaOrderDetailsModel.LastUpdatedOn = DateTime.UtcNow;
                captchaOrderDetailsModel.ContentType = contentType;
                captchaOrderDetailsModel.ImageExtension = imageExtension;
                captchaOrderDetailsModel.CaptchaWaitTime = 60;

                functionresult = string.Empty;

                do
                {
                    functionresult = azureSqlHelper.GetCaptchaTextFromDB(captchaOrderDetailsModel);
                    captchaCounter = captchaCounter + 1;
                    if (!string.IsNullOrEmpty(functionresult))
                        break;
                } while (captchaCounter < Convert.ToInt32(captchaMaxRetryCount));

                if (string.IsNullOrEmpty(functionresult))
                {
                    ResponseModel.ErrorCode = ErrorCode.CaptchaExpired;
                    ResponseModel.ErrorDescription = "[PRISM.Config-QC][PerformSearch]- Captcha Expired";
                    logger.Error("Screening:" + this.screeningID + "-Result is-" + ResponseModel.ErrorCode.ToString() + "~" + ResponseModel.ErrorDescription.ToString());
                    ResponseModel.Status = OrderStatus.Retry;
                    return;
                }

            }
            catch (Exception ex)
            {
                this.ErrorString = "Error in GetManualCaptchaImage : " + ex;
            }
        }

        private void GetCaptchaImage(ConfigurableCWIStepsModel dataRow, string rowData)
        {
            try
            {
                string deathByCaptchaUserID = ConfigDetail.Where(x => x.FieldName == "DeathByCaptchaUserID").First().FieldValue;
                string deathByCaptchaPassword = ConfigDetail.Where(x => x.FieldName == "DeathByCaptchaPassword").First().FieldValue;
                string captchaPollingTime = ConfigDetail.Where(x => x.FieldName == "CaptchaPollingTime").First().FieldValue;
                string captchaExpiryTime = ConfigDetail.Where(x => x.FieldName == "CaptchaExpiryTime").First().FieldValue;
                int captchaMaxRetryCount;
                if (!int.TryParse(ConfigDetail.Where(x => x.FieldName == "CaptchaMaxRetryCount").First().FieldValue, out captchaMaxRetryCount))
                {
                    captchaMaxRetryCount = 3;
                }

                if (string.IsNullOrWhiteSpace(deathByCaptchaUserID) || string.IsNullOrWhiteSpace(deathByCaptchaPassword) || string.IsNullOrWhiteSpace(captchaPollingTime) || string.IsNullOrWhiteSpace(captchaExpiryTime))
                {
                    this.ErrorString = "Error in GetCaptchaImage: Mandatory Captcha fields not found in ConfigDetails table";
                    return;
                }

                HttpManager.URL = dataRow.ConfigCWIParameter.ToString() + rowData;
                HttpWebResponse webResponse = HttpManager.GetCaptchaImage();

                logger.Info("captchaImageUrl = " + HttpManager.URL);
                //Call CheckFailSafe
                if (!HttpManager.Response.CheckFailSafe())
                {
                    ResponseModel.ErrorCode = CommonExtensions.ErrorCode;
                    ResponseModel.ErrorDescription = "[PRISM.ConfigQC-Master]-[PerformSearch]-Captcha Image Posting CheckFailSafe failed :" + CommonExtensions.ErrorDescription;
                    logger.Error("Screening:" + this.screeningID + "-Result is-" + ResponseModel.ErrorCode.ToString() + "~" + ResponseModel.ErrorDescription.ToString());
                    ResponseModel.Status = OrderStatus.Retry;
                    return;
                }

                //Retrieve captcha image
                string contentType = string.Empty;
                contentType = webResponse.ContentType;
                string imageExtension = GetImageContentType(contentType);
                byte[] byteObject = new byte[9999];
                Stream stream = default(Stream);
                int count = 0;
                MemoryStream memorySteram = new MemoryStream();
                stream = webResponse.GetResponseStream();
                do
                {
                    count = stream.Read(byteObject, 0, byteObject.Length);
                    if (count != 0)
                    {
                        memorySteram.Write(byteObject, 0, count);
                    }
                    else
                    {
                        break;
                    }
                }
                while (true);
                byte[] image = memorySteram.ToArray();


                // Call DeatchByCaptcha
                int counter = 0;
                functionresult = string.Empty;
                while (string.IsNullOrWhiteSpace(functionresult) && counter < captchaMaxRetryCount)
                {
                    string errorMessage = string.Empty;
                    CaptchaAutomation captchaAutomation = new CaptchaAutomation();
                    functionresult = captchaAutomation.GetCaptcha(deathByCaptchaUserID, deathByCaptchaPassword, captchaPollingTime, captchaExpiryTime, image, ResponseModel.Screening[0].ScreeningRequest.Vendor, ref errorMessage);
                    if (!string.IsNullOrWhiteSpace(errorMessage))
                    {
                        this.ErrorString = errorMessage;
                        return;
                    }
                    counter++;
                }
            }
            catch (Exception ex)
            {
                this.ErrorString = "Error in GetCaptchaImage: " + ex;
            }
        }
        private void GetAntiCaptchaImage(ConfigurableCWIStepsModel dataRow, string rowData)
        {
            try
            {
                int captchaMaxRetryCount;
                if (!int.TryParse(ConfigDetail.Where(x => x.FieldName == "CaptchaMaxRetryCount").First().FieldValue, out captchaMaxRetryCount))
                {
                    captchaMaxRetryCount = 3;
                }

                HttpManager.URL = dataRow.ConfigCWIParameter.ToString() + rowData;
                HttpWebResponse webResponse = HttpManager.GetCaptchaImage();

                logger.Info("captchaImageUrl = " + HttpManager.URL);
                //Call CheckFailSafe
                if (!HttpManager.Response.CheckFailSafe())
                {
                    ResponseModel.ErrorCode = CommonExtensions.ErrorCode;
                    ResponseModel.ErrorDescription = "[PRISM.ConfigQC-Master]-[PerformSearch]-Captcha Image Posting CheckFailSafe failed :" + CommonExtensions.ErrorDescription;
                    logger.Error("Screening:" + this.screeningID + "-Result is-" + ResponseModel.ErrorCode.ToString() + "~" + ResponseModel.ErrorDescription.ToString());
                    ResponseModel.Status = OrderStatus.Retry;
                    return;
                }

                //Retrieve captcha image
                string contentType = string.Empty;
                contentType = webResponse.ContentType;
                string imageExtension = GetImageContentType(contentType);
                byte[] byteObject = new byte[9999];
                Stream stream = default(Stream);
                int count = 0;
                MemoryStream memorySteram = new MemoryStream();
                stream = webResponse.GetResponseStream();
                do
                {
                    count = stream.Read(byteObject, 0, byteObject.Length);
                    if (count != 0)
                    {
                        memorySteram.Write(byteObject, 0, count);
                    }
                    else
                    {
                        break;
                    }
                }
                while (true);
                byte[] image = memorySteram.ToArray();

                // Call AntiCaptcha
                int counter = 0;
                functionresult = string.Empty;
                while (string.IsNullOrWhiteSpace(functionresult) && counter < captchaMaxRetryCount)
                {
                    IsCaptchaFail = false;

                    string errorMessage = string.Empty;
                    functionresult = GetAntiCaptchaImage(image);
                    if (!string.IsNullOrWhiteSpace(errorMessage))
                    {
                        this.ErrorString = errorMessage;
                        return;
                    }
                    counter++;
                }
            }
            catch (Exception ex)
            {
                this.ErrorString = "Error in AntiCaptcha: " + ex;
            }
            finally
            {
                IsCaptchaFail = false;
            }
        }
        private string GetAntiCaptchaImage(byte[] image)
        {
            try
            {
                //Read Client Key form DB
                string antiCaptchaClientKey = ConfigDetail.Where(x => x.FieldName == "ClientKey").First().FieldValue;

                var api = new AntiCaptcha.Api.ImageToText();
                api.ClientKey = antiCaptchaClientKey;
                api.BodyBase64 = Convert.ToBase64String(image);

                //var balance = api.GetBalance();
                if (!api.CreateTask())
                {
                    IsCaptchaFail = true;
                    this.ErrorString = "Captcha service returned error " + api.ErrorMessage;
                    return string.Empty;
                }
                else if (!api.WaitForResult())
                {
                    IsCaptchaFail = true;
                    this.ErrorString = "Captcha service returned error " + api.ErrorMessage;
                    return string.Empty;
                }
                else
                {
                    IsCaptchaFail = false;
                    return api.GetTaskSolution().Text;
                }
            }
            catch (Exception ex)
            {
                this.ErrorString = "Failed in GetAntiCaptchaImage :" + ex.Message.ToString();
                return string.Empty;
            }

        }
        private void GetTwoCaptchaImage(ConfigurableCWIStepsModel dataRow)
        {
            int threadCount = 10;
            try
            {
                if (string.IsNullOrWhiteSpace(functionresult))
                {
                    ErrorString = "Function Result Does not contain Site Key";
                    return;
                }

                TwoCaptchaClient CaptchaClient = new TwoCaptchaClient()
                {
                    Captcha_Service_Key = ConfigDetail.Where(x => x.FieldName == "Captcha_Service_Key").First().FieldValue.Trim(),
                    Site_key = functionresult,
                    Captcha_Url = HttpManager.URL,
                    TwoCaptcha_Url = ConfigDetail.Where(x => x.FieldName == "TwoCaptcha_Url").First().FieldValue.Trim(),
                    TwoCaptcha_Token_Url = ConfigDetail.Where(x => x.FieldName == "TwoCaptcha_Token_Url").First().FieldValue.Trim()
                };
                string gcaptchaToken = string.Empty;
                functionresult = string.Empty;

                string captchaResponse = CaptchaClient.GenerateCaptchaRequestID();
                if (captchaResponse.Contains("OK|"))
                {
                    string captchId = captchaResponse.Substring(3, captchaResponse.Length - 3);
                    for (int index = 1; index <= 20; index++)
                    {
                        gcaptchaToken = CaptchaClient.GetCaptchaResponseKey(captchId);
                        if (gcaptchaToken.Contains("OK|"))
                            break;

                        Thread.Sleep(1000 * threadCount);
                    }
                    if (gcaptchaToken.Contains("OK|"))
                        functionresult = gcaptchaToken.Substring(3, gcaptchaToken.Length - 3);
                    else
                    {
                        ErrorString = "Error was returned by Two Captcha Service. Error:" + gcaptchaToken;
                    }
                }
                else
                {
                    ErrorString = "Captcha Request not found from 2Captcha";
                }
            }
            catch (Exception ex)
            {
                this.ErrorString = "Error in GetTwoCaptchaImage: " + ex;
            }

        }

        private void CheckCellContents(ConfigurableCWIStepsModel dataRow, DataTable resultDataTable)
        {
            string columnData;
            bool isValuePresent = false;
            try
            {
                //// Check the Result Table
                if (resultDataTable == null)
                {
                    if (dataRow.Verify.ToString().ToUpper() != "NO")
                    {
                        this.ErrorString = "Result Table is NULL.";
                    }
                    return;
                }
                if (resultDataTable.Rows.Count > 0)
                {
                    ////Get all the pipe "|" separated values in a list
                    List<string> valueList = new List<string>();
                    valueList = dataRow.Data.ToString().ToUpper().Split('|').ToList<string>();
                    foreach (DataRow row in resultDataTable.Rows)
                    {
                        ////Reset the Boolean Variable
                        isValuePresent = false;
                        ////Get the value of Column in a string
                        columnData = string.Empty;
                        columnData = row[dataRow.ConfigCWIParameter.ToString()].ToString().ToUpper();
                        if (!string.IsNullOrWhiteSpace(columnData))
                        {
                            switch (dataRow.ConfigCWIFormat.ToString().ToUpper())
                            {
                                case "CONTAINS":
                                    foreach (string value in valueList)
                                    {
                                        if (columnData.Contains(value))
                                        {
                                            isValuePresent = true;
                                            break;
                                        }
                                    }
                                    break;
                                case "EXACT":
                                    if (valueList.Contains(columnData))
                                    {
                                        isValuePresent = true;
                                    }
                                    break;
                                case "CONTAINSATSPECIFICPOSITION":
                                    foreach (string value in valueList)
                                    {
                                        isValuePresent = ContainsAtSpecificPosition(columnData, value + "^" + dataRow.Clickable.ToString(), "");
                                        if (isValuePresent)
                                        {
                                            break;
                                        }
                                    }
                                    break;
                                default:
                                    this.ErrorString = "Incorrect Format provided for CheckCellContents";
                                    return;
                            }
                            if (!isValuePresent)
                            {
                                break;
                            }
                        }
                        else
                        {
                            isValuePresent = true;
                        }
                    }
                    ////Check if value was present or not
                    if (!isValuePresent)
                    {
                        this.ErrorString = "Column Data does not match with the given list of values";
                        return;
                    }
                }
            }
            catch (Exception ex)
            {
                this.ErrorString = "Error in CheckCellContents: " + ex;
            }
        }

        private bool CheckCellContents(string websiteValue, Check check)
        {
            string position = string.Empty;
            string format = string.Empty;
            try
            {
                List<string> valueList = new List<string>();
                valueList = check.Value.ToUpper().Split('|').ToList<string>();
                if (check.Format.IndexOf("^") > -1)
                {
                    format = check.Format.Split('^')[0].ToUpper();
                    position = check.Format.Split('^')[1].ToUpper();
                }
                else
                {
                    format = check.Format.ToUpper();
                }
                ////Reset the Boolean Variable
                bool isValuePresent = false;
                ////Get the value of Column in a string
                switch (format)
                {
                    case "CONTAINS":
                        foreach (string value in valueList)
                        {
                            if (websiteValue.Contains(value))
                            {
                                isValuePresent = true;
                                break;
                            }
                        }
                        break;
                    case "EXACT":
                        if (valueList.Contains(websiteValue))
                        {
                            isValuePresent = true;
                        }
                        break;
                    case "CONTAINSATSPECIFICPOSITION":
                        foreach (string value in valueList)
                        {
                            isValuePresent = ContainsAtSpecificPosition(websiteValue, value + "^" + position, "");
                            if (isValuePresent)
                            {
                                break;
                            }
                        }
                        break;
                    default:
                        this.ErrorString = "Incorrect Format provided for CheckCellContents";
                        return false;
                }

                ////Check if value was present or not
                if (!isValuePresent)
                {
                    this.ErrorString = "Column Data does not match with the given list of values";
                    return false;
                }
                return true;
            }
            catch (Exception ex)
            {
                this.ErrorString = "Error in CheckCellContents: " + ex;
                return false;
            }
        }

        private void Navigate(RemoteWebDriver driver, ConfigurableCWIStepsModel dataRow)
        {
            try
            {
                switch (dataRow.StepType.ToString().ToUpper().Trim())
                {
                    case "BACK":
                        driver.Navigate().Back();
                        break;
                    case "FORWARD":
                        driver.Navigate().Forward();
                        break;
                    case "REFRESH":
                        driver.Navigate().Refresh();
                        break;
                    case "GOTOURL":
                        driver.Navigate().GoToUrl(dataRow.ConfigCWIParameter.ToString());
                        break;
                    default:
                        this.ErrorString = "Incorrect StepType provided for Navigate : " + dataRow.StepType.ToString().ToUpper();
                        break;
                }
            }
            catch (Exception ex)
            {
                this.ErrorString = "Error in Navigate: " + ex;
            }
        }

        private void CheckNumberOfRows(ConfigurableCWIStepsModel dataRow, DataTable resultDataTable)
        {
            int numberOfRows = -1;
            try
            {
                if (resultDataTable == null)
                {
                    if (dataRow.Verify.ToString().ToUpper() != "NO")
                    {
                        this.ErrorString = "Result Table is NULL.";
                    }
                    return;
                }
                if (!int.TryParse(dataRow.Data.ToString(), out numberOfRows) || numberOfRows == -1)
                {
                    this.ErrorString = "Incorrect Number of Rows Provided.";
                    return;
                }


                switch (dataRow.ConfigCWIParameter.ToString().ToUpper().Trim())
                {
                    case "GREATERTHANREVIEW":
                        if (resultDataTable.Rows.Count > numberOfRows)
                        {
                            this.ErrorString = "REVIEW";
                            isReviewCase = true;
                        }
                        break;
                    case "LESSTHANREVIEW":
                        if (resultDataTable.Rows.Count < numberOfRows)
                        {
                            this.ErrorString = "REVIEW";
                            isReviewCase = true;
                        }
                        break;
                    case "EQUALTOREVIEW":
                        if (resultDataTable.Rows.Count == numberOfRows)
                        {
                            this.ErrorString = "REVIEW";
                            isReviewCase = true;
                        }
                        break;
                    //case "GREATERTHANCLEAR":
                    //    if (resultDataTable.Rows.Count > numberOfRows)
                    //    {
                    //        this.ErrorString = "CLEAR";
                    //        isClearCase = true;
                    //    }
                    //    break;
                    //case "LESSTHANCLEAR":
                    //    if (resultDataTable.Rows.Count < numberOfRows)
                    //    {
                    //        this.ErrorString = "CLEAR";
                    //        isClearCase = true;
                    //    }
                    //    break;
                    //case "EQUALTOCLEAR":
                    //    if (resultDataTable.Rows.Count == numberOfRows)
                    //    {
                    //        this.ErrorString = "CLEAR";
                    //        isClearCase = true;
                    //    }
                    //    break;
                    default:
                        this.ErrorString = "Incorrect Parameter provided for CheckNumberOfRows : " + dataRow.ConfigCWIParameter.ToString().ToUpper();
                        break;
                }

                if (this.ErrorString.Trim().ToUpper() == "CLEAR")
                {
                    ////Resetting the error string as paging will happen
                    this.ErrorString = string.Empty;
                    ////Setting the isClearCase and performPaging variable as true
                    //// isClearCase = true;
                    performPaging = true;
                }
                else if (this.ErrorString.Trim().ToUpper() == "REVIEW")
                {
                    if (this.continuePosting == true)
                    {
                        ////Resetting the error string as further postings are pending
                        this.ErrorString = string.Empty;
                    }
                    else
                    {
                        isReviewCase = true;
                        this.ErrorString = string.Empty;
                    }
                }
            }
            catch (Exception ex)
            {
                this.ErrorString = "Error in CheckNumberOfRows: " + ex;
            }
        }

        private void SaveLogs()
        {
            try
            {
                ////Save the response
                HTMLlogs.HtmlLogFile.Add("Configurable_" + ResponseModel.Screening[0].ScreeningRequest.Vendor + "_" + DateTime.UtcNow.ToString("yyyy-MM-dd HH:mm:ss.fff"), websiteResponse);
                ////Check the Response
                HttpManager.Response = websiteResponse;
                if (!HttpManager.Response.CheckFailSafe())
                {
                    this.ErrorString = "Error in CheckFailSafe";
                }
            }
            catch (Exception ex)
            {
                this.ErrorString = "Error in SaveLogs: " + ex;
            }
        }

        private void CheckReqeustNameLength(ConfigurableCWIStepsModel dataRow, string firstName, string lastName)
        {
            try
            {
                string parameters = updatedParameters.Trim();

                ////Check if "Data" column contains numeric value or not
                if (!IsNumeric(dataRow.Data.ToString().Trim()))
                {
                    this.ErrorString = "Value in Data column is not numeric for CheckReqeustNameLength";
                    return;
                }

                ////Convert the value in Data column to numeric variable
                int length = Convert.ToInt32(dataRow.Data.ToString().Trim());
                ////Depending on Step type, extract the element
                switch (dataRow.StepType.ToString().ToUpper().Trim())
                {
                    case "MINIMUMNAMELENGTH":
                        if (parameters.Length < length)
                        {
                            this.ErrorString = "SKIP_NAME";
                        }
                        break;
                    case "MAXIMUMNAMELENGTH":
                        if (dataRow.ConfigCWIFormat.ToString().Trim().ToUpper() == "TRUNCATE_NAME")
                        {
                            functionresult = parameters.Length <= length ? parameters : parameters.Substring(0, length);
                        }
                        if (parameters.Length > length)
                        {
                            this.ErrorString = "SKIP_NAME";
                        }
                        break;
                }
            }
            catch (Exception ex)
            {
                this.ErrorString = "Error in CheckReqeustNameLength: " + ex;
            }
            return;
        }

        private void SwitchToFrames(RemoteWebDriver driver, ConfigurableCWIStepsModel dataRow)
        {
            try
            {
                if (dataRow.ConfigCWIParameter.ToString().Trim().Length > 0)
                {
                    if (IsNumeric(dataRow.ConfigCWIParameter.ToString().Trim()))
                    {
                        int frameNumber;
                        int.TryParse(dataRow.ConfigCWIParameter.ToString(), out frameNumber);
                        driver.SwitchTo().Frame(frameNumber);
                    }
                    else
                    {
                        driver.SwitchTo().Frame(dataRow.ConfigCWIParameter.ToString().Trim());
                    }
                }
                else
                {
                    driver.SwitchTo().DefaultContent();
                }
            }
            catch (Exception ex)
            {
                this.ErrorString = "Error in NavigateBetweenFrames: " + ex;
            }
        }

        private void ReadParameters()
        {
            try
            {
                this.firstname = ResponseModel.Screening[0].ScreeningRequest.PersonName.FirstName;
                this.lastname = ResponseModel.Screening[0].ScreeningRequest.PersonName.LastName;
                this.dateOfBirth = ResponseModel.Screening[0].ScreeningRequest.PersonName.DateOfBirth;
                this.mongoID = ResponseModel.Screening[0].ScreeningId;
                this.transactionId = ResponseModel.Screening[0].TransactionId;
                this.screeningID = ResponseModel.Screening.First().ScreeningId;
                this.parentVendor = ResponseModel.Screening.First().ScreeningRequest.Vendor;

                if (ConfigDetail.Any(x => x.FieldName == "RemoteWebDriverURL"))
                {
                    this.remoteWebDriverURL = ConfigDetail.Where(x => x.FieldName == "RemoteWebDriverURL").First().FieldValue;
                }
                if (ConfigDetail.Any(x => x.FieldName == "AddOnDllPath"))
                {
                    this.addOnDllPath = ConfigDetail.Where(x => x.FieldName == "AddOnDllPath").First().FieldValue;
                }

                if (string.IsNullOrWhiteSpace(ResponseModel.Screening.First().ScreeningRequest.County))
                {
                    ResponseModel.Screening.First().ScreeningRequest.County = string.Empty;
                }
                if (string.IsNullOrWhiteSpace(ResponseModel.Screening.First().ScreeningRequest.Region))
                {
                    ResponseModel.Screening.First().ScreeningRequest.Region = string.Empty;
                }
                if (ConfigDetail.Any(x => x.FieldName == "MinimumFirstNameLength"))
                {
                    this.minimumFirstNameLength = ConfigDetail.Where(x => x.FieldName == "MinimumFirstNameLength").First().FieldValue;
                }

                ////Check the Steps of Configurable CWI
                ConfigCWIModel = (ConfigCWIModel)DataDictionary["ConfigCWIModel"];
                configurableCWISteps = ConfigCWIModel.ConfigurableCWISteps;
                if (configurableCWISteps == null || configurableCWISteps.Count == 0)
                {
                    this.ErrorString = "Steps for configurable CWI not present in Data Table";
                    return;
                }
            }
            catch (Exception ex)
            {
                this.ErrorString = "Error in ReadParameters: " + ex;
            }
        }

        private void ElementOperations(RemoteWebDriver driver, ConfigurableCWIStepsModel dataRow, ref bool exitIteration)
        {
            try
            {
                isElementPresent = false;
                ElementOperations(driver, dataRow.Step.ToString(), dataRow.StepType.ToString(), dataRow.ConfigCWIParameter.ToString(), rowData);
                if (dataRow.Step.ToString().ToUpper() == "CHECKELEMENT" && !isElementPresent)
                {
                    ////As the Clear Case Flag is True, Exiting the iteration
                    this.ErrorString = string.Empty;
                    exitIteration = true;
                }
            }
            catch (Exception ex)
            {
                this.ErrorString = "Error in SubmitElement: " + ex;
            }
        }

        private void ElementOperations(RemoteWebDriver driver, string step, string stepType, string parameter, string data)
        {
            IWebElement element;
            try
            {
                ////Depending on Step type, extract the element
                switch (stepType.ToUpper().Trim())
                {
                    case "FINDELEMENTBYID":
                    case "GETELEMENTBYID":
                        element = driver.FindElement(By.Id(parameter));
                        break;
                    case "FINDELEMENTBYNAME":
                    case "GETELEMENTBYNAME":
                        element = driver.FindElement(By.Name(parameter));
                        break;
                    case "FINDELEMENTBYXPATH":
                    case "GETELEMENTBYXPATH":
                        element = driver.FindElement(By.XPath(parameter));
                        break;
                    case "FINDELEMENTBYLINKTEXT":
                    case "GETELEMENTBYLINKTEXT":
                        element = driver.FindElement(By.LinkText(parameter));
                        break;
                    default:
                        this.ErrorString = "Error in StepType in Function ClickOnElement";
                        return;
                }
                ////Check the extracted element
                if (element == null)
                {
                    isElementPresent = false;
                    this.ErrorString = "Element not Found: " + parameter.ToString();
                    return;
                }
                isElementPresent = true;
                ////Depending on the operation, perform action
                switch (step.ToUpper())
                {
                    case "TYPETEXT":
                        ////Insert the text in element
                        element.SendKeys(data);
                        break;
                    case "CLICK":
                        ////Check the type of parameter we need to find
                        element.Click();
                        break;
                    case "SELECTDROPDOWN":
                        ////Select the drop down element
                        new SelectElement(element).SelectByText(data);
                        break;
                    case "DESELECTDROPDOWN":
                        new SelectElement(element).DeselectByText(data);
                        break;
                    case "SUBMIT":
                        ////Submits the element
                        element.Submit();
                        break;
                    case "CHECKVALUE":
                        ////Checks the value attribute of the element
                        if (element.GetAttribute("value").Trim().ToUpper() != data.Trim().ToUpper())
                        {
                            this.ErrorString = "Value Attribute does not match for element: " + parameter;
                        }
                        break;
                    case "SETCURSORPOSITIONTOSTART":
                        element.SendKeys(Keys.Home);
                        break;
                    case "CHECKDROPDOWN":
                    case "CHECKDROPDOWNELEMENTS":
                        ////Check the Drop Down elements present in the list or not
                        SelectElement select = new SelectElement(element);
                        List<string> dropDownListText = select.Options.Select(x => x.Text).ToList();
                        if (dropDownListText.Count == 0)
                        {
                            this.ErrorString = "No Elements found in Dropdown List";
                            return;
                        }

                        List<string> dropDownElementList = data.Split('|').ToList<string>();
                        if (step.ToUpper() == "CHECKDROPDOWN" && dropDownListText.Count != dropDownElementList.Count)
                        {
                            this.ErrorString = "Drop Down Element count mismatch";
                            return;
                        }
                        ////Check all the elements
                        foreach (string dropDown in dropDownElementList)
                        {
                            if (!dropDownListText.Contains(dropDown.Trim().ToUpper()))
                            {
                                this.ErrorString = "Drop Down Element mismatch for: " + dropDown;
                                return;
                            }
                        }
                        break;
                    default:
                        break;
                }
            }
            catch (Exception ex)
            {
                this.ErrorString = "Error in SubmitElement: " + ex;
            }
            finally
            {
                element = null;
            }
        }

        private void PerformPosting(RemoteWebDriver driver, string url)
        {
            try
            {
                ////Navigate to the URL
                driver.Navigate().GoToUrl(url);
                websiteResponse = driver.PageSource;
                ////Check Error String
                if (this.ErrorString.Trim().Length > 0)
                {
                    ResponseModel.ErrorDescription = this.ErrorString;
                    ResponseModel.ErrorCode = ErrorCode.HttpConnectionFailure;
                    ResponseModel.Status = OrderStatus.Retry;
                }
            }
            catch (Exception ex)
            {
                this.ErrorString = "Error in PerformPosting: " + ex;
            }
        }

        private void SetGlobalVariable(ConfigurableCWIStepsModel dataRow, string rowData)
        {
            try
            {
                ////Set/Reset variables as per the StepType Provided
                switch (dataRow.StepType.ToString().ToUpper().Trim())
                {
                    case "RESETRESPONSEPAGELIST":
                        responsePageList = new List<ResponsePageInfo>();
                        break;
                    case "CASEINFOEXTRACTED":
                        if (dataRow.ConfigCWIParameter.ToString().ToUpper().Trim() == "Y" ||
                        dataRow.ConfigCWIParameter.ToString().ToUpper().Trim() == "YES" ||
                        dataRow.ConfigCWIParameter.ToString().ToUpper().Trim() == "TRUE")
                        {
                            ResponseModel.Screening[0].CaseInfoExtracted = true;
                            //// Set additional search counter to max of name list as all the names are searched
                            additionalSearchCounter = firstNameList.Count;
                        }
                        else ResponseModel.Screening[0].CaseInfoExtracted = false;
                        break;
                    case "PAGING":
                    case "MULTIPLELINK":
                        if (dataRow.ConfigCWIParameter.ToString().ToUpper().Trim() == "Y" ||
                        dataRow.ConfigCWIParameter.ToString().ToUpper().Trim() == "YES" ||
                        dataRow.ConfigCWIParameter.ToString().ToUpper().Trim() == "TRUE")
                        {
                            this.hasPagingOrMultilpleLink = true;
                        }
                        else
                        {
                            this.hasPagingOrMultilpleLink = false;
                        }
                        break;
                    case "CONTINUEPOSTING":
                        if (dataRow.ConfigCWIParameter.ToString().ToUpper().Trim() == "Y" ||
                        dataRow.ConfigCWIParameter.ToString().ToUpper().Trim() == "YES" ||
                        dataRow.ConfigCWIParameter.ToString().ToUpper().Trim() == "TRUE")
                        {
                            this.continuePosting = true;
                        }
                        else
                        {
                            this.continuePosting = false;
                        }
                        break;
                    case "DATEFORMAT":
                        ////Used for posting or inserting date inputs on Website
                        try
                        {
                            if (dataRow.ConfigCWIParameter.ToString().Trim().Length > 0)
                            {
                                string temp = string.Empty;
                                if (!string.IsNullOrWhiteSpace(ResponseModel.Screening.First().ScreeningRequest.PersonName.DateOfBirth))
                                {
                                    temp = Convert.ToDateTime(ResponseModel.Screening.First().ScreeningRequest.PersonName.DateOfBirth).ToString(dataRow.ConfigCWIParameter.ToString().Trim());
                                    this.dateOfBirth = temp.Trim();
                                }
                                temp = Convert.ToDateTime(DateTime.Now).ToString(dataRow.ConfigCWIParameter.ToString().Trim());
                                curentDate = temp.Trim();
                            }
                        }
                        catch
                        {
                            this.dateOfBirth = this.dateOfBirth.Trim();
                            curentDate = DateTime.Now.ToString("MM/dd/yyyy");
                        }
                        break;
                    case "COUNTERFORMAT":
                        ////Used for posting or inserting date inputs on Website
                        if (dataRow.ConfigCWIParameter.ToString().Trim().Length > 0)
                        {
                            counterFormat = dataRow.ConfigCWIParameter.ToString().Trim();
                        }
                        else
                        {
                            counterFormat = string.Empty;
                        }
                        break;
                    case "COUNTER":
                        ////Counter used for any repeatative operation
                        int.TryParse(dataRow.ConfigCWIParameter.ToString(), out counter);
                        break;
                    case "PAGINGCOUNTER":
                        ////Counter used for Paging operation
                        int.TryParse(dataRow.ConfigCWIParameter.ToString(), out pagingCounter);
                        break;
                    case "COUNTERMAXLIMIT":
                        ////Max value allowed for the Counter
                        int.TryParse(dataRow.ConfigCWIParameter.ToString(), out counterMaxLimit);
                        break;
                    case "MULTIPLERESULTPAGES":
                        ////For website having more than 1 response Pages. Set it true just before the multiple page response
                        if (dataRow.ConfigCWIParameter.ToString().ToUpper().Trim() == "Y" ||
                        dataRow.ConfigCWIParameter.ToString().ToUpper().Trim() == "YES" ||
                        dataRow.ConfigCWIParameter.ToString().ToUpper().Trim() == "TRUE")
                        {
                            this.hasMultipleResultPage = true;
                            ////Reset the Check Result Table Variable
                            this.checkResultTable = true;
                        }
                        else
                        {
                            this.hasMultipleResultPage = false;
                            ////Reset the Check Result Table Variable
                            this.checkResultTable = true;
                        }
                        break;
                    case "RESETTABLECOLUMNS":
                        ////Reset the columns of data table
                        this.ResultTableColumnNames = new List<ResultTableColumns>();
                        ////Reset the first row data
                        FirstRowText = string.Empty;
                        break;
                    case "FORMATCOLUMNTEXT":
                        this.formatColumnText = dataRow.ConfigCWIParameter.ToString();
                        break;
                    case "CHECKCONTINUERESULTANALYSIS":
                        if (dataRow.ConfigCWIParameter.ToString().ToUpper().Trim() == "Y" ||
                        dataRow.ConfigCWIParameter.ToString().ToUpper().Trim() == "YES" ||
                        dataRow.ConfigCWIParameter.ToString().ToUpper().Trim() == "TRUE")
                        {
                            this.continueResultAnalysis = true;
                        }
                        else
                        {
                            this.continueResultAnalysis = false;
                        }
                        break;
                    case "RESETPOSTSTRING":
                        this.hashValues = new Dictionary<string, string>();
                        this.postString = string.Empty;
                        break;
                    case "RESETHTTPMANAGER":
                        HttpManager = null;
                        HttpManager = new HttpManager();
                        break;
                    case "SETAUTHENTICATIONUSERNAME":
                        HttpManager.AuthenticationUsername = dataRow.ConfigCWIParameter.ToString().Trim();
                        break;
                    case "SETAUTHENTICATIONPASSWORD":
                        HttpManager.AuthenticationPassword = dataRow.ConfigCWIParameter.ToString().Trim();
                        break;
                    case "SETAUTHENTICATIONURL":
                        HttpManager.AuthenticationURL = dataRow.ConfigCWIParameter.ToString().Trim();
                        break;
                    case "SETAUTHENTICATIONTYPE":
                        switch (dataRow.ConfigCWIParameter.ToString().Trim().ToUpper())
                        {
                            case "NONE":
                                HttpManager.AuthenticationType = HttpManager.AuthType.None;
                                break;
                            case "PREWINDOWSAUTHENTICATION":
                                HttpManager.AuthenticationType = HttpManager.AuthType.PreWindowsAuthentication;
                                break;
                            case "WINDOWSAUTHENTICATION":
                                HttpManager.AuthenticationType = HttpManager.AuthType.WindowsAuthentication;
                                break;
                            default:
                                this.ErrorString = "Error in SetGlobalVariable: Invalid Authentication type provided";
                                break;
                        }
                        break;
                    case "RETRYONMAXCOUNTERLIMIT":
                        if (dataRow.ConfigCWIParameter.ToString().ToUpper().Trim() == "Y" ||
                        dataRow.ConfigCWIParameter.ToString().ToUpper().Trim() == "YES" ||
                        dataRow.ConfigCWIParameter.ToString().ToUpper().Trim() == "TRUE")
                        {
                            this.isRetryOnMaxCounterLimit = true;
                        }
                        else
                        {
                            this.isRetryOnMaxCounterLimit = false;
                        }
                        break;
                    //case "RESETCLEARFLAG":
                    //    if (isClearCase)
                    //    {
                    //        this.isClearCase = false;
                    //    }
                    //    else
                    //    {
                    //        this.ErrorString = "Clear case flag was not set before resetting. Please check your steps";
                    //    }
                    //    break;
                    case "RESETADDITIONALSEARCHCOUNTER":
                        additionalSearchCounter = 0;
                        this.firstname = firstNameList[additionalSearchCounter];
                        this.lastname = lastNameList[additionalSearchCounter];
                        break;
                    case "DONOTUSEHASHTABLEFORPOSTING":
                        DoNotUseHashTableForPosting = true;
                        break;
                    default:
                        Session[dataRow.StepType.ToString()] = rowData;
                        return;
                }
            }
            catch (Exception ex)
            {
                this.ErrorString = "Error in SetGlobalVariable: " + ex;
                return;
            }
        }

        private void GetColumnDetails(ConfigurableCWIStepsModel columnDetailRow)
        {
            ResultTableColumns column;
            Check check;
            try
            {
                switch (columnDetailRow.StepType.ToString().ToUpper())
                {
                    case "COLUMNNAME":
                        ////Assign the values to new column
                        column = new ResultTableColumns();
                        ////Read all the values from Excel and assign to new column
                        column.Name = columnDetailRow.ConfigCWIParameter.ToString();
                        column.Logic = columnDetailRow.Logic.ToString();
                        column.Verify = columnDetailRow.Verify.ToString();
                        if (columnDetailRow.Clickable.ToString() != null && columnDetailRow.Clickable.ToString().Trim().Length > 0
                            && (columnDetailRow.Clickable.ToString().Trim().ToUpper() == "YES" || columnDetailRow.Clickable.ToString().Trim().ToUpper() == "Y"
                            || columnDetailRow.Clickable.ToString().Trim().ToUpper() == "TRUE"))
                        {
                            column.Clickable = true;
                        }
                        column.Format = columnDetailRow.ConfigCWIFormat.ToString();
                        column.MapTo = columnDetailRow.MapTo.ToString();
                        ////Add the new column in list of Result Table Column Names
                        this.ResultTableColumnNames.Add(column);
                        break;
                    case "COLUMNCHECK":
                        ////Create a new check for the result table column
                        check = new Check();
                        ////Read all the values from Excel and assign to new column
                        check.Value = rowData.ToString();
                        check.Type = columnDetailRow.ConfigCWIParameter.ToString();
                        check.Logic = columnDetailRow.Logic.ToString();
                        check.Format = columnDetailRow.ConfigCWIFormat.ToString();                        
                        ////Add the Check in the column
                        this.ResultTableColumnNames[this.ResultTableColumnNames.Count - 1].Checks.Add(check);
                        break;
                    default:
                        this.ErrorString = "Error in StepType Value";
                        break;
                }
            }
            catch (Exception ex)
            {
                this.ErrorString = "Error in GetColumnDetails: " + ex;
            }
        }

        private void CheckTable(DataTable resultDataTable, bool isReviewCase, bool isClearCase, ConfigurableCWIStepsModel dataRow)
        {
            try
            {
                if (dataRow.Data.ToString().ToUpper().IndexOf("DO_NOT_RESET_LINK_LIST") > -1)
                {
                    //// Do not reset the link lists
                }
                else
                {
                    //// Reset the number of links list
                    innerHtmlValueList = new List<string>();
                    clickableLinks = new List<string>();
                }

                if (dataRow.Data.ToString().ToUpper().IndexOf("REVERSE_COLUMN_NAMES") > -1)
                {
                    this.ResultTableColumnNames.Reverse();
                }

                if (resultDataTable == null || resultDataTable.Rows == null || resultDataTable.Rows.Count == 0)
                {
                    if (!isReviewCase && isClearCase)
                    {
                        this.ErrorString = "CLEAR";
                        return;
                    }
                    else if (isReviewCase)
                    {
                        this.ErrorString = "REVIEW";
                        return;
                    }
                    else
                    {
                        ////neither REVIEW nor CLEAR was set. Hence set an error message.
                        logger.Info("Error in Result Page Check, NO rows found and Clear Case String missing");
                        this.ErrorString = "ERROR in Result Page Check, NO rows found and Clear Case String missing.";
                        return;
                    }
                }

                ////Check if all the columns are presend in the Data Table or not
                if (!this.IsAllColumnExist(resultDataTable, this.ResultTableColumnNames.Select(x => x.Name).ToList()))
                {
                    this.ErrorString = "All columns not found in Result Table";
                    return;
                }
                ////Iterate all the rows of the result table

                bool isFirstRow = true;
                bool isReview = false;
                bool isFirst = true;
                bool addLink = true;
                Case websiteCase;
                string rowText = string.Empty;
                logger.Debug("Iterating all the rows of the table and applying all the checks mentioned in Charles Proxy");
                foreach (DataRow row in resultDataTable.Rows)
                {
                    if (isFirstRow == true && rowData.Trim().ToUpper() != "NO_FIRST_ROW_CHECK")
                    {
                        foreach (var column in row.ItemArray)
                            rowText += column.ToString().ToUpper().Trim();

                        if (FirstRowText.ToUpper().Trim() == rowText.Trim().ToUpper())
                        {
                            logger.Info("Same Page as previous page received. Hence setting the error string");
                            this.ErrorString = "SAME PAGE";
                            return;
                        }
                        else
                        {
                            FirstRowText = rowText;
                        }
                    }
                    isFirstRow = false;
                    isReview = false;
                    isFirst = true;
                    addLink = true;
                    websiteCase = new Case() { AdditionalInformations = new List<PRISM.Common.Models.Screening.AdditionalInformation>(), Charges = new List<Charge>(), Defendants = new List<Defendant>(), Docket = new List<Docket>() };

                    ////Check all the columns of the row
                    foreach (ResultTableColumns column in this.ResultTableColumnNames)
                    {
                        //// Map the value to concerned field of Case object
                        if (!string.IsNullOrWhiteSpace(column.MapTo))
                        {
                            string valueTobeMapped = column.Format.ToUpper().Equals("HTML") ? row[column.Name + "_ORG"].ToString() : row[column.Name].ToString();
                            MapToSpecifiedField(websiteCase, column, valueTobeMapped);
                            if (!string.IsNullOrWhiteSpace(this.ErrorString)) return;
                        }
                        if (column.Verify.ToUpper().Trim() == "Y" || column.Verify.ToUpper().Trim() == "YES" || column.Verify.ToUpper().Trim() == "TRUE")
                        {
                            ////Apply all the checks specified on a column
                            foreach (Check check in column.Checks)
                            {
                                check.IsTrue = false;
                                switch (check.Type.ToUpper().Trim())
                                {
                                    case "EXACT":           ////The string should exactly match the column value
                                        if (check.Value.ToUpper().Equals(row[column.Name].ToString().ToUpper().Trim()) == true)
                                        {
                                            check.IsTrue = true;
                                        }
                                        break;
                                    case "CONTAINS":        ////The string should be present in column value
                                        if (row[column.Name].ToString().ToUpper().IndexOf(check.Value.ToUpper().Trim()) > -1)
                                        {
                                            check.IsTrue = true;
                                        }
                                        break;
                                    case "NOCHECKS":        ////Only table is present and NO further checks on columns and rows
                                                            ////Set Review Status to True only when we do not need to do further postings.
                                        if (column.Clickable == false)
                                        {
                                            check.IsTrue = true;
                                        }
                                        break;
                                    case "ANY_STRING":      ////Any value present in column value
                                        if (check.Value.Trim().Length == 0)
                                        {
                                            if (row[column.Name].ToString().Trim().Length > 0)
                                            {
                                                check.IsTrue = true;
                                            }
                                        }
                                        else
                                        {
                                            check.IsTrue = CheckColumnStringData(row[column.Name].ToString(), check.Value.Trim(), check.Format);
                                            if (this.ErrorString.Trim().Length > 0)
                                            {
                                                return;
                                            }
                                        }
                                        break;
                                    case "ISNUMERIC":
                                        if (Regex.IsMatch(row[column.Name].ToString().Trim(), @"^(\d)+$"))
                                        {
                                            check.IsTrue = true;
                                        }
                                        break;
                                    case "DOB":
                                        check.IsTrue = CheckDOB(row[column.Name].ToString().Trim().Replace("&nbsp;", ""), check.Value.Trim(), check.Format);
                                        if (this.ErrorString.Trim().Length > 0 && this.ErrorString.Trim().ToUpper() != "REVIEW")
                                        {
                                            return;
                                        }
                                        break;
                                    case "SPLIT_FULLNAME_MATCH_LNFN":
                                    case "SPLIT_FULLNAME_MATCH_LNFNMN":
                                    case "SPLIT_FULLNAME_MATCH_LNFN_CHECKLENGTH_LNFN":
                                    case "SPLIT_FULLNAME_MATCH_LNFN_CHECKLENGTH_LN":
                                    case "SPLIT_FULLNAME_MATCH_LNFN_CHECKLENGTH_FN":
                                    case "SPLIT_FULLNAME_MATCH_LNFNMN_CHECKLENGTH_LNFN":
                                    case "SPLIT_FULLNAME_MATCH_LNFNMN_CHECKLENGTH_LN":
                                    case "SPLIT_FULLNAME_MATCH_LNFNMN_CHECKLENGTH_FN":
                                        check.IsTrue = SplitAndMatchFullName(row[column.Name].ToString(), check.Type, check.Value, check.Format);
                                        if (this.ErrorString.Trim().Length > 0)
                                        {
                                            return;
                                        }
                                        break;
                                    case "CONTAINS_AT_SPECIFIC_POSITION":
                                        check.IsTrue = ContainsAtSpecificPosition(row[column.Name].ToString(), check.Value, check.Format);
                                        if (this.ErrorString.Trim().Length > 0)
                                        {
                                            return;
                                        }
                                        break;
                                    case "CHECK_CELL_CONTENTS":
                                        check.IsTrue = CheckCellContents(row[column.Name].ToString(), check);
                                        if (this.ErrorString.Trim().Length > 0)
                                        {
                                            return;
                                        }
                                        break;
                                    case "ISREGEXMATCH":
                                        check.IsTrue = CheckISRegexMatch(row[column.Name].ToString(), check.Value, check.Format);
                                        if (this.ErrorString.Trim().Length > 0)
                                        {
                                            return;
                                        }
                                        break;
                                    default:
                                        this.ErrorString = "Incorrect Check Type Provided.";
                                        return;
                                }
                                if (check.Format.IndexOf("REVERSE", StringComparison.OrdinalIgnoreCase) > -1)
                                {
                                    check.IsTrue = !check.IsTrue;
                                    if (this.ErrorString.Trim().ToUpper() == "REVIEW")
                                    {
                                        this.ErrorString = "";
                                        break;
                                    }
                                }
                                if (this.ErrorString.Trim().ToUpper() == "REVIEW")
                                {
                                    break;
                                }                                
                            }

                            ////Apply logic specified on all the column check results
                            if (column.Checks.Count > 1 && string.IsNullOrEmpty(this.ErrorString))
                            {
                                if (column.Checks[0].Logic.Trim().ToUpper() == "CONTINUEOR")
                                {
                                    saveResults.Add(column.Checks[0].IsTrue);
                                    continueResultAnalysis = continueResultAnalysis || column.Checks[0].IsTrue;
                                    addLink = addLink || column.Checks[0].IsTrue;
                                }
                                else if (column.Checks[0].Logic.Trim().ToUpper() == "CONTINUEAND")
                                {
                                    saveResults.Add(column.Checks[0].IsTrue);
                                    continueResultAnalysis = continueResultAnalysis && column.Checks[0].IsTrue;
                                    addLink = addLink && column.Checks[0].IsTrue;
                                }
                                else
                                {
                                    column.SatisfiesChecks = column.Checks[0].IsTrue;
                                }
                                for (int i = 1; i < column.Checks.Count; i++)
                                {
                                    if (column.Checks[i].Logic.Trim().ToUpper() == "OR")
                                    {
                                        column.SatisfiesChecks = column.SatisfiesChecks || column.Checks[i].IsTrue;
                                    }
                                    else if (column.Checks[i].Logic.Trim().ToUpper() == "AND")
                                    {
                                        column.SatisfiesChecks = column.SatisfiesChecks && column.Checks[i].IsTrue;
                                    }
                                    else if (column.Checks[i].Logic.Trim().ToUpper() == "CONTINUEOR")
                                    {
                                        saveResults.Add(column.Checks[i].IsTrue);
                                        continueResultAnalysis = continueResultAnalysis || column.Checks[i].IsTrue;
                                        addLink = addLink || column.Checks[i].IsTrue;
                                    }
                                    else if (column.Checks[i].Logic.Trim().ToUpper() == "CONTINUEAND")
                                    {
                                        saveResults.Add(column.Checks[i].IsTrue);
                                        continueResultAnalysis = continueResultAnalysis && column.Checks[i].IsTrue;
                                        addLink = addLink && column.Checks[i].IsTrue;
                                    }
                                }
                            }
                            else if (column.Checks.Count == 1 && string.IsNullOrEmpty(this.ErrorString))
                            {
                                if (column.Checks[0].Logic.Trim().ToUpper() == "CONTINUEOR")
                                {
                                    saveResults.Add(column.Checks[0].IsTrue);
                                    continueResultAnalysis = continueResultAnalysis || column.Checks[0].IsTrue;
                                    addLink = addLink || column.Checks[0].IsTrue;
                                }
                                else if (column.Checks[0].Logic.Trim().ToUpper() == "CONTINUEAND")
                                {
                                    saveResults.Add(column.Checks[0].IsTrue);
                                    continueResultAnalysis = continueResultAnalysis && column.Checks[0].IsTrue;
                                    addLink = addLink && column.Checks[0].IsTrue;
                                }
                                else
                                {
                                    column.SatisfiesChecks = column.Checks[0].IsTrue;
                                }
                            }
                            else if (this.ErrorString.Trim().ToUpper() == "REVIEW")
                            {
                                column.SatisfiesChecks = true;
                                this.ErrorString = string.Empty;
                            }

                            //// Use very cautiously. With proper testing
                            if (column.Format.ToUpper().Trim() == "EXIT_ON_CLEAR" && !column.SatisfiesChecks)
                            {
                                //this.ErrorString = "CLEAR";
                                functionresult = "EXIT_ON_CLEAR";
                                return;
                            }
                            ////Apply logic specified on all the row check result
                            if (isFirst)
                            {
                                isFirst = false;
                                isReview = column.SatisfiesChecks;
                            }
                            else
                            {
                                if (column.Logic.Trim().ToUpper() == "OR")
                                {
                                    isReview = isReview || column.SatisfiesChecks;
                                }
                                else if (column.Logic.Trim().ToUpper() == "AND")
                                {
                                    isReview = isReview && column.SatisfiesChecks;
                                }
                            }

                            if (!addLink)
                            {
                                clickableLinks.RemoveAt(clickableLinks.Count - 1);
                                innerHtmlValueList.RemoveAt(innerHtmlValueList.Count - 1);
                            }

                            //// Check if review condition is met or not. If not met and all the other logic are "AND" then go for next row
                            if (!isReview && !ResultTableColumnNames.Any(x => x.Logic.ToUpper() == "OR" &&
                            (x.Verify.ToUpper() == "YES" || x.Verify.ToUpper() == "Y" || x.Verify.ToUpper() == "TRUE")))
                            {
                                break;
                            }
                        }
                        ////Check if we have to click on link present in the column
                        if (column.Clickable == true && addLink)
                        {
                            //// Add the link to Extra Info of the case
                            websiteCase.ExtraInfo = "<CASE_URL>" + row[column.Name + "_ORG"].ToString() + "</CASE_URL><FN>" + firstname + "</FN><LN>" + lastname + "</LN>";
                            websiteCase.UrlCode = parentVendor;
                            ////Add the XPath
                            clickableLinks.Add(row[column.Name + "_XPATH"].ToString());
                            //// Add the innerHtml to list
                            innerHtmlValueList.Add(row[column.Name + "_ORG"].ToString());
                            this.ErrorString = "";
                        }
                    }
                    if (dataRow.Data.ToString().Trim().IndexOf("ADD_LINK_ON_REVIEW") > -1)
                    {
                        if (!isReview)
                        {
                            clickableLinks.RemoveAt(clickableLinks.Count - 1);
                            innerHtmlValueList.RemoveAt(innerHtmlValueList.Count - 1);
                        }
                        isReview = false;
                    }
                    if (dataRow.Data.ToString().Trim().IndexOf("ADD_CASE_ON_REVIEW") > -1)
                    {
                        if (isReview && !ResponseModel.Screening[0].ScreeningResponse.Cases.Contains(websiteCase, new CaseCompare()))
                        {
                            ResponseModel.Screening[0].ScreeningResponse.Cases.Add(websiteCase);
                        }

                        //// Resetting the Review flag so as to continue to other cases for Config CWI
                        isReview = false;
                    }
                    ////Exit loop if review, else continue
                    if (isReview)
                    {
                        break;
                    }
                }

                if (dataRow.Data.ToString().ToUpper().Trim() == "REVERSE_COLUMN_NAMES")
                {
                    this.ResultTableColumnNames.Reverse();
                }
                if (isReview)
                {
                    this.ErrorString = "REVIEW";
                }
                else if ((clickableLinks.Count > 0 && this.continuePosting == true) || ResponseModel.Screening[0].ScreeningResponse.Cases.Count() > 0)
                {
                    this.ErrorString = "";
                    return;
                }
                else
                {
                    this.ErrorString = "CLEAR";
                }
            }
            catch (Exception ex)
            {
                this.ErrorString = "Error in CheckResultTable: " + ex;
            }
        }

        private void MapToSpecifiedField(Case websiteCase, ResultTableColumns column, string valueToBeMapped)
        {
            try
            {
                switch (column.MapTo.ToUpper().Trim())
                {
                    case "CASE|CASENUMBER":
                        websiteCase.CaseNumber = valueToBeMapped;
                        break;
                    case "CASE|CASETYPE":
                        websiteCase.CaseType = valueToBeMapped;
                        break;
                    case "CASE|CASEFILEDDATE":
                        websiteCase.CaseFiledDate = valueToBeMapped;
                        break;
                    case "CASE|CASESTATUS":
                        websiteCase.CaseStatus = valueToBeMapped;
                        break;
                    case "CASE|CITATIONNUMBER":
                        websiteCase.CitationNumber = valueToBeMapped;
                        break;
                    case "CASE|COURTNAME":
                        websiteCase.CourtName = valueToBeMapped;
                        break;
                    case "CASE|DEFENDANT|DATEOFBIRTH":
                        MapDefendantFields(websiteCase, column, valueToBeMapped);
                        if (!string.IsNullOrWhiteSpace(this.ErrorString)) return;
                        break;
                    default:
                        this.ErrorString = "Error in MapToSpecifiedField: Incorrect MapTo specified";
                        break;
                }
            }
            catch (Exception ex)
            {
                this.ErrorString = "Error in MapToSpecifiedField: " + ex;
                return;
            }
        }
        
        private void MapDefendantFields(Case websiteCase, ResultTableColumns column, string valueToBeMapped)
        {
            try
            {
                if (websiteCase.Defendants.Count == 0)
                    websiteCase.Defendants.Add(new Defendant() { PersonName = new PersonName() { PersonType = PersonType.Defendant } });

                switch (column.MapTo.ToUpper().Trim())
                {
                    case "CASE|DEFENDANT|DATEOFBIRTH":
                        DateTime tempDOB;
                        if (string.IsNullOrWhiteSpace(column.Format) && !DateTime.TryParseExact(valueToBeMapped, column.Format, CultureInfo.InvariantCulture, DateTimeStyles.AllowWhiteSpaces, out tempDOB))
                        {
                            this.ErrorString = "Error in MapDefendantFields: Value to be mapped in DOB field is not in specified date format";
                            return;
                        }
                        websiteCase.Defendants[0].PersonName.DateOfBirth = valueToBeMapped;
                        break;
                    default:
                        this.ErrorString = "Error in MapDefendantFields: Incorrect MapTo specified";
                        break;
                }
            }
            catch (Exception ex)
            {
                this.ErrorString = "Error in MapDefendantFields: " + ex;
                return;
            }
        }
        private bool ContainsAtSpecificPosition(string websiteValue, string valueToCheck, string format)
        {
            try
            {
                //// Check if RowData contains "^" separator
                if (!valueToCheck.Contains("^"))
                {
                    this.ErrorString = "Error in ContainsAtSpecificPosition: Value to be checked does not contains | separator";
                    return false;
                }
                //// Get the string to be checked and the position of the string
                string stringToMatch = valueToCheck.Split('^')[0].ToUpper();
                int matchPosition;
                if (!int.TryParse(valueToCheck.Split('^')[1], out matchPosition))
                {
                    this.ErrorString = "Error in ContainsAtSpecificPosition: Position is not in integer format";
                    return false;
                }
                //// To check if the string has number
                websiteValue = websiteValue.ToUpper().Replace("\r", "").Replace("\n", "");
                int indexOfRequiredString = -1;
                if (stringToMatch.Trim().ToUpper() == "CHECK_NUMERIC")
                {
                    indexOfRequiredString = websiteValue.IndexOfAny("0123456789".ToCharArray());
                }
                else
                {
                    indexOfRequiredString = websiteValue.IndexOf(stringToMatch);
                }
                if (indexOfRequiredString == matchPosition)
                {
                    return true;
                }
                return false;
            }
            catch (Exception ex)
            {
                this.ErrorString = "Error in ContainsAtSpecificPosition: " + ex;
                return false;
            }
        }

        private bool SplitAndMatchFullName(string fullName, string checkType, string checkValue, string checkFormat)
        {
            NameHelper nameHelper = new NameHelper();
            try
            {
                //// Split Names
                SplitName splitName = new SplitName();
                splitName.FullName = fullName;
                switch (checkFormat.Trim().ToUpper())
                {
                    case "FNLNMN":
                        splitName.FormatOfName = SplitName.NameFormat.FNLNMN;
                        break;
                    case "FNMNLN":
                        splitName.FormatOfName = SplitName.NameFormat.FNMNLN;
                        break;
                    case "LNFNMN":
                        splitName.FormatOfName = SplitName.NameFormat.LNFNMN;
                        break;
                    case "LNMNFN":
                        splitName.FormatOfName = SplitName.NameFormat.LNMNFN;
                        break;
                    default:
                        this.ErrorString = "Incorrect Format Specified for SplitAndMatchFullName";
                        return false;
                }
                if (firstname.Trim().IndexOf(" ") > -1 || lastname.Trim().IndexOf(" ") > -1)
                {
                    splitName.MatchFN = firstname;
                    splitName.MatchLN = lastname;
                }
                splitName = nameHelper.GetLNFNMNSuffix(splitName);
                //// Clean Website Name
                splitName.FirstName = nameHelper.CleanName(splitName.FirstName);
                splitName.LastName = nameHelper.CleanName(splitName.LastName);
                splitName.MiddleName = nameHelper.CleanName(splitName.MiddleName);
                if (checkType.ToUpper().IndexOf("_CHECKLENGTH_") > -1)
                {
                    if (CheckWebsiteNameLength(splitName, checkType, checkValue))
                        return true;
                    else if (!string.IsNullOrWhiteSpace(this.ErrorString))
                        return false;
                }
                return FilterPartyName(checkType, splitName.LastName, splitName.FirstName, splitName.MiddleName);
            }
            catch (Exception ex)
            {
                this.ErrorString = "Error in SplitAndMatchFullName: " + ex;
                return false;
            }
            finally { nameHelper = null; }
        }

        private bool CheckWebsiteNameLength(SplitName splitName, string checkType, string checkValue)
        {
            int minimumNameLength;
            try
            {
                if (!int.TryParse(checkValue.Trim(), out minimumNameLength))
                {
                    this.ErrorString = "Error in CheckWebsiteNameLength: Value passed in Data is not an integer";
                    return false;
                }

                switch (checkType)
                {
                    case "SPLIT_FULLNAME_MATCH_LNFN_CHECKLENGTH_LNFN":
                    case "SPLIT_FULLNAME_MATCH_LNFNMN_CHECKLENGTH_LNFN":
                        if (splitName.LastName.Trim().Length < minimumNameLength || splitName.FirstName.Trim().Length < minimumNameLength)
                            return true;
                        break;
                    case "SPLIT_FULLNAME_MATCH_LNFN_CHECKLENGTH_LN":
                    case "SPLIT_FULLNAME_MATCH_LNFNMN_CHECKLENGTH_LN":
                        if (splitName.LastName.Trim().Length < minimumNameLength)
                            return true;
                        break;
                    case "SPLIT_FULLNAME_MATCH_LNFN_CHECKLENGTH_FN":
                    case "SPLIT_FULLNAME_MATCH_LNFNMN_CHECKLENGTH_FN":
                        if (splitName.FirstName.Trim().Length < minimumNameLength)
                            return true;
                        break;
                    default:
                        this.ErrorString = "Error in CheckWebsiteNameLength: invalid CheckType Provided";
                        return false; ;
                }


                return false;
            }
            catch (Exception ex)
            {
                this.ErrorString = "Error in CheckWebsiteNameLength: " + ex;
                return false;
            }
        }

        private bool FilterPartyName(string checkType, string lastName, string firstName, string middleName)
        {
            string requestMiddleName = string.Empty;
            try
            {
                for (int i = 0; i < firstNameList.Count; i++)
                {
                    firstNameList[i] = firstNameList[i].Trim().ToUpper();
                    lastNameList[i] = lastNameList[i].Trim().ToUpper();
                }
                if (firstNameList.Contains(firstName.Trim().ToUpper()) && lastNameList.Contains(lastName.Trim().ToUpper()))
                {
                    List<string> checkTypes = new List<string>() { "SPLIT_FULLNAME_MATCH_LNFN_CHECKLENGTH_LNFN",
                        "SPLIT_FULLNAME_MATCH_LNFN_CHECKLENGTH_LN","SPLIT_FULLNAME_MATCH_LNFN_CHECKLENGTH_FN","SPLIT_FULLNAME_MATCH_LNFN" };

                    if (checkTypes.Contains(checkType.Trim().ToUpper()))
                    {
                        return true;
                    }
                    requestMiddleName = string.Empty;
                    requestMiddleName = ResponseModel.Screening.First().ScreeningRequest.PersonName.MiddleName;
                    if (string.IsNullOrWhiteSpace(requestMiddleName))
                    {
                        requestMiddleName = string.Empty;
                    }
                    else
                    {
                        NameHelper nameHelper = new NameHelper();
                        requestMiddleName = nameHelper.CleanName(requestMiddleName);
                        requestMiddleName = requestMiddleName.Trim().ToUpper();
                        nameHelper = null;
                    }
                    middleName = middleName.Trim().ToUpper();
                    if (string.IsNullOrWhiteSpace(requestMiddleName) || string.IsNullOrWhiteSpace(middleName))
                    {
                        return true;
                    }
                    else if (requestMiddleName.Length == 1 && middleName.StartsWith(requestMiddleName))
                    {
                        return true;
                    }
                    else if (middleName.Length == 1 && requestMiddleName.StartsWith(middleName))
                    {
                        return true;
                    }
                    else if (requestMiddleName.Length > 1 && middleName.Equals(requestMiddleName))
                    {
                        return true;
                    }
                }
                return false;
            }
            catch (Exception ex)
            {
                this.ErrorString = "Error in FilterPartyName: " + ex;
                return false;
            }
        }

        private bool CheckColumnStringData(string columnData, string parameters, string format)
        {
            try
            {
                if (parameters.IndexOf("_") == -1)
                {
                    this.ErrorString = "Error in Parameters of Step. Parameter: " + parameters;
                    return false;
                }
                if (parameters.Split('_')[0] == "AFTER")
                {
                    if (parameters.Split('_')[1] == "NEWLINE")
                    {
                        if (columnData.Split(new string[] { "\r\n", "\n" }, StringSplitOptions.None).Count() > 1)
                        {
                            return true;
                        }
                        else
                        {
                            return false;
                        }
                    }
                    else if (parameters.Split('_')[1] == "NEWLINEDOB")
                    {
                        string websiteDob = string.Empty;
                        if (columnData.Split(new string[] { "\r\n", "\n" }, StringSplitOptions.None).Count() > 1)
                        {
                            websiteDob = columnData.Split(new string[] { "\r\n", "\n" }, StringSplitOptions.None)[1];
                        }
                        return CheckDOB(websiteDob, dateOfBirth, format);
                    }
                    else
                    {

                    }
                }
                else if (parameters.Split('_')[0] == "BEFORE")
                {

                }
                return false;
            }
            catch (Exception ex)
            {
                this.ErrorString = "Error in CheckColumnStringData: " + ex;
                return false;
            }
        }

        private DataTable ExtractResultDataTable(string websiteResponse, ConfigurableCWIStepsModel dataRow)
        {
            DataTable resultDataTable = new DataTable();
            try
            {
                //// Get the Step of Data Row
                string step = dataRow.Step.ToString();
                //// Get column names
                List<string> columnNames = new List<string>();
                columnNames = this.ResultTableColumnNames.Select(x => x.Name).ToList();
                //// Call the Extract function as per the step provided
                if (step.ToUpper().Trim() == "EXTRACTDATATABLE_ROWHEADER")
                {
                    resultDataTable = ExtractHtmlTableIntoDataTable_RowsAsHeader(websiteResponse, columnNames[0], columnNames.GetRange(1, columnNames.Count - 1).ToArray());
                }
                else if (step.ToUpper().Trim() == "EXTRACTHTMLTABLEINTODATATABLE")
                {
                    resultDataTable = ExtractHtmlTableIntoDataTable_New(websiteResponse, rowData, columnNames[0], columnNames.GetRange(1, columnNames.Count - 1).ToArray());
                }
                else if (step.ToUpper().Trim() == "EXTRACTDATATABLEFROMXML")
                {
                    resultDataTable = ExtractDataTableFromXML(dataRow, rowData);
                }
                else if (step.ToUpper().Trim() == "EXTRACTDATATABLEFROMJSON")
                {
                    resultDataTable = ExtractDataTableFromJson(websiteResponse, dataRow, rowData, columnNames);
                }
                else
                {
                    this.ErrorString = "Incorrect Step provided. Please check your steps";
                    return null;
                }
                return resultDataTable;
            }
            catch (Exception ex)
            {
                this.ErrorString = "Error in ExtractResultDataTable: " + ex;
                return null;
            }
        }

        private DataTable ExtractDataTableFromJson(string websiteResponse, ConfigurableCWIStepsModel dataRow, string rowData, List<string> columnNames)
        {
            DataTable resultTable = new DataTable();
            try
            {
                JToken websiteJSON = JObject.Parse(websiteResponse);

                JToken websiteColumnNames = websiteJSON.SelectToken(dataRow.ConfigCWIParameter.ToString());

                foreach (var name in websiteColumnNames.Children())
                {
                    if (columnNames.Contains(name.ToString().ToUpper().Trim()))
                    {
                        resultTable.Columns.Add(name.ToString());
                    }
                    else
                    {
                        this.ErrorString = "ERROR: New Column Found in JSON Object";
                        return null;
                    }
                }
                DataRow row;
                int i;
                JToken data = websiteJSON.SelectToken(rowData);

                foreach (var item in data.Children())
                {
                    if (item.Children().Count() == resultTable.Columns.Count)
                    {
                        row = resultTable.NewRow();
                        i = 0;
                        foreach (var column in item.Children())
                        {
                            row[i++] = column.ToString();
                        }
                        resultTable.Rows.Add(row);
                    }
                }

                return resultTable;
            }
            catch (Exception ex)
            {
                this.ErrorString = "Error in ExtractDataTableFromJson: " + ex;
                return null;
            }
        }

        /// <summary>
        /// Determines whether all column exist in the specified table name to check.
        /// </summary>
        /// <param name="tableNameToCheck">The data table to check.</param>
        /// <param name="columnsNames">The columns names.</param>
        /// <returns>False if any column is missing and true if all the columns are present</returns>
        private bool IsAllColumnExist(DataTable tableNameToCheck, List<string> columnsNames)
        {
            bool iscolumnExist = true;
            try
            {
                if (null != tableNameToCheck && tableNameToCheck.Columns != null)
                {
                    foreach (string columnName in columnsNames)
                    {
                        if (!tableNameToCheck.Columns.Contains(columnName) && columnName.Trim().Length > 0)
                        {
                            iscolumnExist = false;
                            break;
                        }
                    }
                }
                else
                {
                    iscolumnExist = false;
                }
            }
            catch (Exception)
            {
                iscolumnExist = false;
            }

            return iscolumnExist;
        }

        private DataTable ExtractHtmlTableIntoDataTable_New(string response, string rowData, string firstColumnNameToSerach, params string[] otherColumnNamesToSearch)
        {
            bool isTableFound = false;
            bool dataInNextTable = false;
            bool areTableColumnsMoreThanRequired = false;
            bool checkXpathOfColumns = false;
            bool useWebsiteColumns = true;
            bool doNotCheckFirstColumn = false;
            bool sameResultTableMultipleTimes = false;
            DataTable dt = new DataTable();
            HtmlDocument doc = new HtmlDocument();
            string xpathOfPreviousColumn = string.Empty;
            bool addRowToTable = true;
            response = response.Replace("<br />", "\n");
            doc.LoadHtml(response);
            var tablecollection = doc.DocumentNode.SelectNodes("//table");

            //// Check the Data Column for Data Available in Next Row
            if (!string.IsNullOrWhiteSpace(rowData) && rowData.ToUpper().Trim().IndexOf("DATA_IN_NEXT_TABLE") > -1)
            {
                dataInNextTable = true;
            }
            if (!string.IsNullOrWhiteSpace(rowData) && rowData.ToUpper().Trim().IndexOf("TABLE_COLUMNS_MORE_THAN_REQUIRED") > -1)
            {
                areTableColumnsMoreThanRequired = true;
            }
            if (!string.IsNullOrWhiteSpace(rowData) && rowData.ToUpper().Trim().IndexOf("CHECK_XPATH_OF_COLUMNS") > -1)
            {
                checkXpathOfColumns = true;
            }
            if (!string.IsNullOrWhiteSpace(rowData) && rowData.ToUpper().Trim().IndexOf("DO_NOT_USE_WEBSITE_COLUMNS") > -1)
            {
                useWebsiteColumns = false;
            }
            if (!string.IsNullOrWhiteSpace(rowData) && rowData.ToUpper().Trim().IndexOf("DO_NOT_CHECK_FIRST_COLUMN") > -1)
            {
                doNotCheckFirstColumn = true;
            }
            if (!string.IsNullOrWhiteSpace(rowData) && rowData.ToUpper().Trim().IndexOf("SAME_RESULT_TABLE_MULTIPLE_TIMES") > -1)
            {
                sameResultTableMultipleTimes = true;
                dataInNextTable = true;
            }

            if (tablecollection != null)
            {
                foreach (HtmlNode table in tablecollection)
                {
                    if (sameResultTableMultipleTimes && isTableFound && dataInNextTable)
                    {
                        dataInNextTable = true;
                    }
                    else if (dataInNextTable && isTableFound)
                    {
                        dataInNextTable = false;
                    }
                    else
                    {
                        isTableFound = false;
                    }
                    // for all rows with at least one child with the 'th'  or 'td' tag
                    int rowCount = 0;
                    foreach (HtmlNode row in table.Descendants().Where(desc => desc.Name.Equals("tr", StringComparison.OrdinalIgnoreCase) || desc.Name.Equals("thead", StringComparison.OrdinalIgnoreCase) || desc.Name.Equals("th", StringComparison.OrdinalIgnoreCase)
                                                                                &&
                                                                                (
                                                                                    desc.Descendants().Any(child => child.Name.Equals("th", StringComparison.OrdinalIgnoreCase))
                                                                                    ||
                                                                                    desc.Descendants().Any(child => child.Name.Equals("th", StringComparison.OrdinalIgnoreCase))
                                                                                    ||
                                                                                    desc.Descendants().Any(child => child.Name.Equals("th", StringComparison.OrdinalIgnoreCase))
                                                                                )
                                                                        ))
                    {
                        if (isTableFound == false)
                        {
                            ////Commenting the below line, as the table headers can be present in 2nd row as well.
                            ////if (rowCount == 0)
                            ////{
                            var columns = row.Descendants().Where(desc => desc.Name.Equals("th", StringComparison.OrdinalIgnoreCase)
                                                                        || desc.Name.Equals("td", StringComparison.OrdinalIgnoreCase)
                                                                        || desc.Name.Equals("tr", StringComparison.OrdinalIgnoreCase)).Select(
                                desc => desc.InnerText.ToUpper().Replace("&NBSP;", "").Trim());

                            if (columns.Count() > 0)
                            {
                                rowCount = rowCount + 1;
                                if ((doNotCheckFirstColumn || columns.First() == firstColumnNameToSerach.Trim().ToUpper()) && isTableFound == false)
                                {
                                    bool isAllColumnExist = true;
                                    foreach (string columnName in otherColumnNamesToSearch)
                                    {
                                        if (!columns.Contains(columnName.Trim().ToUpper()))
                                        {
                                            isAllColumnExist = false;
                                            break;
                                        }
                                    }
                                    if (isAllColumnExist == true)
                                    {
                                        string columnName = string.Empty;
                                        if (!useWebsiteColumns)
                                        {
                                            dt.Columns.Add(firstColumnNameToSerach);
                                            dt.Columns.Add(firstColumnNameToSerach + "_ORG");
                                            dt.Columns.Add(firstColumnNameToSerach + "_XPATH");
                                            columnName = string.Empty;
                                            foreach (var column in otherColumnNamesToSearch)
                                            {
                                                columnName = column;
                                                while (dt.Columns.Contains(columnName) == true || dt.Columns.Contains(columnName + "_ORG") == true)
                                                {
                                                    ////continue;
                                                    columnName = columnName + "~";
                                                }
                                                dt.Columns.Add(columnName);
                                                dt.Columns.Add(columnName + "_ORG");
                                                dt.Columns.Add(columnName + "_XPATH");
                                                columnName = string.Empty;
                                            }
                                        }
                                        else
                                        {
                                            foreach (var column in columns)
                                            {
                                                columnName = column;
                                                while (dt.Columns.Contains(columnName) == true || dt.Columns.Contains(columnName + "_ORG") == true)
                                                {
                                                    ////continue;
                                                    columnName = columnName + "~";
                                                }
                                                dt.Columns.Add(columnName);
                                                dt.Columns.Add(columnName + "_ORG");
                                                dt.Columns.Add(columnName + "_XPATH");
                                                columnName = string.Empty;
                                            }
                                        }
                                        isTableFound = true;
                                    }
                                }
                            }
                        }
                        else
                        {
                            if (row.InnerText.ToUpper().IndexOf(firstColumnNameToSerach.Trim().ToUpper()) > 0)
                            {
                                bool allHeaderPresent = true;
                                foreach (string columnName in otherColumnNamesToSearch)
                                {
                                    if (row.InnerText.ToUpper().IndexOf(columnName.Trim().ToUpper()) < 0)
                                    {
                                        allHeaderPresent = false;
                                        break;
                                    }
                                }
                                if (allHeaderPresent == true)
                                {
                                    continue;
                                }
                            }
                            var columns = row.Descendants().Where(
                                 desc => (desc.Name.Equals("th", StringComparison.OrdinalIgnoreCase) || desc.Name.Equals("td", StringComparison.OrdinalIgnoreCase) || desc.Name.Equals("tr", StringComparison.OrdinalIgnoreCase)));

                            var columns1 = row.Descendants().Where(desc => desc.Name.Equals("th", StringComparison.OrdinalIgnoreCase)
                                                                        || desc.Name.Equals("td", StringComparison.OrdinalIgnoreCase)
                                                                        || desc.Name.Equals("tr", StringComparison.OrdinalIgnoreCase)).Select(
                                desc => desc.InnerText.ToUpper().Replace("&NBSP;", "").Trim());

                            DataRow dtRow = dt.NewRow();
                            int i = 0;
                            addRowToTable = true;
                            if (dt.Columns.Count == columns.Count() * 3)
                            {
                                if (checkXpathOfColumns)
                                {
                                    NameHelper nameHelper = new NameHelper();
                                    if (string.IsNullOrWhiteSpace(xpathOfPreviousColumn))
                                    {
                                        xpathOfPreviousColumn = nameHelper.CleanName(columns.First().XPath);
                                    }
                                    else if (xpathOfPreviousColumn.ToUpper().Trim() != nameHelper.CleanName(columns.First().XPath).ToUpper().Trim())
                                    {
                                        addRowToTable = false;
                                        break;
                                    }
                                    nameHelper = null;
                                }
                                foreach (var column in columns)
                                {


                                    dtRow[i++] = FormatColumnText(column.InnerText.Trim());
                                    if (this.ErrorString.Trim().Length > 0)
                                    {
                                        return null;
                                    }
                                    dtRow[i++] = FormatColumnText(column.InnerHtml);

                                    if (this.ErrorString.Trim().Length > 0)
                                    {
                                        return null;
                                    }
                                    dtRow[i++] = column.XPath;
                                }
                                if (addRowToTable)
                                {
                                    dt.Rows.Add(dtRow);
                                }
                            }
                            else if (dt.Columns.Count < columns.Count() * 3 && areTableColumnsMoreThanRequired)
                            {
                                if (checkXpathOfColumns)
                                {
                                    NameHelper nameHelper = new NameHelper();
                                    if (string.IsNullOrWhiteSpace(xpathOfPreviousColumn))
                                    {
                                        xpathOfPreviousColumn = nameHelper.CleanName(columns.First().XPath);
                                    }
                                    else if (xpathOfPreviousColumn.ToUpper().Trim() != nameHelper.CleanName(columns.First().XPath).ToUpper().Trim())
                                    {
                                        addRowToTable = false;
                                        break;
                                    }
                                    nameHelper = null;
                                }
                                foreach (var column in columns)
                                {
                                    dtRow[i++] = FormatColumnText(column.InnerText.ToUpper().Replace("&NBSP;", "").Trim());
                                    if (this.ErrorString.Trim().Length > 0)
                                    {
                                        return null;
                                    }
                                    dtRow[i++] = FormatColumnText(column.InnerHtml);
                                    if (this.ErrorString.Trim().Length > 0)
                                    {
                                        return null;
                                    }
                                    dtRow[i++] = column.XPath;
                                    if (i >= dt.Columns.Count)
                                    {
                                        break;
                                    }
                                }
                                if (addRowToTable)
                                {
                                    dt.Rows.Add(dtRow);
                                }
                            }

                        }
                    }
                    if (isTableFound && !dataInNextTable)
                    {
                        break;
                    }
                }
            }

            if (isTableFound == true && dt.Columns.Count > 0)
            {
                return dt;
            }
            else
            {
                return null;
            }
        }

        private DataTable ExtractHtmlTableIntoDataTable_RowsAsHeader(string response, string firstColumnNameToSerach, params string[] otherColumnNamesToSearch)
        {
            bool isTableFound = false;
            DataTable dt = new DataTable();
            int count = 0;
            HtmlDocument doc = new HtmlDocument();
            bool isFirstColumnExist = false;
            try
            {
                response = response.Replace("&nbsp;", " ");
                doc.LoadHtml(response);
                var tablecollection = doc.DocumentNode.SelectNodes("//table");

                if (tablecollection != null)
                {
                    for (int i = 0; i < otherColumnNamesToSearch.Count(); i++)
                    {
                        otherColumnNamesToSearch[i] = otherColumnNamesToSearch[i].ToUpper();
                    }
                    foreach (HtmlNode table in tablecollection)
                    {
                        DataRow valueRow = dt.NewRow();
                        // for all rows with at least one child with the 'th'  or 'td' tag
                        int rowCount = 0;
                        foreach (HtmlNode row in table.Descendants().Where(desc => desc.Name.Equals("tr", StringComparison.OrdinalIgnoreCase)
                                                                                    &&
                                                                                    (
                                                                                        desc.Descendants().Any(child => child.Name.Equals("th", StringComparison.OrdinalIgnoreCase))
                                                                                        ||
                                                                                        desc.Descendants().Any(child => child.Name.Equals("td", StringComparison.OrdinalIgnoreCase))
                                                                                        ||
                                                                                        desc.Descendants().Any(child => child.Name.Equals("tr", StringComparison.OrdinalIgnoreCase))
                                                                                    )
                                                                            ))
                        {
                            var columns = row.Descendants().Where(
                                     desc => (desc.Name.Equals("th", StringComparison.OrdinalIgnoreCase) || desc.Name.Equals("td", StringComparison.OrdinalIgnoreCase)));

                            ////var columns = row.Descendants().Where(desc => desc.Name.Equals("th", StringComparison.OrdinalIgnoreCase)
                            ////                                            || desc.Name.Equals("td", StringComparison.OrdinalIgnoreCase)
                            ////                                            || desc.Name.Equals("tr", StringComparison.OrdinalIgnoreCase)).Select(
                            ////    desc => desc.InnerText.ToUpper().Replace("&NBSP;", "").Trim());

                            if (columns.Count() > 0)
                            {
                                rowCount = rowCount + 1;
                                if (columns.First().InnerText.ToUpper().Trim() == firstColumnNameToSerach.Trim().ToUpper() || isFirstColumnExist == true)
                                {
                                    isFirstColumnExist = true;
                                    bool columnNameMatched = false;
                                    string headerName = string.Empty;
                                    foreach (var column in columns)
                                    {
                                        if (column.InnerText.ToUpper().Trim() == firstColumnNameToSerach.ToUpper().Trim() && columnNameMatched == false)
                                        {
                                            headerName = firstColumnNameToSerach.Trim();
                                            columnNameMatched = true;
                                        }
                                        else if (otherColumnNamesToSearch.Contains(column.InnerText.Trim().ToUpper()) && columnNameMatched == false)
                                        {
                                            headerName = column.InnerText.Trim();
                                            columnNameMatched = true;
                                        }
                                        else if (columnNameMatched == true)
                                        {
                                            if (!dt.Columns.Contains(headerName))
                                            {
                                                dt.Columns.Add(headerName);
                                                dt.Columns.Add(headerName + "_ORG");
                                                dt.Columns.Add(headerName + "_XPATH");
                                                valueRow[count++] = column.InnerText;
                                                valueRow[count++] = column.InnerHtml;
                                                valueRow[count++] = column.XPath;
                                            }
                                            columnNameMatched = false;
                                        }
                                    }

                                }
                            }
                        }
                        if ((otherColumnNamesToSearch.Count() + 1) * 3 == count)
                        {
                            isTableFound = true;
                            dt.Rows.Add(valueRow);
                            break;
                        }
                        dt = new DataTable();
                        count = 0;
                    }
                }

                if (isTableFound == true && dt.Columns.Count > 0)
                {
                    return dt;
                }
                else
                {
                    return null;
                }
            }
            catch (Exception ex)
            {
                this.ErrorString = "Error in ExtractHtmlTableIntoDataTable_RowsAsHeader : " + ex;
                return null;
            }
        }

        private string FormatColumnText(string columnText)
        {
            try
            {
                if (formatColumnText.Trim().Length > 0)
                {
                    foreach (string text in formatColumnText.Split('|'))
                    {
                        if (text.Trim().ToUpper() == "COMMENT")
                        {
                            ////Remove HTML comments from the column text
                            while (columnText.IndexOf("<!--") > -1 && columnText.IndexOf("-->") > -1)
                            {
                                columnText = columnText.Remove(columnText.IndexOf("<!--"), columnText.IndexOf("-->") - columnText.IndexOf("<!--") + 3);
                            }
                        }
                        else
                        {
                            columnText = columnText.ToUpper().Replace(text.ToUpper(), "");
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                this.ErrorString = "Error in FormatColumnText: " + ex;
                return "";
            }
            return columnText;
        }

        /// <summary>
        /// Applies all the DOB verification Logic
        /// </summary>
        /// <param name="websiteDOB">The DOB available on website</param>
        /// <param name="requestDOB">The Request DOB</param>
        /// <param name="dobFormat">The format of DOB expected on website</param>
        /// <returns>True is the DOB passes the said verification, else False</returns>
        private bool CheckDOB(string websiteDOB, string requestDOB, string dobFormat)
        {
            bool isMatch = false;
            try
            {
                ////Check the format of the DOB specified
                if (dobFormat.Trim().Length > 0)
                {
                    dobFormat = dobFormat.Split('|').First();
                    switch (dobFormat.Trim().ToUpper())
                    {
                        case "RANGEYOB":
                            if (websiteDOB.Trim().Length > 0)
                            {
                                DateTime dobOnWebsite = new DateTime();
                                websiteDOB = websiteDOB.ToLower().Replace("xxxx", "1990");
                                websiteDOB = websiteDOB.ToLower().Replace("x", "1");
                                if (websiteDOB.Trim().Length >= 8)
                                {
                                    dobOnWebsite = Convert.ToDateTime(websiteDOB);
                                }
                                else if (websiteDOB.Trim().Length == 4)
                                {
                                    dobOnWebsite = Convert.ToDateTime("01/01/" + websiteDOB);
                                }
                                if (dobOnWebsite.Year < (DateTime.Now.Year - 100) || dobOnWebsite.Year > (DateTime.Now.Year - 16))
                                {
                                    return true;
                                }
                            }
                            break;
                        case "BLANK":
                            if (websiteDOB.Trim().Length == 0)
                            {
                                return true;
                            }
                            break;
                        case "MATCH_STRING":
                            if (websiteDOB.Trim().ToUpper() == requestDOB.Trim().ToUpper())
                            {
                                this.ErrorString = "REVIEW";
                                return true;
                            }
                            break;
                        default:
                            ////Check the Website DOB is in DATE format or not
                            if (websiteDOB.Trim().Length > 0 && !ParseDate(requestDOB, ref websiteDOB, ref dobFormat, ref isMatch))
                            {
                                return false;
                            }
                            DateTime parsed;
                            if (websiteDOB.Trim().Length > 0 && !DateTime.TryParseExact(websiteDOB, dobFormat, CultureInfo.InvariantCulture, DateTimeStyles.AllowWhiteSpaces, out parsed))
                            {
                                this.ErrorString = "CANCEL: Website DOB not in specified format";
                                ResponseModel.ErrorDescription = this.ErrorString;
                                ResponseModel.ErrorCode = ErrorCode.DateFormatError;
                                ResponseModel.Status = OrderStatus.Cancelled;
                                return false;
                            }
                            break;
                    }
                }
                ////Check the DOB Value
                if (isMatch)
                {
                    return true;
                }
                ////If none of the conditions match, return False
                return false;
            }
            catch (Exception ex)
            {
                logger.Error("Error in CheckDOB: " + ex);
                this.ErrorString = "Error in CheckDOB: " + ex;
                return false;
            }
        }

        private bool ParseDate(string requestDOB, ref string websiteDOB, ref string dobFormat, ref bool isMatch)
        {
            try
            {
                //// Commented below as website DOB can be 1/12/2980 or 12/10/1980. So website DOB and format length will have a mismatch

                ////Check the length of Website DOB and Format. Both should be equal
                //if (websiteDOB.Trim().Length != dobFormat.Trim().Length)
                //{
                //    this.ErrorString = "Length of Website DOB and Format mismatch";
                //    return false;
                //}
                //If DOB on website is not in proper format
                if (dobFormat.ToUpper().IndexOf("X") > -1)
                {
                    char websiteChar;
                    char dobFormatChar;
                    for (int i = 0; i < dobFormat.ToArray().Length; i++)
                    {
                        websiteChar = websiteDOB.ToArray()[i];
                        dobFormatChar = dobFormat.ToArray()[i];
                        if (IsNumeric(websiteChar.ToString()) && dobFormatChar == 'X' && dobFormatChar == 'x'
                                                                && dobFormatChar == '/' && dobFormatChar == '/')
                        {
                            this.ErrorString = "Format of Website DOB and Format mismatch";
                            return false;
                        }
                        else if (!IsNumeric(websiteChar.ToString()) && dobFormatChar != websiteChar)
                        {
                            this.ErrorString = "Format of Website DOB and Format mismatch";
                            return false;
                        }
                    }
                    ////Formating the DOB Format
                    dobFormat = dobFormat.Replace("X", "").Replace("x", "");
                    dobFormat = dobFormat.TrimStart('/').TrimEnd('/').Trim();
                    ////Formatting the Website DOB
                    websiteDOB = websiteDOB.Replace("X", "").Replace("x", "");
                    websiteDOB = websiteDOB.TrimStart('/').TrimEnd('/').Trim();
                }

                ////If complete DOB is present
                if (dobFormat.Trim().Length >= 8)
                {
                    ////If we get DOB in proper format, then check whether Website DOB is in Date format or not
                    if (!IsDate(websiteDOB.Trim()))
                    {
                        this.ErrorString = "String not in Date Format";
                        return false;
                    }
                    ////Check for Matching with Request DOB
                    if (Convert.ToDateTime(websiteDOB).ToString("MM/dd/yyyy") == Convert.ToDateTime(requestDOB).ToString("MM/dd/yyyy"))
                    {
                        isMatch = true;
                    }
                    return true;
                }
                ////Only year is available on Website "yyyy"
                if (dobFormat.Trim() == "yyyy")
                {
                    if (websiteDOB.Trim().Length != 4 && IsNumeric(websiteDOB.Trim()) == false)
                    {
                        this.ErrorString = "String not in Year Format";
                        return false;
                    }
                    int year;
                    int.TryParse(websiteDOB.Trim(), out year);
                    if (1900 > year && year > DateTime.Now.Year)
                    {
                        this.ErrorString = "Year available not in valid range";
                        return false;
                    }
                    ////Match with request DOB
                    if (year.ToString() == Convert.ToDateTime(requestDOB).ToString("yyyy"))
                    {
                        isMatch = true;
                    }
                    return true;
                }
                ////Only month and date available "MM/dd"
                if (dobFormat.Trim() == "MM/dd")
                {
                    if (websiteDOB.Trim().Length != 5 || websiteDOB.IndexOf("/") == -1 || !IsDate(websiteDOB.Trim() + "/1980"))
                    {
                        this.ErrorString = "DOB available not valid as per format specified";
                        return false;
                    }
                    ////Match with request DOB
                    if (websiteDOB.Trim() == Convert.ToDateTime(requestDOB).ToString("MM/dd"))
                    {
                        isMatch = true;
                    }
                    return true;
                }
                ////Only Month and Year available "MM/yyyy"
                if (dobFormat.Trim() == "MM/yyyy" || dobFormat.Trim() == "dd/yyyy")
                {
                    if (websiteDOB.Trim().Length != 7 || websiteDOB.IndexOf("/") == -1 || !IsDate("01/" + websiteDOB.Trim()))
                    {
                        this.ErrorString = "DOB available not valid as per format specified";
                        return false;
                    }
                    ////Match with request DOB
                    if ((dobFormat.Trim() == "MM/yyyy" && websiteDOB.Trim() == Convert.ToDateTime(requestDOB).ToString("MM/yyyy"))
                        || (dobFormat.Trim() == "dd/yyyy" && websiteDOB.Trim() == Convert.ToDateTime(requestDOB).ToString("dd/yyyy")))
                    {
                        isMatch = true;
                    }
                    return true;
                }
                this.ErrorString = "DOB Format not matching with specified formats";
                return false;
            }
            catch (Exception ex)
            {
                this.ErrorString = "Error in ParseDate: " + ex;
                return false;
            }
            finally
            {
                if (!string.IsNullOrWhiteSpace(ErrorString) && !ErrorString.StartsWith("Error in ParseDate:"))
                {
                    this.ErrorString = "CANCEL: " + ErrorString.Trim();
                    ResponseModel.ErrorDescription = this.ErrorString;
                    ResponseModel.ErrorCode = ErrorCode.DateFormatError;
                    ResponseModel.Status = OrderStatus.Cancelled;
                }
            }
        }

        /// <summary>
        /// To check if the given string is in Date format or not
        /// </summary>
        /// <param name="input">The input string to be checked</param>
        /// <returns>True if the string is in Date format, Else False</returns>
        private bool IsDate(string input)
        {
            DateTime temp;
            return DateTime.TryParse(input, CultureInfo.CurrentCulture, DateTimeStyles.NoCurrentDateDefault, out temp) &&
                   temp.Hour == 0 &&
                   temp.Minute == 0 &&
                   temp.Second == 0 &&
                   temp.Millisecond == 0 &&
                   temp > DateTime.MinValue;
        }

        public bool IsNumeric(string s)
        {
            float output;
            return float.TryParse(s, out output);
        }

        private void PerformDateOperations(ConfigurableCWIStepsModel dataRow, string rowData)
        {
            string dateFormat = string.Empty;
            int parameter = 0;
            try
            {
                ////Reset functionresult
                functionresult = string.Empty;

                ////Check if rowdata is in date format or not
                if (!IsDate(rowData))
                {
                    this.ErrorString = "Error in PerformDateOperations: Specified string 'rowData' is not in Date Format";
                    return;
                }

                ////Check if Parameter is in "int" format or not
                if (!IsNumeric(updatedParameters))
                {
                    this.ErrorString = "Error in PerformDateOperations: Specified string 'Parameters' is not in Integer Format";
                    return;
                }
                else
                {
                    parameter = Convert.ToInt32(updatedParameters);
                }

                ////Converting the date from string to DateTime format
                DateTime oldDate = Convert.ToDateTime(rowData);

                //// Checking Format. If format is blank, we will use default format as MM/dd/yyyy
                if (string.IsNullOrWhiteSpace(dataRow.ConfigCWIFormat.ToString()))
                {
                    dateFormat = "MM/dd/yyyy";
                }
                else
                {
                    dateFormat = dataRow.ConfigCWIFormat.ToString().Trim();
                }

                ////Performing operations as per the step type
                switch (dataRow.StepType.ToString().ToUpper())
                {
                    case "ADDYEARS":
                        oldDate = oldDate.AddYears(parameter);
                        break;
                    case "ADDMONTHS":
                        oldDate = oldDate.AddMonths(parameter);
                        break;
                    case "ADDDAYS":
                        oldDate = oldDate.AddDays(parameter);
                        break;
                    default:
                        this.ErrorString = "Incorrect StepType provided for PerformDateOperation : " + dataRow.StepType.ToString().ToUpper();
                        break;
                }
                functionresult = Convert.ToDateTime(oldDate).ToString(dateFormat);
            }
            catch (Exception ex)
            {
                this.ErrorString = "Error in PerformDateOperation: " + ex;
            }
        }

        private void ExtractEnumValues(ConfigurableCWIStepsModel dataRow, string rowData)
        {
            string parameter = updatedParameters;
            functionresult = string.Empty;
            try
            {
                if (!parameter.Contains("|"))
                {
                    this.ErrorString = "Error in ExtractEnumValues: Parameter does not contain pipe seperator";
                }
                switch (dataRow.StepType.ToString().ToUpper())
                {
                    case "EXTRACTVENDORCODE":
                        if (EnumValues.Any(x => x.Type.Trim().ToUpper() == parameter.Split('|')[0].ToUpper().Trim()
                       && x.County.ToUpper().Trim() == parameter.Split('|')[1].Trim().ToUpper()
                       && x.VendorDescription.Trim().ToUpper() == rowData.Trim().ToUpper()))
                        {
                            functionresult = EnumValues.Where(x => x.Type.Trim().ToUpper() == parameter.Split('|')[0].ToUpper().Trim()
                                            && x.County.ToUpper().Trim() == parameter.Split('|')[1].Trim().ToUpper()
                                            && x.VendorDescription.Trim().ToUpper() == rowData.Trim().ToUpper())
                                           .Select(y => y.VendorCode).First();
                        }
                        else
                        {
                            this.ErrorString = "Error in ExtractEnumValues: Enumvalue not found";
                        }
                        break;
                    case "EXTRACTVENDORDESCRIPTION":
                        if (EnumValues.Any(x => x.Type.Trim().ToUpper() == parameter.Split('|')[0].ToUpper().Trim()
                        && x.County.ToUpper().Trim() == parameter.Split('|')[1].Trim().ToUpper()
                        && x.VendorCode.Trim().ToUpper() == rowData.Trim().ToUpper()))
                        {
                            functionresult = EnumValues.Where(x => x.Type.Trim().ToUpper() == parameter.Split('|')[0].ToUpper().Trim()
                                            && x.County.ToUpper().Trim() == parameter.Split('|')[1].Trim().ToUpper()
                                            && x.VendorCode.Trim().ToUpper() == rowData.Trim().ToUpper())
                                            .Select(y => y.VendorDescription).First();
                        }
                        else
                        {
                            this.ErrorString = "Error in ExtractEnumValues: Enumvalue not found";
                        }
                        break;
                    default:
                        this.ErrorString = "Error in ExtractEnumValues: Incorrect StepType provided : " + dataRow.StepType.ToString().ToUpper();
                        return;
                }

                if (dataRow.Verify.ToString().ToUpper() == "DO_NOT_SET_ERROR" && !string.IsNullOrWhiteSpace(this.ErrorString))
                {
                    this.ErrorString = string.Empty;
                }
            }
            catch (Exception ex)
            {
                this.ErrorString = "Error in ExtractEnumValues: " + ex;
            }
        }

        private void ExtractAttributeFromNode(ConfigurableCWIStepsModel dataRow, string rowData, ref bool exitIteration)
        {
            string innerHtmlText = string.Empty;
            functionresult = string.Empty;
            try
            {
                HtmlDocument doc = new HtmlDocument();
                switch (dataRow.StepType.ToString().ToUpper())
                {
                    case "INNERHTML":
                        if (innerHtmlValueList.Count > counter)
                        {
                            innerHtmlText = innerHtmlValueList[counter].Replace("\n", "").Replace("&nbsp;", "").Replace("&amp;", "&").Replace("&", "&amp;").Trim();
                            if (!string.IsNullOrWhiteSpace(innerHtmlText))
                            {
                                functionresult = XElement.Parse("<th>" + innerHtmlText + "</th>")
                                            .Descendants("a")
                                            .Select(x => x.Attribute(dataRow.ConfigCWIParameter.ToString()).Value)
                                            .FirstOrDefault().ToString();
                            }
                        }
                        else
                        {
                            if (dataRow.Verify.ToString().Trim().ToUpper() == "CONTINUE")
                            {
                                functionresult = "CONTINUE_AND_EXITLOOP";
                                this.ErrorString = string.Empty;
                            }
                            else
                            {
                                exitIteration = true;
                            }
                        }
                        break;
                    case "BUTTONINNERTEXT":
                        if (innerHtmlValueList.Count > counter)
                        {
                            innerHtmlText = innerHtmlValueList[counter].Replace("\n", "").Replace("&nbsp;", "").Replace("&amp;", "&").Replace("&", "&amp;");
                            if (!string.IsNullOrWhiteSpace(innerHtmlText))
                            {
                                functionresult = XElement.Parse("<th>" + innerHtmlText + "</th>")
                                            .Descendants("button")
                                            .Select(x => x.Attribute(dataRow.ConfigCWIParameter.ToString()).Value)
                                            .FirstOrDefault().ToString();
                            }
                        }
                        else
                        {
                            if (dataRow.Verify.ToString().Trim().ToUpper() == "CONTINUE")
                            {
                                functionresult = "CONTINUE_AND_EXITLOOP";
                                this.ErrorString = string.Empty;
                            }
                            else
                            {
                                exitIteration = true;
                            }
                        }
                        break;
                    case "INNERHTMLTEXT":
                        if (innerHtmlValueList.Count > counter)
                        {
                            functionresult = innerHtmlValueList[counter].Replace("\n", "");
                        }
                        else
                        {
                            if (dataRow.Verify.ToString().Trim().ToUpper() == "CONTINUE")
                            {
                                functionresult = "CONTINUE_AND_EXITLOOP";
                                this.ErrorString = string.Empty;
                            }
                            else
                            {
                                exitIteration = true;
                            }
                        }
                        break;
                    case "SCREENINGRESPONSECASEEXTRAINFO":
                        if (ResponseModel.Screening[0].ScreeningResponse.Cases.Count > counter)
                        {
                            if (dataRow.ConfigCWIParameter.ToString().ToUpper() == "ONLY_PENDING_CASES" &&
                                ResponseModel.Screening[0].ScreeningResponse.Cases[counter].CaseServedStatus == OrderStatus.Fulfilled)
                            {

                                functionresult = "FULFILLED_CASE";
                                this.ErrorString = string.Empty;
                                return;
                            }
                            functionresult = ResponseModel.Screening[0].ScreeningResponse.Cases[counter].ExtraInfo.Replace("\n", "").Replace("&nbsp;", "").Replace("&amp;", "&").Replace("&", "&amp;").Trim();
                            //// Get the Case URL from complete Extra Info
                            functionresult = functionresult.EvaluateValues("<CASE_URL>", "</CASE_URL>");
                            ResponseModel.Screening[0].ScreeningResponse.Cases[counter].CaseServedStatus = OrderStatus.InProgress;
                        }
                        else
                        {
                            if (dataRow.Verify.ToString().Trim().ToUpper() == "CONTINUE")
                            {
                                functionresult = "CONTINUE_AND_EXITLOOP";
                                this.ErrorString = string.Empty;
                            }
                            else
                            {
                                exitIteration = true;
                            }
                        }
                        break;
                    default:
                        break;
                }

            }
            catch (Exception ex)
            {
                this.ErrorString = "Error in ExtractAttributeFromNode: " + ex;
            }
        }

        private void SplitIntoInnerHTMLList(ConfigurableCWIStepsModel dataRow, string rowData)
        {
            char splitter;
            try
            {
                if (string.IsNullOrWhiteSpace(rowData)) splitter = '|';
                else splitter = rowData.Trim()[0];

                switch (dataRow.StepType.ToString().ToUpper())
                {
                    case "":
                    case "RESETVALUE":
                        innerHtmlValueList = new List<string>();
                        counter = 0;
                        break;
                    case "DONOTRESET":
                        break;
                    default:
                        this.ErrorString = "Incorrect StepType provided in SplitIntoInnerHTMLList";
                        break;
                }

                // Add Functionresult values to List
                if (!string.IsNullOrWhiteSpace(functionresult))
                    innerHtmlValueList.AddRange(functionresult.Split(splitter));
            }
            catch (Exception ex)
            {
                this.ErrorString = "Error in SplitIntoInnerHTMLList: " + ex;
            }
        }

        private void CheckRequestParameters(ConfigurableCWIStepsModel dataRow, string rowData)
        {
            bool parameterMatched = false;
            bool previousResult = false;
            try
            {
                switch (dataRow.StepType.ToString().ToUpper())
                {
                    case "REGION":
                        parameterMatched = ResponseModel.Screening.First().ScreeningRequest.Region.Trim().ToUpper() == rowData.Trim().ToUpper();
                        break;
                    case "COUNTY":
                        parameterMatched = ResponseModel.Screening.First().ScreeningRequest.County.Trim().ToUpper() == rowData.Trim().ToUpper();
                        break;
                    case "COURTNAME":
                        if (!string.IsNullOrWhiteSpace(ResponseModel.Screening.First().ScreeningRequest.CourtName))
                        {
                            parameterMatched = ResponseModel.Screening.First().ScreeningRequest.CourtName.Trim().ToUpper() == rowData.Trim().ToUpper();
                        }
                        break;
                    case "SSN":
                        CheckAndFormatSSN(dataRow);
                        return;
                    default:
                        this.ErrorString = "Incorrect StepType provided in CheckRequest";
                        break;
                }

                if (!string.IsNullOrWhiteSpace(functionresult) && functionresult.Trim().ToUpper() == "TRUE")
                {
                    previousResult = true;
                }
                else if (string.IsNullOrWhiteSpace(functionresult))
                {
                    functionresult = parameterMatched.ToString();
                    return;
                }

                if (dataRow.Logic != null && dataRow.Logic.ToString().ToUpper() == "OR")
                {
                    previousResult = previousResult || parameterMatched;
                    functionresult = previousResult.ToString();
                    return;
                }
                else if (dataRow.Logic != null && dataRow.Logic.ToString().ToUpper() == "AND")
                {
                    previousResult = previousResult && parameterMatched;
                    functionresult = previousResult.ToString();
                    return;
                }
                else
                {
                    functionresult = parameterMatched.ToString();
                    return;
                }
            }
            catch (Exception ex)
            {
                this.ErrorString = "Error in CheckRequestParameters : " + ex;
            }
        }

        private void CheckAndFormatSSN(ConfigurableCWIStepsModel dataRow)
        {
            try
            {
                string ssn = ResponseModel.Screening[0].ScreeningRequest.PersonName.SSN;
                if (string.IsNullOrWhiteSpace(ssn))
                {
                    this.ErrorString = "SSN ERROR : SSN not found in Request";
                    ResponseModel.ErrorDescription = this.ErrorString;
                    ResponseModel.ErrorCode = ErrorCode.SSNFormatError;
                    ResponseModel.Status = OrderStatus.Cancelled;
                    return;
                }
                ssn = ssn.Replace("-", "").Replace(" ", "");
                if (string.IsNullOrWhiteSpace(ssn) || string.IsNullOrWhiteSpace(ssn.Replace("9", "")) || ssn.Length != 9)
                {
                    this.ErrorString = "SSN ERROR : SSN not in proper format for the Request";
                    ResponseModel.ErrorDescription = this.ErrorString;
                    ResponseModel.ErrorCode = ErrorCode.SSNFormatError;
                    ResponseModel.Status = OrderStatus.Cancelled;
                    return;
                }
                if (dataRow.ConfigCWIParameter.ToString().ToUpper().Trim() == "SSN_WITH_DASH")
                {
                    formattedSSN = ssn.Insert(3, "-").Insert(6, "-");
                }
                else
                {
                    formattedSSN = ssn.Trim();
                }
            }
            catch (Exception ex)
            {
                this.ErrorString = "Error in CheckAndFormatSSN : " + ex;
            }
        }

        private void GoToSpecifiedStep(ConfigurableCWIStepsModel dataRow, string rowData)
        {
            try
            {
                if (dataRow.StepType.ToString().Trim().ToUpper() == "CHECKPREVIOUSCONDITION")
                {
                    bool reverse = false;
                    if (dataRow.ConfigCWIFormat.ToString().Trim().ToUpper() == "REVERSE")
                    {
                        reverse = true;
                    }
                    bool changeStep = false;
                    switch (dataRow.ConfigCWIParameter.ToString().ToUpper())
                    {
                        case "ANY_STRING":
                            if (!string.IsNullOrWhiteSpace(functionresult))
                            {
                                changeStep = true;
                            }
                            break;
                        case "EMPTY_STRING":
                            if (string.IsNullOrWhiteSpace(functionresult))
                            {
                                changeStep = true;
                            }
                            break;
                        default:
                            if (functionresult.Trim().ToUpper() == dataRow.ConfigCWIParameter.ToString().Trim().ToUpper())
                            {
                                changeStep = true;
                            }
                            break;
                    }
                    if (!(changeStep ^ reverse))
                    {
                        return;
                    }
                }

                for (int i = 0; i < configurableCWISteps.Count; i++)
                {
                    if (configurableCWISteps[i].StepDescription.ToString().Trim().ToUpper() == rowData.Trim().ToUpper())
                    {
                        stepCount = i - 1;
                        return;
                    }
                }

            }
            catch (Exception ex)
            {
                this.ErrorString = "Error in GoToSpecifiedStep : " + ex;
            }
        }

        private string GetImageContentType(string contentType)
        {
            string imageExt = string.Empty;
            try
            {
                switch (contentType)
                {
                    case "image/gif":
                        imageExt = "gif";
                        break;

                    case "image/x-icon":
                        imageExt = "ico";
                        break;

                    case "image/jpeg":
                        imageExt = "jpeg";
                        break;

                    case "image/Jpeg":
                        imageExt = "Jpeg";
                        break;

                    case "image/png":
                        imageExt = "png";
                        break;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return imageExt;
        }

        private void EvaluateValues(ConfigurableCWIStepsModel dataRow, string rowData)
        {
            string inputString = string.Empty;
            try
            {
                switch (dataRow.StepType.ToString().ToUpper())
                {
                    case "FUNCTIONRESULT":
                        inputString = functionresult;
                        break;
                    case "WEBSITERESPONSE":
                        inputString = HttpManager.Response;
                        break;
                    case "POSTSTRING":
                        inputString = postString;
                        break;
                    default:
                        this.ErrorString = "Incorrect StepType provided for EvaluateValues : " + dataRow.StepType.ToString().ToUpper();
                        return;
                }
                functionresult = string.Empty;
                ////Check the row data
                if (!rowData.Contains("|"))
                {
                    functionresult = inputString.EvaluateValues(rowData);
                    if (string.IsNullOrWhiteSpace(functionresult))
                    {
                        functionresult = inputString.ToUpper().EvaluateValues(rowData.ToUpper());
                    }
                }
                else
                {
                    //// Apply Evaluate Values Function
                    functionresult = inputString.EvaluateValues(rowData.Split('|')[0], rowData.Split('|')[1]);
                }

            }
            catch (Exception ex)
            {
                this.ErrorString = "Error in EvaluateValues : " + ex;
            }
        }

        private void URLDecode(string rowData)
        {
            try
            {
                functionresult = string.Empty;
                functionresult = HttpManager.UrlDecode(rowData);
            }
            catch (Exception ex)
            {
                this.ErrorString = "Error in URLDecode: " + ex;
            }
        }

        private void ReplaceInString(ConfigurableCWIStepsModel dataRow, string rowData)
        {
            string inputString = string.Empty;
            try
            {
                switch (dataRow.StepType.ToString().ToUpper())
                {
                    case "FUNCTIONRESULT":
                        functionresult = functionresult.Replace(updatedParameters, rowData);
                        break;
                    case "WEBSITERESPONSE":
                        HttpManager.Response = websiteResponse = websiteResponse.Replace(updatedParameters, rowData);
                        break;
                    case "POSTSTRING":
                        postString = postString.Replace(updatedParameters, rowData);
                        break;
                    default:
                        this.ErrorString = "Incorrect StepType provided for ReplaceInString : " + dataRow.StepType.ToString().ToUpper();
                        return;
                }
            }
            catch (Exception ex)
            {
                this.ErrorString = "Error in ReplaceInString: " + ex;
            }
        }

        private string NumberToWords(string rowData)
        {
            int number;
            try
            {
                if (!Int32.TryParse(rowData, out number))
                {
                    this.ErrorString = "Row Data is not in Integer Format";
                    return "";
                }
                if (number == 0)
                {
                    return "zero";
                }

                if (number < 0)
                {
                    return "minus " + NumberToWords(Math.Abs(number).ToString());
                }

                functionresult = "";

                if ((number / 1000000) > 0)
                {
                    functionresult += NumberToWords((number / 1000000).ToString()) + " million ";
                    number %= 1000000;
                }

                if ((number / 1000) > 0)
                {
                    functionresult += NumberToWords((number / 1000).ToString()) + " thousand ";
                    number %= 1000;
                }

                if ((number / 100) > 0)
                {
                    functionresult += NumberToWords((number / 100).ToString()) + " hundred ";
                    number %= 100;
                }

                if (number > 0)
                {
                    if (functionresult != "")
                        functionresult += "and ";

                    var unitsMap = new[] { "zero", "one", "two", "three", "four", "five", "six", "seven", "eight", "nine", "ten", "eleven", "twelve", "thirteen", "fourteen", "fifteen", "sixteen", "seventeen", "eighteen", "nineteen" };
                    var tensMap = new[] { "zero", "ten", "twenty", "thirty", "forty", "fifty", "sixty", "seventy", "eighty", "ninety" };

                    if (number < 20)
                        functionresult += unitsMap[number];
                    else
                    {
                        functionresult += tensMap[number / 10];
                        if ((number % 10) > 0)
                            functionresult += "-" + unitsMap[number % 10];
                    }
                }

                return functionresult;
            }
            catch (Exception ex)
            {
                this.ErrorString = "" + ex;
                return this.ErrorString;
            }
        }

        private DataTable ExtractDataTableFromXML(ConfigurableCWIStepsModel dataRow, string rowData)
        {
            DataTable resultDataTable = new DataTable();
            try
            {
                //// Parse the response in to a XDoc
                XDocument responseXDocument = XDocument.Parse(websiteResponse);
                //// Get all the column Names as per the header node specified
                XElement headerElement = null;
                if (responseXDocument.Descendants().Any(x => x.Name.LocalName.ToUpper().Trim() == rowData.Trim().ToUpper()))
                {
                    headerElement = responseXDocument.Descendants()
                                            .Where(x => x.Name.LocalName.ToUpper().Trim() == rowData.Trim().ToUpper())
                                            .First();
                }
                else
                {
                    if (dataRow.Verify.ToString().ToUpper() != "NO")
                    {
                        this.ErrorString = "Proper XML node not found";
                    }
                    return null;
                }

                if (!headerElement.HasElements)
                {
                    this.ErrorString = "Header Element specified is not present in XML";
                    return null;
                }
                //// Get all the column names from the header element
                List<string> columnNames = new List<string>();
                columnNames = this.ResultTableColumnNames.Select(x => x.Name.ToUpper()).ToList();
                foreach (XElement element in headerElement.Descendants())
                {
                    if (!columnNames.Contains(element.Name.LocalName.ToUpper()))
                    {
                        this.ErrorString = "Error in ExtractDataTableFromXML : Column found on website not given in list of columns";
                        return null;
                    }
                    resultDataTable.Columns.Add(new DataColumn(element.Name.LocalName, typeof(string)));
                }

                //// Iterate all the header nodes and fill the data table
                var allDescendentsNodes = from p in responseXDocument.Descendants(headerElement.Name.ToString()) select p;
                foreach (XElement element in allDescendentsNodes)
                {
                    if (element.Descendants().Count() == resultDataTable.Columns.Count)
                    {
                        DataRow dr = resultDataTable.NewRow();
                        foreach (XElement innerElement in element.Descendants())
                        {
                            dr[innerElement.Name.LocalName.ToString()] = innerElement.Value; //add in the values
                        }
                        //// Add the new row to DataTable
                        resultDataTable.Rows.Add(dr);
                    }
                    else
                    {

                    }
                }

                return resultDataTable;
            }
            catch (Exception ex)
            {
                this.ErrorString = "Error in ExtractDataTableFromXML: " + ex;
                return null;
            }
        }

        private HtmlDocument LoadHtmlDocument(string webresponse)
        {
            try
            {
                HtmlDocument htmlDocument = new HtmlDocument();
                MemoryStream stream = new MemoryStream(Encoding.Default.GetBytes(webresponse), 0, webresponse.Length);
                htmlDocument.Load(stream);
                stream.Close();
                return htmlDocument;
            }
            catch (Exception Ex)
            {
                this.ErrorString ="Error in LoadHtmlDocument "  + Ex.ToString();
                return null;
            }
        }

        //// Commented out code. Do Not Remove

        //private void CreateList(ConfigurableCWIStepsModel dataRow)
        //{
        //    string inputString = string.Empty;
        //    try
        //    {
        //        switch (dataRow.Step.ToString().ToUpper())
        //        {
        //            case "EXTRACTFROMWEBSITE":
        //                ExtractFromWebsite(dataRow);
        //                break;
        //            case "EXTRACTFROMFUNCTIONRESULT":
        //                //// TODO: Implement when needed
        //                break;
        //            default:
        //                this.ErrorString = "Invalid Step provided for CreateList function";
        //                break;
        //        }               
        //    }
        //    catch (Exception ex)
        //    {
        //        this.ErrorString = "In Catch block of CreateList: " + ex;
        //    }
        //}

        //private void ExtractFromWebsite(ConfigurableCWIStepsModel dataRow)
        //{
        //    HtmlDocument htmlDocument = new HtmlDocument();
        //    HtmlNodeCollection elements = null;
        //    List<HtmlNode> requiredElements = new List<HtmlNode>();
        //    try
        //    {

        //        htmlDocument.LoadHtml(websiteResponse);


        //        switch (dataRow.StepType.ToString().ToUpper())
        //        {
        //            case "EXTRACTFROMLINK":
        //                elements = htmlDocument.DocumentNode.SelectNodes("//a");
        //                break;
        //            default:
        //                this.ErrorString = "Invalid StepType provided for ExtractFromWebsite function";
        //                return;
        //        }
        //        if (elements == null)
        //        {
        //            this.ErrorString = "No elements found for StepType Provided in ExtractFromWebsite";
        //            return;
        //        }
        //        switch (dataRow.ConfigCWIParameter.ToString().ToUpper())
        //        {
        //            case "EXTRACTBYCLASS":
        //                if (elements.Any(x => x.GetAttributeValue("Class", string.Empty).ToUpper().Trim() == rowData))
        //                {
        //                    requiredElements = elements.Where(x => x.GetAttributeValue("Class", string.Empty).ToUpper().Trim() == rowData).ToList();
        //                }
        //                break;
        //            default:
        //                this.ErrorString = "Invalid Parameters provided for ExtractFromWebsite function";
        //                return;
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        this.ErrorString = "In Catch block of ExtractFromWebsite: " + ex;
        //    }
        //}

        //public string CheckForReview(IntegrationResponseModel responseModel, List<ConfigDetails> configDetail, List<EnumValuesModel> EnumValues, string websiteResponse, ConfigurableCWIStepsModel dataRow, List<string> firstNameList, List<string> lastNameList, ArrayList additionalParameters)

    }

    class CaseCompare : IEqualityComparer<Case>
    {
        public bool Equals(Case x, Case y)
        {
            if (x.CaseNumber == y.CaseNumber)
            {
                return true;
            }
            else { return false; }
        }
        public int GetHashCode(Case codeh)
        {
            return 0;
        }

    }
}