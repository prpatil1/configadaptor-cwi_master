﻿namespace PRISMDataAdaptor
{
    using HtmlAgilityPack;
    using PRISM.Common.HttpHelper;
    using PRISM.Common.Models.Screening;
    using System;
    using System.Data;

    class AddOnFulfillment
    {
        public string IN_BOONE_GetValueOfXPage1(IntegrationResponseModel responseModel, string websiteResponse, DataRow dataRow)
        {
            bool foundValue = false;
            try
            {
                ////Convert HTML response in to HTML Document
                HtmlDocument htmlDocument = new HtmlAgilityPack.HtmlDocument();
                htmlDocument.LoadHtml(websiteResponse);

                ////Get all the Links in website response
                foreach (HtmlNode link in htmlDocument.DocumentNode.SelectNodes("//a[@href]"))
                {
                    if (link.InnerText.ToUpper()== dataRow["Data"].ToString().ToUpper())
                    {
                        string value = link.Attributes["onclick"].Value;
                        value = CommonExtensions.EvaluateValues(value, "'id7', '?x=", "', '");
                        if (!string.IsNullOrWhiteSpace(CommonExtensions.ErrorDescription))
                        {
                            throw (new Exception(CommonExtensions.ErrorDescription));
                        }
                        foundValue = true;
                        return value;
                    }
                }
                if (!foundValue)
                {
                    throw (new Exception("IN_BOONE_GetValueOfXPage1: Value of X not found"));
                }
                return "";
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
