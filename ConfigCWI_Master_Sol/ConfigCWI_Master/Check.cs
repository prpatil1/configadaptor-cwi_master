﻿namespace PRISMDataAdaptor
{
    /// <summary>
    /// Stores all the values required to perform checks on a particular string or column name
    /// </summary>
    public class Check
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Check"/> class.
        /// </summary>
        public Check()
        {
            this.Type = string.Empty;
            this.Logic = "AND";
            this.Value = string.Empty;
            this.Format = string.Empty;
            this.IsTrue = false;
        }

        /// <summary>
        /// Gets or sets the type.
        /// </summary>
        /// <value>
        /// The type.
        /// </value>
        public string Type { get; set; }

        /// <summary>
        /// Gets or sets the logic.
        /// </summary>
        /// <value>
        /// The logic.
        /// </value>
        public string Logic { get; set; }

        /// <summary>
        /// Gets or sets the value.
        /// </summary>
        /// <value>
        /// The value.
        /// </value>
        public string Value { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether this instance is true.
        /// </summary>
        /// <value>
        ///   <c>true</c> if this instance is true; otherwise, <c>false</c>.
        /// </value>
        public bool IsTrue { get; set; }

        /// <summary>
        /// Gets or sets the Format.
        /// </summary>
        /// <value>
        /// The Format specified.
        /// </value>
        public string Format { get; set; }       
    }
}
