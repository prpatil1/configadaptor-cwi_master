﻿namespace ConfigCWI_MasterTests
{

    using System;
    using Microsoft.VisualStudio.TestTools.UnitTesting;
    using System.Collections.Generic;
    using System.Data;
    using System.Data.OleDb;
    using PRISM.Common.DataHelper.AzureSqlManager;
    using PRISM.Common.HttpHelper;
    using PRISM.Common.Models.DBModels;
    using PRISM.Common.Models.Screening;
    using PRISMDataAdaptor;
    using Newtonsoft.Json;
    using System.Linq;
    using CaseParserMaster;

    [TestClass]
    public class FulfillmentTest
    {
        private string parentVendor = "CWI_TX_NUECES_DRE";
        private AzureSqlHelper azureSqlHelper = new AzureSqlHelper();

        [TestMethod]
        public void SearchData_Test()
        {
            // Arrange
            HttpManager storedHTTPManager = new HttpManager();
            string result = string.Empty;
            ConfigDetails config;

            IntegrationResponseModel tempResponseModel = null;

            List<string> firstNames = new List<string>() { "ABIGAYLE" };
            List<string> lastNames = new List<string>() { "DA-RGAN" };
            List<string> Dobs = new List<string>() { "08/14/1994" };
            for (int i = 0; i < firstNames.Count; i++)
            {
                // Create object for Fulfillment
                Fulfillment fulfillment = new Fulfillment();

                List<ConfigDetails> lstconfig = new List<ConfigDetails>();
                List<EnumValuesModel> enumValues = new List<EnumValuesModel>();
                List<string> parentVendorList = new List<string> { parentVendor };
                IEnumerable<VendormapModel> vendorInfo = azureSqlHelper.GetVendormapData(parentVendorList);
                if ((vendorInfo.Where(x => x.ParentVendor.ToUpper() == parentVendor.ToUpper()).Select(y => y.ConfigDetails).Single()) != null)
                {
                    //lstconfig.AddRange(vendorInfo.Where(x => x.ParentVendor.ToUpper() == parentVendor.ToUpper()).Select(y => y.ConfigDetails).Single());
                }
                if ((vendorInfo.Where(x => x.ParentVendor.ToUpper() == parentVendor.ToUpper()).Select(y => y.EnumValues).Single()) != null)
                {
                    enumValues.AddRange(vendorInfo.Where(x => x.ParentVendor.ToUpper() == parentVendor.ToUpper()).Select(y => y.EnumValues).Single());
                }

                // Act
                if (tempResponseModel == null)
                {

                    fulfillment.ResponseModel = new IntegrationResponseModel();
                    fulfillment.ResponseModel.Screening = new List<Screening>();
                    fulfillment.ResponseModel.Screening.Add(new Screening());
                    fulfillment.ResponseModel.Screening[0].ScreeningId = "ABCD_1234";
                    fulfillment.ResponseModel.Screening[0].ScreeningRequest = new ScreeningRequest();
                    fulfillment.ResponseModel.Screening[0].ScreeningRequest.PersonName = new PersonName();
                    fulfillment.ResponseModel.Screening[0].ScreeningRequest.PersonName.DateOfBirth = Dobs[i];
                    fulfillment.ResponseModel.Screening[0].ScreeningRequest.PersonName.FirstName = firstNames[i];
                    fulfillment.ResponseModel.Screening[0].ScreeningRequest.PersonName.MiddleName = "XI";
                    fulfillment.ResponseModel.Screening[0].ScreeningRequest.PersonName.SSN = "987098998";
                    fulfillment.ResponseModel.Screening[0].ScreeningRequest.PersonName.LastName = lastNames[i];
                    fulfillment.ResponseModel.Screening[0].ScreeningRequest.Region = "TX";
                    fulfillment.ResponseModel.Screening[0].ScreeningRequest.County = "NUECES";
                    fulfillment.ResponseModel.Screening[0].ScreeningRequest.CourtName = "";
                }
                else
                {
                    fulfillment.ResponseModel = tempResponseModel;
                }
                config = new ConfigDetails
                {
                    FieldName = "AddOnDllPath",
                    FieldValue = @"C:\PRISMGit\configadaptor-cwi_tx_nueces_dre\CWI_TX_NUECES_DRESol\PRISM.ConfigAdaptor.CWI.TX.NUECES.DRE\bin\Debug\PRISMConfigAdaptor.dll",
                    ParentVendor = parentVendor
                };
                lstconfig.Add(config);

                fulfillment.ConfigDetail = lstconfig;
                fulfillment.EnumValues = enumValues;
                fulfillment.HttpManager = storedHTTPManager;

                // Data Dictionary
                DataSet dataSet = azureSqlHelper.GetConfigCWISteps(parentVendor);

                if (dataSet.Tables.Count > 5)
                    fulfillment.DataDictionary = new Dictionary<string, object>
                {
                    {"ConfigCWIModel", new ConfigCWIModel ()
                        {
                            ConfigurableCWISteps = CommonFunctions.GetConfigurableCWISteps(dataSet.Tables[0]),
                            RuleEngineSteps = CommonFunctions.GetRuleEngineSteps(dataSet.Tables[1]),
                            ExtractionSteps=  CommonFunctions.GetExtractionSteps(dataSet.Tables[2]),
                            FieldConfigurations = CommonFunctions.GetFieldConfigData(dataSet.Tables[3]),
                            SourceFieldMappings = CommonFunctions.GetMappingData(dataSet.Tables[4]),
                            DataFilters = CommonFunctions.GetFilterData(dataSet.Tables[5])
                        }
                    }
                };
                //else
                //fulfillment.DataDictionary = new Dictionary<string, object>
                //    {
                //    {"ConfigCWIModel", new ConfigCWIModel ()
                //        {
                //            ConfigurableCWISteps = CommonFunctions.ConfigurableCWISteps(@"C:\Vinkal\PRISMGit\Config CWI\" + parentVendor+ @"\" + parentVendor + "_Posting.xlsx", 1),
                //            RuleEngineSteps = CommonFunctions.GetRuleEngineSteps(@"C:\Vinkal\PRISMGit\Config CWI\" + parentVendor + @"\" + parentVendor + @"_RuleEngineSteps.xlsx", 3),
                //            ExtractionSteps=  CommonFunctions.GetExtractionSteps(@"C:\Vinkal\PRISMGit\Config CWI\" + parentVendor+ @"\" + parentVendor +@"_ExtractionSteps.xlsx", 2),
                //            FieldConfigurations = CommonFunctions.GetFieldConfigurations(@"C:\Vinkal\PRISMGit\Config CWI\" + parentVendor+ @"\" + parentVendor +@"_ExtractionSteps.xlsx", 2),
                //            SourceFieldMappings = CommonFunctions.GetMappingDetails(@"C:\Vinkal\PRISMGit\Config CWI\" +parentVendor + @"\" + parentVendor + @"_RuleEngineSteps.xlsx", 3),
                //            DataFilters = CommonFunctions.GetFilters(@"C:\Vinkal\PRISMGit\Config CWI\" + parentVendor + @"\" + parentVendor + @"_RuleEngineSteps.xlsx", 3)
                //        }
                //    }
                //    };

                fulfillment.SearchData();
                tempResponseModel = fulfillment.ResponseModel.CloneObject<IntegrationResponseModel>();
                storedHTTPManager = fulfillment.HttpManager;

                var temp = JsonConvert.DeserializeObject<IntegrationResponseModel>(JsonConvert.SerializeObject(fulfillment.ResponseModel));

                if (fulfillment.ResponseModel.Screening[0].ResultStatus != null)
                {
                    result = fulfillment.ResponseModel.Screening[0].ResultStatus;
                }
                else
                {
                    result = fulfillment.ResponseModel.ErrorDescription;
                }
            }
            // Assert
            Assert.AreEqual("HIT", result.ToUpper());
        }
    }
}